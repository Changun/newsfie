(defproject lifestreams-db "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0-alpha1"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [http-kit "2.1.16"]
                 [aprint "0.1.3"]
                 [cheshire "5.4.0"]
                 [throttler "1.0.0"]
                 [base64-clj "0.1.1"]
                 [enlive "1.1.5"]
                 [org.jruby/jruby-complete "1.7.19"]
                 [edu.stanford.nlp/stanford-corenlp "3.5.1"]
                 [cc.mallet/mallet "2.0.7"]
                 [com.novemberain/monger "2.0.1"]
                 [org.apache.lucene/lucene-snowball "3.0.3"]
                 [net.mikera/core.matrix "0.34.0"]
                 [net.mikera/vectorz-clj "0.29.0"]
                 [com.taoensso/timbre "3.4.0"]
                 [org.apache.commons/commons-lang3 "3.0"]
                 [clj-oauth "1.5.2"]
                 [compojure "1.3.3"]
                 [ring "1.3.2"]
                 [org.clojure/core.memoize "0.5.6"]
                 [clatrix "0.4.0"]
                 [simple-progress "0.1.3"]
                 [incanter/incanter-core "1.5.6"]
                 [incanter/incanter-charts "1.5.6"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [clojure.jdbc/clojure.jdbc-c3p0 "0.3.1"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"]
                 [amazonica "0.3.22"]
                 [org.apache.logging.log4j/log4j-api "2.0-beta9"]
                 [org.apache.logging.log4j/log4j-core "2.0-beta9"]
                 [com.taoensso/nippy "2.8.0"]
                 [org.apache.mahout/mahout-core "0.9"]
                 [primitive-math "0.1.3"]
                 [prismatic/hiphip "0.2.1"]
                 [com.restfb/restfb "1.14.1"]


                 ]
  :exclusions [org.slf4j/slf4j-log4j12]
  :source-paths ["src/main/clojure"]
  :resource-paths ["resources"]
  :main ^:skip-aot lifestreams-db.core
  :target-path "target/%s"
  :jvm-opts ["-Xmx5g" "-server"]
  :java-source-paths ["src/main"]

  :profiles {:uberjar {:aot :all}
             :dev {:dependencies []
                   :resource-paths ["resources"]}})
