package cc.mallet.topics;

import cc.mallet.util.Randoms;

import java.util.ArrayList;

/**
 * Created by Cheng-Kang Hsieh on 9/14/15.
 */
public class CPBackgroundWorkerRunnable extends  BackgroundWorkerRunnable {

    public CPBackgroundWorkerRunnable(
            int numTopics, double[] alpha, double alphaSum, double beta, double betaBackground, double lambda,
            Randoms random, ArrayList<TopicAssignment> data,
            int[][] typeTopicCounts, int[] tokensPerTopic,
            int[] typeBackgroundCounts,
            int[] typePersonalCounts,
            int[] backgroundAndTopicalCounts,
            int startDoc, int numDocs) {
        super(numTopics, alpha, alphaSum, beta, betaBackground, lambda, random, data, typeTopicCounts, tokensPerTopic, typeBackgroundCounts, backgroundAndTopicalCounts, startDoc, numDocs);
    }
}
