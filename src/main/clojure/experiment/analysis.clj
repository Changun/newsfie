(ns experiment.analysis
  "Various functions to evaluate recommendation results"
  (:require [cheshire.core :as json]
            [clojure.core.matrix :as m]
            [incanter.stats :as stats])
  (:use [incanter.core]
        [incanter.charts]))
(defn MAP [coll]
  (float (/ (reduce (fn [ret [i r]]
                      (+ ret (/ (+ 1 i) (+ 1 r)))
                      ) 0 (map vector (range) (sort coll)))
            (count coll)))

  )
(defn k-precision [n coll]
  (float (/ (count (filter #(< % n) coll)) n))
  )
(defn k-recall [n coll]
  (float (/ (count (filter #(< % n) coll)) (count coll)))
  )
(defn rr [coll]
  (/ 1 (+ 1 (first (sort coll))))
  )

(defn IDCG [n]
  (apply +
         (for [i (range n)]
           (/ 1 (log2 (+ 2 i)))
           ))
  )
(defn nDCG [n coll]
  (/
    (apply +
           (for [i (filter #(<= % n) coll)]
             (/ 1 (log2 (+ 2 i)))
             ))
    (IDCG (min (count coll) n)))
  )

(defn pr-fn [coll]
  (let [dat (->>
              coll
              (sort)
              (map vector (range))
              (reduce (fn [ret [i rank]]
                        (conj ret [(float (/ (+ i 1) (count coll)))
                                   (float (/ (+ i 1) (+ rank 1)))])
                        ) [[0 1.0]]))]
    (fn [recall]
      (let [recall (float recall)]
        (loop [[[r1 p1] & [[r2 p2] & _ :as ns]] dat]
          (cond
            (= r1 recall)
            p1
            (= r2 recall)
            p2
            (< r1 recall r2)
            (+ (* (/ (- p2 p1) (- r2 r1)) (- recall r1)) p1)
            :default
            (recur ns)
            )
          )
        )
      )
    )
  )
(defn entropy [coll]

  (->>
    (m/esum coll)
    (m/div coll)
    (seq)
    (filter #(> % 0.0) )
    (map #(* % (Math/log %)))
    (apply - 0)
    )
  )



(defn plot-pr
  [n]
  (with-data (->> (to-dataset (flatten (for [{:keys [n results test-size]}
                                             (json/parse-string (slurp "/home/changun/Downloads/fixed.json") true)]
                                         (for [r (range 0.1 0.9 0.1)]
                                           (for [[method rank] results]
                                             {:method    (str method n)
                                              :n         n
                                              :p         ((pr-fn rank) r)
                                              :r         r
                                              :test-size test-size
                                              }
                                             )

                                           )
                                         )))
                  ($where {:n n})
                  ($rollup :mean :p [:method :r])

                  )
             (view (xy-plot :r :p :group-by :method :legend true))
             ))


(defn plot-corr-hist
  "Plot histogram of correlation between user's twitter profile and the article they liked"
  []
  (with-data (->> (to-dataset (for [[user corr]
                                    (json/parse-string (slurp "/home/changun/Downloads/corr.json") true)]
                                {:user user
                                 :corr corr}
                                ))

                  )
             (view (histogram :corr))
             ))


(defn plot-pr
  [n & [f]]
  (with-data (->> (to-dataset (flatten (for [{:keys [n results test-size pool-size]}
                                             (json/parse-string (slurp "/home/changun/Downloads/fixed.json") true)]
                                         (for [[method rank] results]
                                           (let [pr (pr-fn rank)]
                                             (for [r (range 0.1 1.0 0.1)]

                                               {:method    (str method n)
                                                :n         n
                                                :p         (* pool-size (pr r))
                                                :r         r
                                                :test-size test-size
                                                :pool-size pool-size
                                                })
                                             )
                                           ))))
                  ($where {:n n})
                  ($rollup (or f :mean) :p [:method :r])

                  )
             (view (xy-plot :r :p :group-by :method :legend true))
             ))

(defn summary
  [dat]
  (with-data (->> (to-dataset (flatten (for [{:keys [n results test-size pool-size]}
                                             dat]
                                         (for [[method rank] results]
                                           {:method    (str method n)
                                            :n         n
                                            :top10     (k-precision 10 rank)
                                            :top50     (k-precision 50 rank)
                                            :recall100 (k-recall 50 rank)
                                            :rr        (rr rank)
                                            :nDCG_10   (nDCG 10 rank)
                                            :nDCG_50   (nDCG 50 rank)
                                            :MAP       (MAP rank)
                                            :test-size test-size
                                            :pool-size pool-size
                                            }
                                           ))))

                  )
             (->> (aggregate [:top10 :top50 :rr :nDCG_10 :nDCG_50 :MAP :recall100] [:method :n] :rollup-fun stats/mean)
                  ($order [:n :method] :asec))

             ))







;25 {:avg 338.46143, :median 317}
;10 {:avg 226.19775, :median 217}

;25 {:avg 148, :median 317}
;10 {:avg 226.19775, :median 217}

(comment (let [users (into #{} users)
               days (->> (mc/find-maps medium-db "user-recommendations" {} ["_id" "recommendations.firstPublishedAt"])
                         (filter #(contains? users (:_id %)))
                         (map :recommendations)

                         (map #(map :firstPublishedAt %))
                         (map sort)
                         (map reverse)

                         (filter #(> (count %) 26))
                         (map (fn [rs] (/ (.getDays (Days/daysBetween (DateTime. (first rs)) (DateTime. (last rs)))) (count rs))))
                         )]
           (float (/ (apply + days) (count days)))
           ))