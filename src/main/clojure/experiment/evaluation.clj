(ns lifestreams-db.evaluation
  (:require [lifestreams-db.medium :as medium]
            [lifestreams-db.topic-model :as topics]
            [monger.collection :as mc]
            [lifestreams-db.twitter :as tw]
            [clojure.core.matrix :as m]
            [monger.core :as mg]
            [cheshire.core :as json]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [incanter.stats :as stats]
            [lifestreams-db.analysis :refer [MAP summary]]

            [lifestreams-db.ctr :as ctr]
            [lifestreams-db.postgres :refer [pooled-db]]
            [lifestreams-db.data :as data])
  (:use [incanter.core]
        [incanter.charts])
  (:import (org.joda.time DateTime Days)
           (librec.data SparseMatrix)
           (librec.rating RegSVD)
           (com.google.common.collect HashBasedTable)
           (happy.coding.io FileConfiger)

           (librec.intf Recommender)
           (librec.ranking BPR)))


(m/set-current-implementation :vectorz)

(def upvote-threshold 5)
(comment (defonce stories
                  (fn [& [begin end]]
                    (data/get-stories @pooled-db :start begin :end end :upvotes upvote-threshold)
                    )
                  )

         (defonce users
                  (->>
                    (data/get-people @pooled-db)
                    (map :medium_user_name)
                    (filter identity)
                    (filter #(>= (count (user->stories %)) 10))
                    )
                  )



         (defonce user->stories
                  (fn [user & [begin end]]
                    (data/get-user-medium-library @pooled-db user :start begin :end end :upvotes upvote-threshold)
                    )
                  )

         )

(defonce user->twitter
         (->>
           (data/get-people @pooled-db)
           (map #(vector (:medium_user_name %)
                         (:twitter_screen_name %)))
           (into {})
           ))
(defonce stories
         (let [stories (json/parse-string (slurp "stories.json"))]
           (fn [& [begin end]]
             (-> stories
                 (cond->>
                   begin
                   (drop-while #(< (first %) (.getMillis begin)))
                   end
                   (take-while #(< (first %) (.getMillis end)))
                   )
                 (->> (map second))
                 )
             )
           )
         )

(defonce users
         (->> (json/parse-string (slurp "user->ss.json"))
              (filter #(>= (count (second %)) 10)
                      )
              (map first)
              (into []))

         )



(defonce user->stories
         (let [user->ss (json/parse-string (slurp "user->ss.json"))
               user->ss (into {} user->ss)
               ]

           (fn [user & [begin end]]
             (-> (get user->ss user)
                 (cond->>
                   begin
                   (drop-while #(< (second %) (.getMillis begin)))
                   end
                   (take-while #(< (second %) (.getMillis end)))
                   )
                 (->> (map first))
                 )
             )
           )
         )
(comment (old 200 [10 16 65 104 137 171 145 13])
         (for 500 [6 10 16 58 77 311 356 361 401]))
(defn infer-topics [content]
  (apply topics/infer-topic-dist medium/model content 100 [8 21 189 193 41 167]))

(defonce infer-story-topics
         (memoize
           (fn [id]
             (infer-topics (:text (data/get-story-with-id @pooled-db id)))))
         )
(def infer-tweet-topics
  (memoize
    (fn [entity-name]
      (let [content (data/get-agg-tweets-with-entity-name @pooled-db entity-name)]
        (if content
          (infer-topics content))
        )
      ))
  )


(defonce enough-tweets?
         (memoize
           (fn [user]
             (let [tuser (if (= (first user) \@) user (str "@" user))
                   tweets (data/get-tweets-count-with-entity-name @pooled-db tuser)
                   friends (data/get-twitter-friends-with-entity-name @pooled-db tuser)
                   hashtags (data/get-hashtags-with-entity-name @pooled-db tuser)
                   friends-available (->> friends
                                          (pmap #(data/get-tweets-count-with-entity-name @pooled-db %))
                                          (filter #(>= % 50))
                                          (take 100)
                                          )
                   hashtags-available (->> hashtags

                                           (pmap #(data/get-tweets-count-with-entity-name @pooled-db %))
                                           (filter #(>= % 50))
                                           (take 100)
                                           )
                   ]
               (and (>= (+ (count friends-available) (count hashtags-available)) 100)
                    (>= tweets 100)
                    )

               )))

         )
(defonce user->twitter-profile
         (memoize (fn [user]
                    (if (enough-tweets? user)
                      (let [tuser (if (= (first user) \@) user (str "@" user))
                            friends @(future (data/get-twitter-friends-with-entity-name @pooled-db tuser))
                            hashtags @(future (data/get-hashtags-with-entity-name @pooled-db tuser))
                            instances (concat
                                        [{:topics (infer-tweet-topics tuser)
                                          :weight (data/get-tweets-count-with-entity-name @pooled-db tuser)}]
                                        (->> friends
                                             (pmap #(infer-tweet-topics %))
                                             (filter identity)
                                             (map (fn [%] {:topics % :weight 1}))
                                             )
                                        (->> hashtags
                                             (pmap #(infer-tweet-topics %))
                                             (filter identity)
                                             (map (fn [%] {:topics % :weight 0.5}))
                                             )

                                        )
                            topics (map :topics instances)
                            twitter-pref (m/mmul (map :weight instances) (m/matrix topics))
                            ]
                        (m/div twitter-pref (m/esum twitter-pref))
                        ))
                    ))

         )
(defn user->topics [user]
  (take 20 (reverse (sort-by second (map vector medium/topic-titles (user->twitter-profile (user->twitter user))))))
  )


(defn rank-stories
  "Return story->rank map where lower the rank value the higher the rank"
  [story-prefs stories]
  (let [ranked-stories (->> story-prefs
                            (map vector stories)
                            (sort-by second)
                            (reverse)
                            (map first)
                            )
        ]
    (into {} (map vector ranked-stories (range))))
  )

(defn rank-stories-with-inner-product
  "Given a pref vector, rank stories with the inner product"
  [pref stories]
  (let [stories-topics (m/matrix (map infer-story-topics stories))
        stories-pref (m/inner-product stories-topics pref)

        ]
    (rank-stories stories-pref stories))
  )

(defn user-story-pref [trainning-stories]
  (if-let [stories (seq trainning-stories)]
    (->>
      stories
      (map infer-story-topics)
      (apply m/add)
      (#(m/div % (m/esum %)))
      )
    )

  )


(defn user-user-twitter-rank [weight-fn {:keys [user twitter-pref others user->trainning-stories user->testing-stories testing-stories]}]
  (let [
        story->pref (->>
                      others
                      (map
                        (fn [u]
                          (if-let [story-pref (user-story-pref
                                                (concat (user->trainning-stories u)
                                                        (user->testing-stories u)))]
                            (let [weight (weight-fn twitter-pref story-pref)]
                              (for [s (user->testing-stories u)]
                                {s weight}
                                ))))
                        )
                      (filter identity)
                      (flatten)
                      (apply merge-with +)
                      )]
    (rank-stories (for [s testing-stories]
                    (or (story->pref s) 0))
                  testing-stories)

    )
  )
(defn user-user-twitter-jaccard [weight-fn {:keys [user twitter-pref others user->trainning-stories user->testing-stories testing-stories]}]
  (let [
        story->pref (->>
                      others
                      (map
                        (fn [u]
                          (let [u-stories (concat (user->trainning-stories u)
                                                  (user->testing-stories u))
                                story-pref (user-story-pref u-stories)
                                mine (into #{} (user->trainning-stories user))
                                you (into #{} (user->trainning-stories u))
                                jaccard (/ (count (clojure.set/intersection mine you))
                                           (count (clojure.set/union mine you))
                                           )]
                            (if story-pref
                              (let [weight (* (weight-fn twitter-pref story-pref) jaccard)]
                                (for [s (user->testing-stories u)]
                                  {s weight}
                                  )))
                            ))
                        )
                      (filter identity)
                      (flatten)
                      (apply merge-with +)
                      )]
    (rank-stories (for [s testing-stories]
                    (or (story->pref s) 0))
                  testing-stories)

    )
  )

(defn user-user-twitter-ctr [{:keys [user others user->trainning-stories user->testing-stories tranning-stories testing-stories]}]
  (let [users (cons user (filter (comp user->twitter-profile user->twitter) others))
        _ (println "CTR People:" (count users))
        user->id (into {} (map vector users (range)))
        ; all the story in the matrix
        all-stories (concat tranning-stories testing-stories)
        story->id (into {} (map vector all-stories (range)))

        ; add all other users' both tranning and testing stories
        story->users (->> (for [u users]
                            (let [uid (user->id u)]
                              (for [s (concat (user->trainning-stories u)
                                              (user->testing-stories u))]
                                {(story->id s) [uid]}
                                ))
                            )
                          (flatten
                            )
                          (apply merge-with concat))


        Theta (pmap infer-story-topics all-stories)
        Gamma (pmap #(if (enough-tweets? (user->twitter %))
                      (user->twitter-profile (user->twitter %))
                      (repeat (count medium/topic-titles) (/ 1 (count medium/topic-titles)))
                      ) users)


        [U V] (ctr/estimate (count users)
                            Theta
                            story->users
                            1
                            0.01
                            10
                            10
                            5
                            nil
                            Gamma
                            )
        u (m/get-row U 0)
        V-for-testing (drop (count tranning-stories) V)

        ]
    (rank-stories (map m/scalar (m/inner-product V-for-testing u))
                  testing-stories)

    )
  )


(defn user-user-base [weight-fn {:keys [user others user->trainning-stories user->testing-stories tranning-stories testing-stories]}]
  (let [
        my-stories (into #{} (user->trainning-stories user))

        all-prev-stories (into #{} tranning-stories)
        story->pref (->>
                      others
                      (map
                        (fn [u]
                          (let [user-prev-stories (into #{} (user->trainning-stories u))
                                user-testing-stories (user->testing-stories u)
                                weight (if (seq user-testing-stories)
                                         (weight-fn my-stories user-prev-stories all-prev-stories))]
                            (if weight
                              (for [u user-testing-stories]
                                {u weight}
                                )
                              )
                            )
                          )
                        )
                      (filter identity)
                      (flatten)
                      (apply merge-with +)
                      (p :merge)

                      )]
    (p :rank (rank-stories (for [s testing-stories] (or (get story->pref s) 0)) testing-stories))

    )
  )
(def user-user-jaccard
  (partial user-user-base (fn [my-stories user-stories _]
                            (if (seq user-stories)
                              (/ (count (clojure.set/intersection my-stories user-stories))
                                 (count (clojure.set/union my-stories user-stories))
                                 )
                              )
                            )
           )
  )
(def user-user-corr
  (partial user-user-base (fn [my-stories user-stories all-stories]
                            (let [n11 (count (clojure.set/intersection my-stories user-stories))
                                  n00 (count (clojure.set/difference all-stories my-stories user-stories))
                                  n10 (count (clojure.set/difference my-stories user-stories))
                                  n01 (count (clojure.set/difference user-stories my-stories))
                                  n1x (count my-stories)
                                  nx1 (count user-stories)
                                  n0x (- (count all-stories) n1x)
                                  nx0 (- (count all-stories) nx1)
                                  ]
                              (if (and (not= 0 (* n1x nx1 n0x nx0)))
                                (/ (- (* n11 n00) (* n01 n10))
                                   (Math/sqrt (* n1x nx1 n0x nx0)
                                              )

                                   )
                                )

                              ))
           ))
(defn most-popular [{:keys [others user->testing-stories testing-stories]}]
  (let [
        story->pref (->>
                      (for [u others]
                        (user->testing-stories u)
                        )
                      (flatten)
                      (frequencies))]
    (rank-stories (for [s testing-stories] (or (get story->pref s) 0))
                  testing-stories)

    )
  )
(defn similar-to-twitter [{:keys [testing-stories twitter-pref]}]
  (rank-stories-with-inner-product twitter-pref testing-stories)
  )

(defn similar-to-prev [{:keys [user user->trainning-stories testing-stories]}]
  (let [pref (user-story-pref (user->trainning-stories user))]
    (rank-stories-with-inner-product pref testing-stories)
    )
  )
(def matrix-cache (atom {}))
(defn user-item-matrix [user others user->trainning-stories user->testing-stories tranning-stories testing-stories]
  (if-let [cache (:m (get @matrix-cache [user (count (user->trainning-stories user))]))]
    (do (println "Use cached matrix.")
        cache
        )
    (let [; add all other users' both tranning and testing stories
          their-stories (->>
                          others
                          (map (fn [u] (concat (user->trainning-stories u)
                                               (user->testing-stories u))))
                          (filter seq)
                          )
          ; only add the user's tranning stories into the matrix
          my-trainning-stories (user->trainning-stories user)
          users-stories (cons my-trainning-stories their-stories)
          ; all the story in the matrix
          all-stories (concat tranning-stories testing-stories)
          story->id (into {} (map vector all-stories (range)))
          _ (println (format "Create %sx%s SparseMatrix" (count users-stories) (count all-stories)))
          ; create a hash table used by RegSVD
          t (HashBasedTable/create)
          _ (doseq [[uid stories] (map vector (range) users-stories)]
              (doseq [story-id (map story->id stories)]
                (.put t (int uid) (int story-id) (double 1.0))
                )
              )
          m (SparseMatrix. (int (count users-stories)) (int (count all-stories)) t)
          ]

      (swap! matrix-cache
             assoc
             [user (count (user->trainning-stories user))]
             {:time (DateTime.) :m m})
      m
      ))
  )
(defn rating-based [train-model {:keys [user others user->trainning-stories user->testing-stories tranning-stories testing-stories]}]
  (let [; all the story in the matrix
        all-stories (concat tranning-stories testing-stories)
        story->id (into {} (map vector all-stories (range)))
        m (user-item-matrix user others user->trainning-stories user->testing-stories tranning-stories testing-stories)
        ; predict the ratings of all the testing stories by the user (the user is at the bottom of the matrix)
        model (train-model m)

        ]
    (rank-stories
      (for [sid testing-stories]
        (.ranking model 0 (story->id sid))
        )
      testing-stories
      )
    )
  )
(defn RegSVD-based [factors]
  (partial rating-based (fn [m]
                          (let [
                                model (RegSVD. (doto (FileConfiger. "config/RegSVD.conf")
                                                 (.setString "num.factors" (str factors))) m m 10)]
                            (println (str model))
                            (.trainModel model)
                            (println "RegSVD Finished")
                            ; predict
                            model
                            )))
  )
(defn BPR-based [factors]
  (partial rating-based (fn [m]
                          (let [
                                model (BPR. (doto (FileConfiger. "config/BPR.conf")
                                              (.setString "num.factors" (str factors)))
                                            '(0.0 1.0)
                                            m m 10)]
                            (println (str model))
                            (.trainModel model)
                            (println "BPR Finished")
                            ; predict
                            model
                            )))
  )

(defn twitter-KNN [weight-fn K {:keys [user others user->trainning-stories user->testing-stories testing-stories]}]
  (let [twitter-pref (user->twitter-profile (user->twitter user))

        KNN (->>
              others
              (pmap
                (fn [u]
                  (let [u-stories (concat (user->trainning-stories u)
                                          (user->testing-stories u))

                        story-pref (user-story-pref u-stories)]
                    (if story-pref
                      [u (weight-fn twitter-pref story-pref)]
                      )
                    ))
                )
              (filter identity)
              (sort-by second)
              (reverse)
              (take K)
              (map first)
              )
        story->pref (->>
                      KNN
                      (mapcat
                        (fn [u]
                          (user->testing-stories u)
                          )
                        )
                      (frequencies)
                      )
        ]
    (rank-stories (for [s testing-stories]
                    (or (story->pref s) 0))
                  testing-stories)

    )
  )
(defn match-criteria? [n user]
  (if-let [trainning (seq (take n (user->stories user)))
           ]
    (let [start (:published_at (data/get-story-with-id @pooled-db (last trainning)))
          start (.plusMillis start 1)
          end (.plusMonths start 3)
          ]
      (if (and (>= (count (user->stories user start end)) 2)
               (.isAfter start (DateTime. 2013 5 1 0 0))
               (enough-tweets? (user->twitter user))
               )
        [start end (count (user->stories user start end))])
      )

    )
  )
(defn evaluate [user first-n fns]
  (try
    (when (match-criteria? first-n user)
      (println "User: " first-n user)
      (let [trainning (seq (take first-n (user->stories user)))
            _ (reset! matrix-cache {})

            start (:published_at (data/get-story-with-id @pooled-db (last trainning)))
            start (.plusMillis start 1)
            end (.plusMonths start 3)

            user->trainning-stories (memoize #(user->stories % nil start))
            user->testing-stories (memoize #(user->stories % start end))
            others (->> (disj (into #{} users) user)
                        (filter (fn [u] (seq (concat (user->trainning-stories u)
                                                     (user->testing-stories u))))))

            tranning-stories (stories (.minusMonths start 8) (.plusMillis start 1))
            testing-stories (stories start end)
            twitter-pref (user->twitter-profile (user->twitter user))]
        (println (format "Start %s End %s Trainning %s(%s) Testing %s(%s) People %s User %s"
                         start end
                         (count tranning-stories) (count (user->trainning-stories user))
                         (count testing-stories) (count (user->testing-stories user))
                         (inc (count others))
                         user
                         ))
        (let [results (->> fns
                           (pmap (fn [[name rec]]
                                   (rec {:user                    user
                                         :others                  others
                                         :user->trainning-stories user->trainning-stories
                                         :user->testing-stories   user->testing-stories
                                         :tranning-stories        tranning-stories
                                         :testing-stories         testing-stories
                                         :twitter-pref            twitter-pref
                                         })))
                           (map #(map %1
                                      (user->testing-stories user)))
                           (map #(vector %1 %2) (keys fns))
                           (into {})
                           )]

          (println (map #(vector (first %) (MAP (second %))) results))
          {:user          user
           :results       results
           :test-size     (count (user->testing-stories user))
           :tranning-size (count (user->trainning-stories user))
           :pool-size     (count testing-stories)
           :start         start
           :end           end
           :n             first-n}
          )

        ))
    (catch Exception e (clojure.stacktrace/print-cause-trace e)))

  )

(defonce results-atom (atom []))
(def old-results (json/parse-string (slurp "fixed.json") true))
(defn try-new [fns]
  (map
    (fn [{:keys [n user results] :as row} index]
      (try (let [new-results (:results (evaluate user n fns))]
             (swap! results-atom (fn [all-rows]
                                   (assoc (into [] all-rows)
                                     index
                                     (assoc row :results (merge results new-results)))
                                   ))

             (println (summary @results-atom))
             )
           (catch Exception e (clojure.stacktrace/print-cause-trace e)))
      )
    @results-atom (range)
    )
  )





(defn output-results [filename results]
  (spit filename (json/generate-string (map (fn [%] (assoc % :start (str (:start %)) :end (str (:end %)))) results)))
  )

(def fns {:most-popular          most-popular
          :twitter-user-user     (partial user-user-twitter-rank stats/correlation)
          :twitter-with-jaccard  (partial user-user-twitter-jaccard stats/correlation)
          :twitter-ctr           user-user-twitter-ctr
          :jaccard               user-user-jaccard
          ;:corr                  user-user-corr
          :content-based         similar-to-prev
          :twitter-content-based similar-to-twitter
          :twitter-KNN-200       (partial twitter-KNN stats/correlation 200)
          ;:BRP-100 (BPR-based 200)
          })
(defn run []
  (let [tasks (for [first-n [1 5 10 15 20 25 30 35 40 45 50]
                    trials (range 50)]
                [first-n trials])]

    (map
      (fn [[first-n trials]]
        (let [ret (loop []
                    (if-let [ret (evaluate (rand-nth (into [] users)) first-n fns)]
                      ret (recur))
                    )]
          (swap! results-atom conj ret)
          (println (summary @results-atom))
          (println first-n "/" trials)
          ret
          ))
      tasks)
    )
  )


