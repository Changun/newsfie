(ns new-model.eval-meetup-rec
  (:require [lifestreams-db.meetup-crawler :as crawler]
            [lifestreams-db.meetup :as meetup]
            [new-model.eval :as eval]
            [lifestreams-db.twitter :as tw]
            [new-model.eval-meetup :as eval-meetup]
            [clojure.core.matrix :as m]
            [lifestreams-db.ctr :as ctr]
            [cheshire.core :as json]
            [cheshire.generate :refer [add-encoder encode-seq remove-encoder]])
  (:import (mikera.vectorz.impl AArrayVector)))


(def results
  (let [group->users (into {} (crawler/group->users))
        groups (map first group->users)
        [testing-groups training-groups] (split-at 2000 (eval/deterministic-shuffle groups))

        user-record->user (fn [user]
                            (let [{:keys [id other_services] :as u} user
                                  twitter (->> (:twitter other_services)
                                               :identifier)]
                              {:id id
                               :twitter (if (= \@ (first twitter))
                                          (-> twitter
                                              (clojure.string/replace " " "")
                                              (clojure.string/replace #"@+" "@")
                                              tw/normalize-screen-name
                                              ))
                               }))
        users->testing-groups
        (->> (for [group testing-groups]
               (->>
                 (group->users group)
                 (map user-record->user)
                 (map (fn [u] {u [group]}))

                 )
               )
             (apply concat)
             (apply merge-with concat))
        users->training-groups
        (->> (for [group training-groups]
               (->>
                 (group->users group)
                 (map user-record->user)
                 (map (fn [u] {u [group]}))

                 )
               )
             (apply concat)
             (apply merge-with concat))

        users (distinct
                (concat (keys users->testing-groups)
                        (keys users->training-groups)))

        ; 36619 people with twitter data
        twitter-users (eval/deterministic-shuffle (filter (comp eval/entity-avail? :twitter) users))
        testing-users (->> twitter-users
                           (filter (fn [u]
                                     (and (>= (count (users->training-groups u)) 5)
                                          (>= (count (users->testing-groups u)) 1))
                                     ))
                           (take 5000)
                           )
        training-twitter-user (let [testing? (into #{} testing-users)]
                                (filter (complement testing?) twitter-users)
                                )

        training-users (take 100000 (distinct (concat training-twitter-user (eval/deterministic-shuffle users))))
        training-users? (into #{} training-users)
        user->content (reduce (fn [ret u-rec]
                                (let [u (user-record->user u-rec)]
                                  (cond->
                                    ret
                                    (and (training-users? u) (not (ret u)))
                                    (assoc u (meetup/user->content u-rec))
                                    ))
                                )
                              {}
                              (mapcat second group->users))

        user-group (->> (concat users->training-groups users->testing-groups)
                        (filter (comp training-users? first))
                        (mapcat (fn [[u groups]]
                                  (for [g groups]
                                    [u g])
                                  )))
        Theta (into {} (pmap #(vector % (new-model.eval-meetup/group->topic :multi-200 (name %))) groups))
        Gamma (into {} (pmap #(vector % (if (eval/entity-avail? (:twitter %))
                                          (eval/entity-name->profile :multi-200 (:twitter %))
                                          (eval-meetup/user->topic :multi-200 (:id %) (user->content %))
                                          )) training-users))


        [_ pmf]
        (ctr/estimate
          (into {} (pmap #(vector % (m/zero-vector 200)) groups))
          (into {} (pmap #(vector % (m/zero-vector 200)) training-users))
          user-group
          1
          0.01
          0.01
          0.01
          5
          )
        [_ ours]
        (ctr/estimate Theta
                      Gamma
                      user-group
                      1
                      0.01
                      10
                      10
                      10
                      )


        [_ ctr]
        (ctr/estimate Theta
                      (into {} (pmap #(vector % (m/zero-vector 200)) training-users))
                      user-group
                      1
                      0.01
                      10
                      0.01
                      5
                      )



        ]
    {:ours ours
     :ctr ctr
     :pmf pmf
     :theta Theta
     :gamma Gamma
     :traning-groups training-groups
     :testing-groups training-groups
     :testing-users testing-users
     :training-users training-users
     :user->training users->training-groups
     :user->testing users->testing-groups
     }


    ))




(with-open [w (clojure.java.io/writer "meetup-rec-eval.edn")]
  (binding [*print-length* false
            *out* w]
    (pr (reduce (fn [ret k]
                  (assoc ret k (into {} (map #(assoc % 1 (seq (second %)))) (ret k)))
                  )
                results
                [:ours :ctr :pmf :theta :gamma]))))


(add-encoder AArrayVector encode-seq)
(with-open [w (clojure.java.io/writer "eval-meetup-rec.json")]
  (json/generate-stream results
                         w))
