<<<<<<< HEAD
(ns new-model.model
  (:require
    [primitive-math :as prim]
    [hiphip.double :as dbl])
  (:import (java.util Arrays)))
=======
(ns new-model.model)
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d


(defprotocol TopicModel
  (update-local-params [model token old-word-params  new-word-params local-params])
<<<<<<< HEAD
  (update-corpus-params [model token old-word-params  new-word-params global-params])
  (update-global-params [model token old-word-params  new-word-params global-params])
  (update-local-cache [model token old-word-params  new-word-params  local-cache local-params corpus-params global-params])
  (init-global-params [model])
  (init-corpus-params [model])
  (init-local-params [model])
  (init-local-cache [model local-params corpus-params global-params])
  (empty-word-params [model])
  (diagnose [model corpus->corpus-params global-params])
  (sample [model token local-cache  local-params corpus-params global-params])
  )


(defn assoc-but-zero [m key value]
  (if (= 0 value)
    (dissoc m key)
    (assoc m key value)
    )
  )

(defn position-with-fn [^double sample ^longs cases ^doubles weights]
  (loop [i 0 sample sample]
    (let [w (aget weights i)]
      (if (prim/>=  w sample )
        (aget cases i)
        (recur (inc i) (prim/- sample w))
        )
      )

    )
  )
(defn uniform-binary-search [^double sample cases ^doubles cum-weights]
   (let [pos (Arrays/binarySearch cum-weights sample)
         pos (if (< pos 0)
               (- (+  pos 1))
               pos
               )
         ]
     (nth cases pos))

  )

(defn uniform-sample-lazy [^double sample cases weights]
  (loop [sample sample [^long case & cases] cases [^double weight & weights] weights]
    (if (prim/>=  weight sample )
      case
      (recur (prim/- sample weight) cases weights)
      )
    )
  )



(defn update-topic-map-dis-zero
  [data
   old-topic
   new-topic
   ]
  (if (not= old-topic new-topic)
    (cond->
      data
      old-topic
      (assoc-but-zero old-topic (prim/dec ^long (data old-topic)))
      new-topic
      (assoc new-topic (inc (or (data new-topic) 0)))
      )
    data
    ))
(defn close? [^double x ^double y]
  (< (Math/abs (- x y)) 0.0000001))
=======
  (update-global-params [model token old-word-params  new-word-params global-params])
  (update-local-cache [model token old-word-params  new-word-params  local-cache local-params global-params])
  (init-global-params [model])
  (init-local-params [model])
  (init-local-cache [model local-params global-params])
  (empty-word-params [model])
  (diagnose [model global-params])
  (sample [model token local-cache  local-params global-params])
  )


>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
