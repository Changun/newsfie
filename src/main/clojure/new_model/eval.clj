(ns new-model.eval

  (:require [clojure.core.matrix :as m]
            [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :as postgres]
            [new-model.models :as models]
            [lifestreams-db.data :as data]
            )
  (:require [taoensso.nippy :as nippy])
  (:import (java.util ArrayList Random Collections Collection)
           (clojure.lang RT)
           (java.io DataOutputStream DataInputStream)))

(m/set-current-implementation :vectorz)

(defn deterministic-shuffle
  ([coll] (deterministic-shuffle coll 1))
  ([^Collection coll seed]
   (let [al (ArrayList. coll)
         rng (Random. seed)]
     (Collections/shuffle al rng)
     (RT/vector (.toArray al)))))
(defn profile [model-name entity  text type]
  (let [cache (first (sql/query
                       @postgres/pooled-db
                       ["SELECT topics from profile_cache where model=? AND type=? AND entity=?" (name model-name) type (str entity)]))]
    (if cache
      (:topics cache)
      (let [topics ((models/models model-name) text type)]
        (future (sql/insert!
                  @postgres/pooled-db
                  "profile_cache"
                  {:model (name model-name)
                   :type type
                   :entity (str entity)
                   :topics (seq topics)}))
        topics
        )
      )
    )
  )

(def entity-avail?
(let [_ (comment (->> (sql/query
                        @postgres/pooled-db
                        ["SELECT name, id FROM twitter_entity_text inner join twitter_entity on id=entity_id"])
                      (map #(vector (:name %) (:id %)))
                      (into {})))
      ]
  (with-open [r (clojure.java.io/input-stream "entity-name->entity-id.bin")]
    (nippy/thaw-from-in! (DataInputStream. r)))
  )
  )


(def entity->topics-count
  (memoize
    (fn [model-name entity]
      (let [{:keys [text c]}
            (first (sql/query
                     @postgres/pooled-db
                     ["SELECT text, count as c FROM twitter_entity_text where entity_id=?" entity]))]
        (if text
          [(profile model-name entity text "Twitter") c]
          [nil 0])
        )
      )))


(defn entity-name->topics-count
  [profile-fn name]

  (entity->topics-count profile-fn (entity-avail? name)))

(defn entities->topic-sum [profile-fn entities]
  (->> entities
       (filter identity)
       (map #(entity->topics-count profile-fn %))
       (filter first)
       (map first)
       (apply m/add)
       )
  )


(defn norm [ma]
  (m/div ma (m/esum ma))
  )



(defn scores->relavant-ranks [scores n]
  (->> (map #(array-map :score %1 :index %2) scores (range))
       (sort-by :score)
       (reverse)
       (map #(assoc %2 :rank %1) (range))
       (filter #(< (:index %) n))
       (map :rank)
       ))

(defn entity-name->profile [profile-fn entity_name]
  (let [hashtags  (take 300 (deterministic-shuffle (filter identity (map entity-avail? (data/get-twitter-friends-with-entity-name @postgres/pooled-db entity_name)))))
        followees (take 300 (deterministic-shuffle (filter identity (map entity-avail? (data/get-hashtags-with-entity-name @postgres/pooled-db entity_name)))))
        [topics own-count] (entity-name->topics-count profile-fn entity_name)]
    (->
      (m/add (m/mul 0.2 (or (entities->topic-sum profile-fn hashtags) 0))
             (or (entities->topic-sum profile-fn followees) 0)
             (m/mul topics own-count)
             )
      (norm)
      (m/to-vector))
    )
  )
