(ns new-model.fast-lda
  (:use [new-model.model]
        [new-model.utils]

        )
<<<<<<< HEAD
=======

>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
  (:require [new-model.model :refer [TopicModel]]
            [primitive-math :as prim]
            [hiphip.double :as dbl]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)])

  )

<<<<<<< HEAD


=======
(defn assoc-but-zero [m key value]
  (if (= 0 value)
    (dissoc m key)
    (assoc m key value)
    )
  )

(defn position-with-fn [^double sample ^longs cases ^doubles weights]


  (loop [i 0 sample sample]
    (let [w (aget weights i)]
      (if (prim/>=  w sample )
        (aget cases i)
        (recur (inc i) (prim/- sample w))
        )
      )

    )


  )

(defn update-topic-map-dis-zero
  [data
   old-topic
   new-topic
   ]
  (if (not= old-topic new-topic)
    (cond->
      data
      old-topic
      (assoc-but-zero old-topic (prim/dec ^long (data old-topic)))
      new-topic
      (assoc new-topic (inc (or (data new-topic) 0)))
      )
    data
    ))

(defn close? [^double x y]
  (< (Math/abs (- x y)) 0.0000001))
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d

(defrecord FastLDA [T W alpha beta index->token]
  TopicModel
  (empty-word-params [model]
    nil
    )
  (init-global-params [model]
    [
     ; word->topic->count
     (into [] (repeat W {}))
     ; topic->count
     (into [] (repeat T (* W beta)))
     ]
    )

  (init-local-params [model]
    ; local-topic->count
    [{}]
    )
  (init-local-cache [model [local-topic->count] [_ topic->count]]
    [
     ; s
     (* beta
        alpha
        (->> (range T)
             (map (fn [topic] (/ 1 (topic->count topic))))
             (apply + )
             )
        )
     ; r
     (* beta
        (->> local-topic->count
             (map (fn [[topic cnt]]
                    (/ cnt  (topic->count topic))))
             (apply +)
             )
        )
     ; q-coefficients
     (mapv
       (fn [t]
         (/ (+ alpha (or (local-topic->count t) 0))
            (topic->count t)
            ))
       (range T))
     ]
    )

  (update-global-params [model word old new [word->topic->count topic->count]]
    [
     ; update word->topic->count
     (assoc word->topic->count
       word
       (update-topic-map-dis-zero (word->topic->count word) old new))
     ; update topic->count
     (update-topic-map topic->count old new)
     ]
    )

  (update-local-params [model word old new [topic->count]]
    [(update-topic-map-dis-zero topic->count old new)]
    )

  (update-local-cache [model token old new [s r q-coefficients] [local-topic->count] [_ topic->count] ]
    (if (= old new)
      [s r q-coefficients]
      (let [orig-old-topic-local-count (if old (or (local-topic->count old) 0))
            orig-new-topic-local-count (if new (or (local-topic->count new) 0))
            beta-mul-alpha (* beta alpha)
            orig-old-topic-count (if old (topic->count old))
            orig-new-topic-count (if new (topic->count new))

            ]
        [(cond->
           s
           old
           (-> (- (/ beta-mul-alpha
                     orig-old-topic-count)
                  )
               (+ (/ beta-mul-alpha
                     (- orig-old-topic-count 1)))
               )
           new
           (-> (- (/ beta-mul-alpha
                     orig-new-topic-count)
                  )
               (+ (/ beta-mul-alpha
                     (+ orig-new-topic-count 1)))
               )
           )
         (cond->
           r
           old
           (-> (- (/ (* orig-old-topic-local-count beta)
                     orig-old-topic-count)
                  )
               (+ (/ (* (- orig-old-topic-local-count 1) beta)
                     (- orig-old-topic-count 1)))
               )
           new
           (-> (- (/ (* orig-new-topic-local-count beta)
                     orig-new-topic-count)
                  )
               (+ (/ (*
                       (+ orig-new-topic-local-count 1) beta)
                     (+ orig-new-topic-count 1)))
               )
           )
         (cond->
           q-coefficients
           old
           (assoc old
                  (/ (- (+ alpha orig-old-topic-local-count)  1)
                     (- orig-old-topic-count 1)
                     )
                  )
           new
           (assoc new
                  (/ (+ (+ alpha orig-new-topic-local-count) 1)
                     (+ orig-new-topic-count 1)
                     )
                  )
           )
         ]
        ))



    )

  (sample [model word [s r q-coefficients] [local-topic->count]  [word->topic->count topic->count]]

    (let [current-word->topic->count (word->topic->count word)
          q-topics (keys current-word->topic->count)
          q-weight (map #(* (q-coefficients (first %)) (second %)) current-word->topic->count)
          sum_q (apply + q-weight)
          mass   (+ s r sum_q)
          sample (* (rand) mass)
          ]
      (comment (assert (close? (apply + q_weights)
                               (apply + (for [t (range T)]
                                          (/ (* (+ (or (local-topic->count t) 0) alpha)
                                                (or (current-word->topic->count t) 0))
                                             (topic->count t)
                                             )
                                          ))
                               )))
      (cond
        (< sample sum_q)
        (position-with-fn sample q-topics q-weight)

        (< sample (+ sum_q r))
        (let [sample (/ (- sample sum_q) beta)]
          (position-with-fn
            sample
            (keys local-topic->count)
            (map (fn [[topic cnt]] (/ cnt (topic->count topic))) local-topic->count)
            )
          )
        :default
        (let [sample (/ (- sample sum_q r) (* beta alpha))
              cases (range T)
              weights (map (fn [topic]
                             (/ 1  (topic->count topic))
                             )
                           cases)]
          ;(comment (assert (close? (apply + weights) (/ s (* beta alpha)))))
          (position-with-fn sample cases weights)
          )
        )


      )
    )
  (diagnose [model [word->topic->count topic->count]]
    (->>
      (for [[word-index topic-counts] (map vector (range) word->topic->count)]
        (for [[topic-index count] topic-counts]
          {:topic topic-index :word word-index :count count}
          )

        )
      (flatten)
      (group-by :topic)
      (sort-by first)
      (pmap (fn [[t ws]]
              (let [keywords (map (comp index->token :word) (take 4 (reverse (sort-by :count ws))))]
                (str t ":" (clojure.string/join "," keywords))
                )))
      (clojure.string/join "\n")
      (println)
      )
    )

  )


