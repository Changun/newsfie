(ns new-model.models
  (:require [lifestreams-db.topic-model :as topics]
            [clojure.core.matrix :as m]
            [org.httpkit.client :as http]
            [cheshire.core :as json])
  (:import (cc.mallet.topics TopicInferencer ParallelTopicModel MultiBackgroundTopicModel)
           (cc.mallet.types InstanceList Instance)))

(defn norm [ma]
  (m/div ma (m/esum ma))
  )
(defn infer [model text source]

  (let [^TopicInferencer inferer (if (= MultiBackgroundTopicModel (class model))
                                   (.getInferencer  model source)
                                   (.getInferencer  model)
                                   )
        freezed-alphabet (doto
                           (.alphabet model)
                           (.stopGrowth))
        instance-list (InstanceList. (topics/full-pipe nil freezed-alphabet))]
    (do (.addThruPipe instance-list (Instance. text nil "testing instance", nil))
        (-> (.getSampledDistribution inferer (nth instance-list 0) 100 1 5)
            (cond-> (= MultiBackgroundTopicModel (class model))
                    (butlast))
            (norm))
        )
    ))
(defn file->profile-fn [file]
  (let [model (topics/load-model file)]
    (fn multi [text source]
          (infer model text source)
      ))
  )

(defn create-tf-idf-model [model-file]

  )
(defn doc2vec-profile-fn [size source]
  (fn multi [text _]
    (->> @(http/post "http://localhost:8080"
                     {:body (json/generate-string {:size size :text text :source source})
                      :timeout 10000})
         :body
         (slurp)
         (json/parse-string)
         (m/as-vector)
         (norm))
    )
  )






;(def meetup-200 (file->profile-fn "meetup-groups.bin"))

              (comment
                :multi-50  (file->profile-fn "model-2015-10-05T09:25:41.670Z-50-multi.bin")

                :medium-50 (file->profile-fn "model-2015-10-05T12:16:54.148Z-50-medium.bin")
                :multi-300 (file->profile-fn "model-2015-09-16T19:37:05.188Z-300.bin")
                          :multi-500 (file->profile-fn "model-multi-500.bin")
                          :medium-300 (file->profile-fn "model-2015-09-17T22:58:21.082Z-300-orig.bin")
                          :medium-500 (file->profile-fn "model-2015-09-18T02:43:28.818Z-500-orig.bin"))
(def models
  {:multi-200 (file->profile-fn "model-2015-09-17T03:24:53.366Z-200.bin")
   :multi-400 (file->profile-fn "model-2015-09-17T00:23:33.822Z-400.bin")
  :multi-100 (file->profile-fn "model-2015-10-05T11:24:01.825Z-100-multi.bin")



   :medium-100 (file->profile-fn "model-2015-10-05T13:20:53.771Z-100-medium.bin")
   :medium-200 (file->profile-fn "model-2015-09-17T19:45:36.598Z-200-orig.bin")
   :medium-400 (file->profile-fn "model-2015-09-18T00:48:00.181Z-400-orig.bin")

   :meetup-100  (file->profile-fn "model-2015-10-05T13:34:14.572Z-100-meetup.bin")
   :meetup-200  (file->profile-fn "model-2015-10-05T13:41:41.372Z-200-meetup.bin")
   :meetup-400  (file->profile-fn "model-2015-10-05T13:49:17.665Z-400-meetup.bin")

   :doc2vec-50 (doc2vec-profile-fn 50 "News")
   :doc2vec-100 (doc2vec-profile-fn 100 "News")
   :doc2vec-200 (doc2vec-profile-fn 200 "News")
   :doc2vec-400 (doc2vec-profile-fn 400 "News")


   :meetup-doc2vec-100 (doc2vec-profile-fn 100 "Meetup")

   :meetup-doc2vec-200 (doc2vec-profile-fn 200 "Meetup")
   :meetup-doc2vec-400 (doc2vec-profile-fn 400 "Meetup")


   })







