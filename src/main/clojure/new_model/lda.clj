(ns new-model.lda
  (:use [new-model.model]
        [new-model.utils])
  (:require [new-model.model :refer [TopicModel]])

  )


(defrecord LDA [T W alpha beta index->token]
  TopicModel
  (init-local-cache  [model local global]
    nil)
  (init-local-params [model]
    ; topic->count
    [(into [] (repeat T alpha))]
    )
  (init-global-params [model]
    [; word->topic->count
     (into [] (repeat W (into [] (repeat T beta))))
     ; topic->count
     (into [] (repeat T (* W beta)))
     ]
    )
  (empty-word-params [model]
    nil)
  (update-local-cache [model token old-word-params  new-word-params  local-cache local-params global-params]
    nil
    )
  (update-local-params [model word old new [topic->count]]
    [(update-topic-map topic->count old new)]
    )
  (update-global-params [model word old new [word->topic->count topic->count]]
    (try
      [(assoc word->topic->count word
                                 (update-topic-map (nth word->topic->count word) old new))
       (update-topic-map topic->count old new)]
      (catch Exception e (println (count topic->count) old new)
                         (throw e)))
    )


  (sample [model word _ [local-topic->count] [word->topic->count topic->count]]
    (let [current-word->topic->count (word->topic->count word)
          weights (for [topic (range T)]
                  (/ (* (current-word->topic->count topic)
                        (local-topic->count topic)
                        )
                     (topic->count topic)
                     )
                  )
          mass (apply + weights)
          sample (* (rand) mass)
          cases (range T)
          ]
         (position sample cases weights)
        )
      )
  (diagnose [model [word->topic->count _]]
    (->>
      (for [[word-index topic-counts] (map vector (range) word->topic->count)]
        (for [[topic-index count] (map vector (range) topic-counts)]
          {:topic topic-index :word (index->token word-index) :count count}
          )

        )
      (flatten)
      (group-by :topic)
      (sort-by first)
      (map (fn [[t ws]] (take 5 (reverse (sort-by :count ws)))))
      )
    )

  )



