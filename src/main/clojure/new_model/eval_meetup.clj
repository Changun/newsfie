(ns new-model.eval-meetup
  (:require
    [clojure.core.reducers :as r]
    [clojure.core.matrix :as m]
    [cheshire.core :as json]
    [new-model.eval :as eval]
    [lifestreams-db.meetup :as meetup]
    [experiment.analysis :as analysis]
    [incanter.stats :as stats]))


(def eval-info
  (with-open [wrd (clojure.java.io/reader "meet-eval.json")]
    (json/parse-stream wrd true)
    )
  )


(def gid->group (:groups eval-info))




(defn user->pref [profile-fn [entity_id {:keys [followees hashtags]}] pref-source]
  (let [hashtags  (take 300 (eval/deterministic-shuffle hashtags))
        followees (take 300 (eval/deterministic-shuffle followees))
        [topics own-count] (eval/entity-name->topics-count profile-fn entity_id)]
    (->>

      (cond-> []
              (pref-source :own)
              (conj (m/mul topics own-count))
              (pref-source :hashtags)
              (conj (m/mul (pref-source :hashtags) (eval/entities->topic-sum profile-fn hashtags)))
              (pref-source :followees)
              (conj (m/mul (pref-source :followees) (eval/entities->topic-sum profile-fn followees)))
              )

      (apply m/add)
      (eval/norm)
      (m/to-vector))
    )
  )
(def group->topic
         (memoize
           (fn [profile-fn id]
             (eval/profile profile-fn id (meetup/group->content (gid->group  id)) "Meetup")
             )))

(def user->topic
  (fn [profile-fn id text]
    (eval/profile profile-fn (str "user:" id) text "Twitter")
    ))

(defn eval-user [profile-fn [entity_id {:keys [followees hashtags groups]} :as u] pref-source]
  (let [topics (user->pref profile-fn u pref-source)
        user-groups (map keyword groups)
        positive? (into #{} user-groups)
        all-groups (->>  (eval/deterministic-shuffle (keys gid->group) (hash entity_id))
                      (filter (complement positive?))
                      (take 95)
                      (concat (take 5 (eval/deterministic-shuffle user-groups (hash entity_id)))))
        stories-topics (->> all-groups
                            (r/map (partial group->topic profile-fn))
                            (r/map eval/norm)
                            (into [])
                            (m/matrix)
                            )
        ]
    (-> (m/inner-product stories-topics topics)
         (eval/scores->relavant-ranks 5))
    )
  )
[:meetup-100 :meetup-200 :meetup-400
 :multi-100 :multi-200 :multi-400
 :meetup-doc2vec-100 :meetup-doc2vec-200 :meetup-doc2vec-400
 ]
{:own 1 :hashtags 1 :followees 1 }
{:own 1 :hashtags 1 :followees 2 }
{:own 1 :hashtags 1 :followees 5 }
(def results
  (->>
    (:users eval-info)
    (eval/deterministic-shuffle)
    (filter #(>= (count (:followees (second %))) 50))
    (take 5000)
    (pmap
      (fn [i u]
        (println i)
        (try
          (->> (for [model [:meetup-doc2vec-200
                            :meetup-200
                            :multi-200
                            ]]

                 (doall
                   (for [pref-source [
                                      {:own 1 :hashtags 0.2 :followees 5 }
                                      {:followees 1 }
                                      {:hashtags 1 }
                                      {:own 1 }
                                      ]]
                     {:model model
                      :user u
                      :results (eval-user model u pref-source)
                      :pref-source pref-source
                      }))
                 )
               (doall)

               )

          (catch Exception e (println e))))

      (range))
    (filter identity)
    (flatten)
    )
  )

(->> (flatten
       (for [{:keys [results model user pref-source]} results]
         (let [pr-fn (analysis/pr-fn results)]

           (for [i (range 0.1 1.05 0.05)]
             {:method model
              :recall i
              :precision (pr-fn i)
              :pref (clojure.string/join "+" (map str pref-source))
              :user (:id user)
              }
             ))
         ))
     (incanter.core/to-dataset)
     (incanter.core/aggregate [ :precision] [:recall :method :pref] :rollup-fun stats/mean :dataset)
     (#(incanter.core/save % "meetup-profile.csv"))
     )
