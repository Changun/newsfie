(ns new-model.test
  (:require [clojure.xml :as xml]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.matrix :as m]
            [cheshire.core :as json]
            [lifestreams-db.postgres :as postgres]
            [clojure.java.jdbc :as sql]
            [clojure.core.reducers :as r]
            [lifestreams-db.meetup :as meetup]
            [lifestreams-db.topic-model :as topics])
  (:import (cc.mallet.types InstanceList Instance TokenSequence Token Alphabet)
           (cc.mallet.pipe CharSequenceLowercase CharSequence2TokenSequence TokenSequenceRemoveStopwords TokenSequence2FeatureSequence SerialPipes Pipe)

           (cc.mallet.topics ParallelTopicModel TopicAssignment TopicInferencer BackgroundTopicModel MultiBackgroundTopicModel)
           (org.tartarus.snowball.ext EnglishStemmer)
           (java.io FileOutputStream ObjectOutputStream ObjectInputStream PrintWriter BufferedOutputStream ByteArrayInputStream ByteArrayOutputStream StringReader)
           (org.joda.time DateTime)
           (java.util List ArrayList HashMap)
           (cc.mallet.extract StringSpan)
           (clojure.lang PersistentVector)))



(defn run-model [instance-list n-topics n-iteration stop-word-percent]
  (let [
        model (MultiBackgroundTopicModel. n-topics 1.0 0.01 0.1 0.8)]
    (.setNumThreads model 16)
    (.setTopicDisplay model 50 20)
    (.setNumIterations model n-iteration)
    (.addInstances model instance-list)
    (.estimate model)
    ; output model
    (let [out (FileOutputStream. (str "model-" (DateTime.) "-" n-topics ".bin") false)]
      (with-open [dos (ObjectOutputStream. out)]
        (.writeObject dos model))
      )
    model
    )
  )



(defn run-multi-model [instance-list n-topics n-iteration]
  (let [
        model (MultiBackgroundTopicModel. n-topics 1.0 0.01 0.1 0.8)]
    (.setNumThreads model 32)
    (.setTopicDisplay model 50 20)
    (.setNumIterations model n-iteration)
    (.addInstances model instance-list)
    (.estimate model)
    ; output model
    (let [out (FileOutputStream. (str "model-" (DateTime.) "-" n-topics "-" "multi" ".bin") false)]
      (with-open [dos (ObjectOutputStream. out)]
        (.writeObject dos model))
      )
    model
    )
  )
(defn run-single-model [instance-list n-topics n-iteration source]
  (let [
        model (ParallelTopicModel. n-topics 1.0 0.01)]
    (.setNumThreads model 32)
    (.setTopicDisplay model 50 20)
    (.setNumIterations model n-iteration)
    (.addInstances model instance-list)
    (.estimate model)
    ; output model
    (let [out (FileOutputStream. (str "model-" (DateTime.) "-" n-topics "-" source ".bin") false)]
      (with-open [dos (ObjectOutputStream. out)]
        (.writeObject dos model))
      )
    model
    )
  )


(defn ^ParallelTopicModel extract-instances [file]
  (let [model ^ParallelTopicModel (topics/deserialize-model file)
        instances (->> (.data model)
                       (map #(.instance %))

                       )]
    (.stopGrowth (.getAlphabet model))
    (doto (InstanceList. (.getAlphabet model) nil)
      (.addAll instances))

    ))
(defn ^ParallelTopicModel extract-source-instances [file source]
  (let [model ^ParallelTopicModel (topics/deserialize-model file)
        instances (->> (.data model)
                       (map #(.instance %))
                       (filter #(#{source} (.getSource %)))
                       )]
    (.stopGrowth (.getAlphabet model))
    (doto (InstanceList. (.getAlphabet model) nil)
      (.addAll instances))

    ))

(defn instance->df-map [instances]
  (->> instances
       (map #(-> %
                 (.getData)
                 (.getFeatures)
                 (distinct)
                  ))
       frequencies
       )

  )
(def instances (extract-instances "model-2015-09-17T06:41:28.437Z-200.bin"))
(def news-instance (extract-source-instances "model-2015-09-17T06:41:28.437Z-200.bin" "News"))
(def meetup-instance (extract-source-instances "model-2015-09-17T06:41:28.437Z-200.bin" "Meetup"))

(def model-50 (run-multi-model instances 50 1000 ))
(def model-100 (run-multi-model instances 100 1000 ))

(def medium-model-50 (run-single-model news-instance 50 1000 "medium"))
(def medium-model-100 (run-single-model news-instance 100 1000 "medium"))


(def meetup-model-50 (run-single-model meetup-instance 50 1000 "meetup"))
(def meetup-model-100 (run-single-model meetup-instance 100 1000 "meetup"))
(def meetup-model-200 (run-single-model meetup-instance 200 1000 "meetup"))
(def meetup-model-400 (run-single-model meetup-instance 400 1000 "meetup"))
