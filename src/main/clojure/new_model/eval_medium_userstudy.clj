(ns new-model.eval-medium-userstudy
  (:require [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :refer [pooled-db]]
            [new-model.eval-medium :as eval-medium]
            [lifestreams-db.ctr :as ctr]
            [clojure.core.matrix :as m]
            [cheshire.core :as json]
            [cheshire.generate :refer [add-encoder encode-seq remove-encoder]]
            [new-model.eval :as eval])
  (:import (org.joda.time DateTime)
           (mikera.vectorz.impl AArrayVector)))

(def models
  (let [users (->> (sql/query @pooled-db
                              ["SELECT twitter_entity.id as entity_id, person.id as id, twitter_entity.name as entity_name
                              FROM person
                              INNER JOIN twitter_entity on twitter_entity.name=twitter_screen_name
                              INNER JOIN twitter_entity_text on entity_id=twitter_entity.id
                             "])

                   )
        stories (->> (sql/query @pooled-db
                                ["SELECT medium_story.id AS id, person_id AS uid
                                            FROM medium_story
                                            INNER JOIN medium_like ON story_id = id
                                            WHERE person_id=ANY(?)
                                                  AND text IS NOT NULL
                                                  AND upvote_count >= 5
                                                  AND published_at >= ?"
                                 (into [] (map :id users)) (DateTime. 2014 1 1 0 0)])

                     )

        user->stories (->> stories
                           (map (fn [{:keys [uid id]}] {uid [id]}))
                           (apply merge-with concat)
                           )

        user-doc (apply concat
                        (for [u users]
                          (for [s (user->stories (:id u))]
                            [u s]
                            )

                          ))
        docs (distinct (map :id stories))
        Theta (into {} (pmap #(vector % (eval-medium/story->topic :multi-200 %)) docs))
        Gamma (into {} (pmap #(vector % (eval/entity-name->profile :multi-200 (:entity_name %))) users))


        [_ pmf]
        (ctr/estimate
          (into {} (pmap #(vector % (m/zero-vector 200)) docs))
          (into {} (pmap #(vector % (m/zero-vector 200)) users))
          user-doc
          1
          0.01
          0.01
          0.01
          5
          )
        [_ ours]
        (ctr/estimate Theta
                      Gamma
                      user-doc
                      1
                      0.01
                      10
                      10
                      10
                      )


        [_ ctr]
        (ctr/estimate Theta
                      (into {} (pmap #(vector % (m/zero-vector 200)) users))
                      user-doc
                      1
                      0.01
                      10
                      0.01
                      5
                      )

        ]
    {:ours ours :pmf pmf :ctr ctr :theta Theta}
    )
  )


(def theta
  (let [users (->> (sql/query @pooled-db
                              ["SELECT twitter_entity.id as entity_id, person.id as id
                              FROM person
                              INNER JOIN twitter_entity on twitter_entity.name=twitter_screen_name
                              INNER JOIN twitter_entity_text on entity_id=twitter_entity.id
                             "])

                   )
        stories (->> (sql/query @pooled-db
                                ["SELECT medium_story.id AS id, person_id AS uid
                                            FROM medium_story
                                            INNER JOIN medium_like ON story_id = id
                                            WHERE person_id=ANY(?)
                                                  AND text IS NOT NULL
                                                  AND upvote_count >= 5
                                                  AND published_at >= ?"
                                 (into [] (map :id users)) (DateTime. 2014 1 1 0 0)])

                     )
        docs (distinct (map :id stories))
        Theta (into {} (pmap #(vector % (eval-medium/story->topic :multi-200 %)) docs))
        ]
    Theta
    )
  )
(add-encoder AArrayVector encode-seq)
(with-open [w (clojure.java.io/writer "medium_ousr_pmf_ctr_theta.json")]
  (json/generate-stream (assoc models :theta theta) w)
  )