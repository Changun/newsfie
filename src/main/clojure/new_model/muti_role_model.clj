(ns lifestreams-db.muti-role-model
  (:require [lifestreams-db.data :as data]
            [clojure.core.matrix :as m]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)])
  (:import (org.tartarus.snowball.ext EnglishStemmer))
  )
(def gamma 0.3)
(def alpha 0.1)
(def beta_0 0.01)
(def beta_1 0.01)
(def T 50)
(def BACKGROUND 0)
(def TOPICAL 1)
(def WORD-DIM 0)
(def TYPE-DIM 1)
(def TOPIC-DIM 2)

(def possible-type-topics (cons [BACKGROUND nil]
                                (map #(vector TOPICAL %) (range T))))

(defn sparse-sample [K numerators denominators]
   (let [norm-func (fn [sequence element-fn agg-fn]
                 (fn norm-func [from-i]
                   (cond->
                     (element-fn (nth sequence from-i))
                     (not= from-i (dec K))
                     (agg-fn (norm-func (inc from-i)))
                     )
                   ))
         numerator-norms
         (for [sequence numerators]
           (let [norm-sum-fn (norm-func sequence #(Math/pow % 2) +)]
             (fn [from-i]
               (Math/pow (norm-sum-fn from-i) 0.5)
               )
             )
           )

         denominator-norms
         (for [sequence denominators]
           (let [norm-sum-fn (norm-func sequence identity min)]
             (fn [from-i]
               (norm-sum-fn from-i)
               )
             )
           )
         u (rand)
         ]
     (loop [k 0 prev-sums [] prev-Z 0 prev-uZ 0]
       (let [prev-sum (or (last prev-sums) 0)
             sum (+ prev-sum (/ (apply * (map #(nth % k) numerators))
                           (apply * (map #(nth % k) denominators))
                           ))
             Z (+ sum (/ (apply * (for [norm numerator-norms]
                                    (norm (inc k))
                                    ))
                         (apply * (for [norm denominator-norms]
                                    (norm (inc k))
                                    ))
                         ))
             uZ (* u Z)
             ]

         (if (<= uZ sum)
           (if (or (= k 0) (> uZ prev-sum))
             k
             (let [u (/ (* (- prev-uZ prev-sum) Z)
                        (- prev-Z Z))]
               (or (reduce (fn [i]
                             (if (>= (nth prev-sums i) u)
                               (reduced i)
                               )
                             )

                           nil
                           (range k)
                           )
                   (recur (inc k) (conj prev-sums sum) Z uZ)
                   )
               )
             )
           (recur (inc k) (conj prev-sums sum) Z uZ)

           )
         )
       )

     )
  )
(defn init-global-params [W T]
  ;word->topic->count topic->count word->type->count type->count
  [(into [] (repeat W (into [] (repeat T 0))))
   (into [] (repeat T 0))
   (into [] (repeat W (into [] (repeat 2 0))))
   (into [] (repeat 2 0))
   ]
  )
(defn init-local-params [T]
  ;[local-topic->count local-type->count]
  [(into [] (repeat T 0))
   (into [] (repeat 2 0))
   ]
  )
(defn update!
    [data
     old-tuple
     new-tuple
     update-dim
     ]

  (let [old-element (old-tuple update-dim )
        new-element (new-tuple update-dim )
        ]
    (if (not= old-element new-element)
      (cond->
        data
        old-element
        (assoc old-element (dec (nth data old-element)))
        new-element
        (assoc new-element (inc (nth data new-element)))
        )
      data
      )
    )  )
(defn exclude-and-get
  [data
   tuple
   element
   element-dim
   ]
  (let [val (nth data element)
        val (cond-> val (= (tuple element-dim) element) dec)]
    val
    )
)

(defn sample-one-word
  [[word->topic->count topic->count word->type->count type->count]
   [local-topic->count local-type->count]
   [T W N]
   [word _ _ :as old-tuple]

   ]
  (let [local-topical-count (exclude-and-get local-type->count old-tuple TOPICAL TYPE-DIM)
        current-word->type->count (nth word->type->count word)
        current-word->topic->count (nth word->topic->count word)
        W-beta_0 (* W beta_0)
        probs (cons
                (/
                  (* (+ (exclude-and-get local-type->count old-tuple BACKGROUND TYPE-DIM) gamma)
                     (/ (+ (exclude-and-get current-word->type->count old-tuple BACKGROUND TYPE-DIM) beta_1)
                        (+ (exclude-and-get type->count old-tuple BACKGROUND TYPE-DIM)
                           (* W beta_1))
                        )
                     (+ local-topical-count
                        (* T alpha))
                     )
                  (+ local-topical-count gamma))

                (for [topic (range T)]
                  (do
                    (* (+ (exclude-and-get local-topic->count old-tuple topic TOPIC-DIM) alpha)
                       (/ (+ (exclude-and-get current-word->topic->count old-tuple topic TOPIC-DIM) beta_0)
                          (+ (exclude-and-get topic->count old-tuple topic TOPIC-DIM)
                             W-beta_0)
                          )
                       ))
                  ))
        mass (apply + probs)
        sample (* (rand) mass)
        ]
    (reduce
      (fn [[cum-sum [prob & probs]] [type topic]]
        (let [cum-sum (+ cum-sum prob)]
          (if (>= cum-sum sample)
            (reduced [word type topic])
            [cum-sum probs]
            ))
        )
      [0 probs]
      possible-type-topics)
    )
  )

(defn update-global-params! [old-tuple
                            new-tuple
                            [word->topic->count topic->count word->type->count type->count]]
  (if (not= old-tuple new-tuple)
    (let [word (first old-tuple)]
      [(->
         word->topic->count
         (assoc word (update! (nth word->topic->count word) old-tuple new-tuple TOPIC-DIM))
         )

       (update! topic->count old-tuple new-tuple TOPIC-DIM)
       (->
         word->type->count
         (assoc word (update! (nth word->type->count word) old-tuple new-tuple TYPE-DIM)))
       (update! type->count old-tuple new-tuple TYPE-DIM)
       ]
      )
    [word->topic->count topic->count word->type->count type->count]
    )

  )
(defn update-local-params!
  [old-tuple
   new-tuple
   [local-topic->count local-type->count]]
  [(update! local-topic->count old-tuple new-tuple TOPIC-DIM)
   (update! local-type->count old-tuple new-tuple TYPE-DIM)
   ]
  )

(defn sample-one-doc [[T W]
                      global-params
                      word-tuples]
  (let [N (count word-tuples)
        local-params
        (p :init (reduce (fn [local-params tuple]
                           (update-local-params!  [(first tuple) nil nil] tuple local-params)
                           )
                         (init-local-params T)
                         word-tuples
                         ))
        constants [T W N]
        [global-params _ new-word-tuples]
        (reduce
             (fn [[global-params local-params new-word-tuples] index]
               (let [old-tuple (nth word-tuples index)
                     new-tuple (p :one-word
                                  (sample-one-word
                                    global-params
                                    local-params
                                    constants
                                    (nth word-tuples index)
                                    ))
                     ]
                 (p :update
                    [(update-global-params! old-tuple new-tuple global-params)
                     (update-local-params! old-tuple new-tuple local-params)
                     (assoc new-word-tuples index new-tuple)])
                 )
               )
             [global-params
              local-params
              []]
             (range N)
             )
        ]
    [global-params new-word-tuples]

    )

  )
(defn one-iteration [[T W] params docs-word-tuples]
  (reduce
    (fn [[params docs-word-tuples] index]
      (let [old-word-tuples (nth docs-word-tuples index)
            [params new-word-tuples]
            (sample-one-doc
              [T W]
              params
              old-word-tuples
              )
            ]
        [params (assoc docs-word-tuples index new-word-tuples)]
        )
      )
    [params docs-word-tuples]
    (range (count docs-word-tuples))
    )
  )

(defn one-iteration [[T W] params docs-word-tuples]
  (let [global-params (atom params)
        docs-word-tuples (doall
                           (pmap (fn [old-word-tuples]
                                   (let [[_ new-word-tuples]
                                         (sample-one-doc
                                           [T W]
                                           @global-params
                                           old-word-tuples
                                           )
                                         ]
                                     (swap! global-params
                                            #(reduce (fn [params [old new]]
                                                       (update-global-params! old new params))
                                                     %
                                                     (map vector old-word-tuples new-word-tuples)
                                                     ))
                                     new-word-tuples

                                     )
                                   )
                                 docs-word-tuples
                                 ))]
    [@global-params docs-word-tuples]


    )


  )

             (comment
               (swap! @params-atom #(reduce (fn [params [old new]]
                                              (update-global-params! old new params))
                                            %
                                            (map vector old-word-tuples new-word-tuples)
                                            )))

(m/set-current-implementation :vectorz)
(let [stem (fn [word]
             (let [stemmer (EnglishStemmer.)]
               (.setCurrent stemmer word)
               (.stem stemmer)
               (.getCurrent stemmer))
             )
      docs (->> (data/get-stories @lifestreams-db.postgres/pooled-db)
                (map (partial data/get-story-with-id @lifestreams-db.postgres/pooled-db))
                (map :text)
                (filter #(> (count %) 1000))
                (map #(->> (re-seq #"\p{L}\p{L}\p{L}+" %)
                           (map clojure.string/lower-case)
                           (map stem)
                           (map keyword)
                           ))
                (take 2000)
                )
      doc-freq (->> docs
                   (map distinct)
                   (flatten)
                   (frequencies)
                   )
      stop-words (->> doc-freq
                      (filter (fn [[_ freq]]
                                (> freq (* 0.1 (count docs)))
                                ))
                      (map first)
                      (into #{})
                      )
      _ (println stop-words)
      docs (for [words docs]
             (filter (complement stop-words) words))
      all-words (flatten docs)
      words (into #{} all-words)
      W (count words)
      word->index (into {} (map #(vector %1 %2) words (range)))
      index->word (into {} (map #(vector %2 %1) words (range)))
      ; randomly initialize word tuples
      doc-word-tuples
      (mapv (fn [doc] (mapv #(apply vector (word->index %) (rand-nth possible-type-topics)) doc)) docs)
      _ (println (first doc-word-tuples))
      global-params
      (reduce (fn [global-params tuple]
                (update-global-params! [(first tuple) nil nil] tuple global-params)
                )
              (init-global-params W T)
              (mapcat identity doc-word-tuples)
              )
      num-iteration 20

      ]
  (profile :info :overall
           (reduce
             (fn [[global-params doc-word-tuples] iteration]
               (println iteration)
               (one-iteration [T W] global-params doc-word-tuples)
               )
             [global-params doc-word-tuples]
             (range num-iteration)
             )
           )
  nil

  )



(comment
  (let [[word->topic->count topic->word-count word->background-count background-word-count] params
        topic->word->count (reduce (fn [ret [word topic->count]]
                                     (reduce (fn [ret [topic count]]
                                               (assoc ret topic (conj (ret topic) [word count]) )
                                               )
                                             ret
                                             topic->count
                                             )
                                     )
                                   {}
                                   word->topic->count)
        word-count (+ background-word-count (reduce (fn [ret [word topic->count]]
                                                      (reduce (fn [ret [topic count]]
                                                                (+ ret count)
                                                                )
                                                              ret
                                                              topic->count
                                                              )
                                                      )
                                                    0
                                                    word->topic->count))
        ]
    (println word-count (count all-words))
    (println (take 100 (reverse (sort-by second word->background-count))))
    (map #(->>
           (second %)
           (sort-by second)
           (reverse)
           (take 20)
           ) topic->word->count)
    ))