
(ns new-model.eval-meetup-userstudy
  (:require [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :refer [pooled-db]]
            [new-model.eval-meetup :as eval-meetup]
            [lifestreams-db.ctr :as ctr]
            [clojure.core.matrix :as m]
            [cheshire.core :as json]
            [cheshire.generate :refer [add-encoder encode-seq remove-encoder]]
            [lifestreams-db.meetup-crawler :as crawler]
            [lifestreams-db.twitter :as tw]
            [new-model.eval :as eval]
            [lifestreams-db.meetup :as meetup]
            [lifestreams-db.data :as data])
  (:import (org.joda.time DateTime)
           (mikera.vectorz.impl AArrayVector)))

(def models
  (let [group->users (crawler/group->users)
        user-record->user (fn [user]
                            (let [{:keys [id other_services] :as u} user
                                  twitter (->> (:twitter other_services)
                                               :identifier)]
                              {:id id
                               :twitter (if (= \@ (first twitter))
                                          (-> twitter
                                              (clojure.string/replace " " "")
                                              (clojure.string/replace #"@+" "@")
                                              tw/normalize-screen-name
                                              ))
                               }))
        users (->>
                (mapcat second group->users)

                (map user-record->user )
                (distinct))

        ; 36619 people with twitter data
        twitter-users (filter (comp eval/entity-avail? :twitter) users)
        users (take 100000 (distinct (concat twitter-users (eval/deterministic-shuffle users))))
        users? (into #{} users)
        user-group (->> (for [[group users] group->users]
                            (->>
                              (map user-record->user users)
                              (filter users?)
                              (map #(vector %1 group))
                              )
                            )
                          (mapcat identity)
                          )

        user->content (reduce (fn [ret u-rec]
                                (let [u (user-record->user u-rec)]
                                  (cond->
                                    ret
                                    (and (users? u) (not (ret u)))
                                    (assoc u (meetup/user->content u-rec))
                                    ))
                                )
                              {}
                              (mapcat second group->users))

        ; A user join 933182/200000 groups on average

        docs (distinct (map second user-group))
        Theta (into {} (pmap #(vector % (eval-meetup/group->topic :multi-200 %)) docs))
        Gamma (into {} (pmap (fn [% i]
                               (if (= (mod i 10000) 0)
                                 (println i))
                               (vector % (if (eval/entity-avail? (:twitter %))
                                           (eval/entity-name->profile :multi-200 (:twitter %))
                                           (eval-meetup/user->topic :multi-200 (:id %) (user->content %))
                                           ))) users (range)))


        [_ pmf]
        (ctr/estimate
          (into {} (pmap #(vector % (m/zero-vector 200)) docs))
          (into {} (pmap #(vector % (m/zero-vector 200)) users))
          user-group
          1
          0.01
          0.01
          0.01
          5
          )
        [_ ours]
        (ctr/estimate Theta
                      Gamma
                      user-group
                      1
                      0.01
                      10
                      10
                      10
                      )


        [_ ctr]
        (ctr/estimate Theta
                      (into {} (pmap #(vector % (m/zero-vector 200)) users))
                      user-group
                      1
                      0.01
                      10
                      0.01
                      5
                      )

        ]
    {:ours ours :pmf pmf :ctr ctr :theta Theta :user-group user-group}
    )
  )



(add-encoder AArrayVector encode-seq)
(with-open [w (clojure.java.io/writer "meetup_ousr_pmf_ctr_theta.json")]
  (json/generate-stream models w)
  )
