(ns new-model.fast-lda-background-multi-corpus
  (:use [new-model.model]
        [new-model.utils]
        )

  (:require [new-model.model :refer [TopicModel]]
            [primitive-math :as prim]
            [hiphip.double]
            [clojure.data.priority-map :as prio-map]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)])
  )

(set! *warn-on-reflection* true)
(def TOPICAL 0)
(def BACKGROUND 1)
(defn bg-coeff [T alpha local-type->count type->count]
  (/
    (* (+ (* T alpha) (local-type->count TOPICAL))
       (local-type->count BACKGROUND)
       )
    (local-type->count TOPICAL)
    (type->count BACKGROUND)
    ))
(defrecord FastLDABackgroundMultiCorpus [T W ^double alpha ^double beta ^double gamma index->token]
  TopicModel
  (empty-word-params [model]
    [nil nil]
    )
  (init-global-params [model]
    [
     ; word->topic->count
     (into [] (repeat W {}))
     ; topic->count
     (into [] (repeat T (* W beta)))
     ]
    )
  (init-corpus-params [model]
    [; word->type->-count
     (into [] (repeat W (into [] (repeat 2 beta))))
     ; type->count
     (into [] (repeat 2 (* W beta)))
     ]
    )
  (init-local-params [model]
    [{}
     ; local-topic->count
     ; [TOPICAL+gamma BACKGROUND+gamma]
     [gamma gamma]
     ]
    )
  (init-local-cache [model [local-topic->count local-type->count] [_ type->count] [_ topic->count]]
    [
     ; s
     (* beta
        alpha
        (->> (range T)
             (map (fn [topic] (/ 1 (topic->count topic))))
             (apply + )
             )
        )
     ; r
     (* beta
        (->> local-topic->count
             (map (fn [[topic cnt]]
                    (/ cnt  (topic->count topic))))
             (apply +)
             )
        )
     ; q-coefficients
     (mapv
       (fn [t]
         ^double
         (/ (+ alpha (or (local-topic->count t) 0))
            (topic->count t)
            ))
       (range T))
     ; bg-coefficient
     (bg-coeff T alpha local-type->count type->count)
     ]
    )

  (update-global-params [model word [old-topic old-type] [new-topic new-type]
                         [word->topic->count  topic->count ]]

    [
     ; update word->topic->count
     (assoc word->topic->count
       word
       (update-topic-map-dis-zero (word->topic->count word) old-topic new-topic))
     ; update topic->count
     (update-topic-map topic->count old-topic new-topic)
     ]
    )
  (update-corpus-params [model word [old-topic old-type] [new-topic new-type]
                         [word->type->count type->count]]

    [
     ; update word->type->count
     (assoc word->type->count
       word
       (update-topic-map (word->type->count word) old-type new-type)
       )
     ; update type->count
     (update-topic-map type->count old-type new-type)
     ]
    )

  (update-local-params [model word [old-topic old-type] [new-topic new-type]  [topic->count type->count]]
    [(update-topic-map-dis-zero topic->count old-topic new-topic)
     (update-topic-map type->count old-type new-type)
     ]
    )

  (update-local-cache [model token [old old-type] [new new-type]  [s r q-coefficients bg-coefficient]
                       [local-topic->count local-type->count] [_ type->count] [_ topic->count ] ]
    (let [bg-coeff
          (if (= old-type new-type)
            bg-coefficient
            (let [new-local-type-count (update-topic-map local-type->count old-type new-type)]
              (bg-coeff T alpha new-local-type-count type->count))
            )]
      (if (= old new)
        [s r q-coefficients bg-coeff]
        (let [orig-old-topic-local-count (if old (or (local-topic->count old) 0))
              orig-new-topic-local-count (if new (or (local-topic->count new) 0))
              beta-mul-alpha (* beta alpha)
              orig-old-topic-count (if old (topic->count old))
              orig-new-topic-count (if new (topic->count new))

              ]
          [(cond->
             s
             old
             (-> (- (/ beta-mul-alpha
                       orig-old-topic-count)
                    )
                 (+ (/ beta-mul-alpha
                       (- orig-old-topic-count 1)))
                 )
             new
             (-> (- (/ beta-mul-alpha
                       orig-new-topic-count)
                    )
                 (+ (/ beta-mul-alpha
                       (+ orig-new-topic-count 1)))
                 )
             )
           (cond->
             r
             old
             (-> (- (/ (* orig-old-topic-local-count beta)
                       orig-old-topic-count)
                    )
                 (+ (/ (* (- orig-old-topic-local-count 1) beta)
                       (- orig-old-topic-count 1)))
                 )
             new
             (-> (- (/ (* orig-new-topic-local-count beta)
                       orig-new-topic-count)
                    )
                 (+ (/ (*
                         (+ orig-new-topic-local-count 1) beta)
                       (+ orig-new-topic-count 1)))
                 )
             )
           (cond->
             q-coefficients
             old
             (assoc old
                    ^double
                    (/ (- (+ alpha orig-old-topic-local-count)  1)
                       (- orig-old-topic-count 1)
                       )
                    )
             new
             (assoc new
                    ^double
                    (/ (+ (+ alpha orig-new-topic-local-count) 1)
                       (+ orig-new-topic-count 1)
                    )
                    )
             )
           bg-coeff
           ]
          ))

      )




    )

  (sample [model word [^double s ^double r q-coefficients ^double bg-coeff]
           [local-topic->count local-type->count]
           [word->type->count type->count]
           [word->topic->count topic->count]]
    (let [b (prim/*  bg-coeff ^double ((word->type->count word) BACKGROUND) )
          current-word->topic->count (word->topic->count word)
          ;^longs  q-topics (p :q-topics (long-array (keys current-word->topic->count)))
          ;^doubles q-cnt  (p :q-cnt (double-array (vals current-word->topic->count)))
          q-weights (double-array (count current-word->topic->count))
          index-array (int-array [0])
          ^double sum_q
          (reduce-kv (fn [sum topic cnt]
                       (let [weight (prim/* ^double (q-coefficients topic)  (double cnt))
                             sum (+ weight sum)
                             i (aget index-array 0)
                             ]
                         (aset q-weights i sum)
                         (aset index-array 0 (inc i))
                         sum
                         )
                       )
                     0 current-word->topic->count
                     )
          ;^double sum_q (p :q-sum (hiphip.double/asum q-weights))

          mass   (prim/+ b s r sum_q)
          sample (prim/* ^double (rand) mass)
          ]

      (cond

        (prim/< sample b)
        [nil BACKGROUND]

        (prim/< sample (prim/+ b sum_q))
        [(uniform-binary-search
           (prim/- sample b)
           (keys current-word->topic->count)
           q-weights
           )

         TOPICAL]

        (prim/< sample (prim/+ b sum_q r))
        [(uniform-sample-lazy
           (prim/div (prim/- sample b sum_q) beta)
           (keys local-topic->count)
           (map (fn [[topic cnt]]
                  (prim/div  (double cnt) ^double (topic->count topic))) local-topic->count)
           )
         TOPICAL]
        :default
        (let [sample (/ (prim/- sample b sum_q r) beta alpha)
              cases (range T)
              weights (map (fn [topic]
                             (prim/div 1.0  ^double (topic->count topic))
                             )
                           cases)]
          ;(comment (assert (close? (apply + weights) (/ s (* beta alpha)))))
          [(uniform-sample-lazy sample cases weights)
           TOPICAL]
          )
        )


      )
    )
  (diagnose [model corpus->corpus-params [word->topic->count topic->count]]

    (doseq [[word->type->count type->count] corpus->corpus-params]
      (let [num-tokens (- (apply + type->count) (* W beta 2))]
        (->>
          [(for [[word-index topic-counts] (map vector (range) word->topic->count)]
             (for [[topic-index count] topic-counts]
               {:topic topic-index :token word-index :count count}
               )

             )
           (for [[word-index counts] (map vector (range) word->type->count)]
             {:topic :BACKGROUND :token word-index :count (- (counts BACKGROUND) beta)}
             )
           ]
          (flatten)
          (group-by :topic)
          (sort-by (fn [[topic _]]
                     (if (= topic :BACKGROUND)
                       -1 topic)))
          (map (fn [[t ws]]
                 (let [keywords

                       (->> ws
                            (sort-by :count)
                            (reverse)
                            (take 5)
                            (map (comp index->token :token))
                            )
                       proportion (/ (apply + (map :count ws)) num-tokens)]
                   (printf "%s(%.4f): %s\n" t  proportion (clojure.string/join "," keywords))
                   )))
          (doall)
          )

        )
      )
    nil
    )

  )
