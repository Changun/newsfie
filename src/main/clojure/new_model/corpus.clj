(ns new-model.corpus
  (:require [lifestreams-db.meetup :as meetup]
            [clojure.core.reducers :as r]
            [cheshire.core :as json]
            [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :as postgres]))

(def twitter
  (future (->>
            (sql/query @postgres/pooled-db ["SELECT text FROM twitter_entity_text ORDER BY random() LIMIT 100000;" ])

            (r/map (fn [{:keys [text]}]
                     (-> text

                         (clojure.string/replace #"<[^>]*>" " ")
                         (clojure.string/replace #"&\w+;" " ")
                         (clojure.string/replace #"http[^ ]+" " ")
                         (clojure.string/replace #"@[^ ]+" " ")
                         )
                     ))
            (r/filter #(> (count %) 1000))
            (r/map #(assoc {
                            :label  "trainning"
                            :name   "a"
                            :source "Twitter"} :data %))
            (into [])
            ))
  )
(def medium-news
  (future
    (->> (sql/query @postgres/pooled-db "select id, json_extract_path(json, 'tags') as tags , text from medium_story")
         (map #(identity {:data   (str (:text %) " " (clojure.string/join " " (map :name (:tags %))))
                          :label  "trainning"
                          :name   (:id %)
                          :source "News"}))


         ))

  )
(def nyt
  (future
    (->>
      (json/parse-stream (clojure.java.io/reader "nyt.json") true)
      (map (fn [{:keys [text keywords]}]
             {:data   (str (clojure.string/join " " keywords) text)
              :label  "trainning"
              :name   "a"
              :source "News"}
             )

           )
      (take 10000)
      )))
(def email
  (future
    (->>
      (concat
        (->>
          "email-corpus/enron/"
          (clojure.java.io/file)
          (file-seq)
          (filter #(.isFile %))
          (shuffle)
          (take 10000))
        (->>
          "email-corpus/elenas_inbox-master/parsed/source/"
          (clojure.java.io/file)
          (file-seq)
          (filter #(.isFile %))
          (shuffle)
          (take 10000))
        (->>
          "email-corpus/sarahs_inbox-master/parsed/msnbc"
          (clojure.java.io/file)
          (file-seq)
          (filter #(.isFile %))
          (shuffle)
          (take 10000))

        )
      (r/map (fn [f]
               (with-open [rdr (clojure.java.io/reader f)]
                 (-> (->>
                       (line-seq rdr)

                       (reverse)
                       (take-while (complement #(or (re-matches #"\s*[\w-\s]+:.*" %)
                                                    (re-matches #"\s*[_-]+.*" %)
                                                    )))
                       (map clojure.string/lower-case)
                       (reverse)
                       (butlast)
                       (filter (complement #(re-find #";|(http)|(palin)|(governor)|(pra)|(gsp)|(alaska)|(msnbc.com)|@|(www)" %)))
                       (clojure.string/join "\n")


                       )
                     (clojure.string/replace #"<[^>]*>" " ")
                     (clojure.string/replace #"=\d\d+" " ")
                     (clojure.string/replace #"=\n" "")
                     (clojure.string/replace #"&\w+;" " ")
                     (clojure.string/replace #"[\w\d]+(\.[\w\d]+)*@[\w\d]+(\.[\w\d]+)*" " ")

                     (clojure.string/replace #"\[image\]" " ")
                     (clojure.string/replace #"enron" " ")
                     ))



               ))
      (into [])
      (map #(assoc {
                    :label  "trainning"
                    :name   "a"
                    :source "Mail"} :data %))


      )))
(defn data
  []
  (concat
    @twitter
    @medium-news
    @nyt
    @email

    (->>
      (meetup/groups)
      (map second)
      (filter :description)
      (map meetup/group->content)
      (map #(assoc {
                    :label  "trainning"
                    :name   "a"
                    :source "Meetup"} :data %))

      )

    )

  )

