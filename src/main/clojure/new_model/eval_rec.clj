(ns new-model.eval-rec
  (:require [clojure.core.matrix :as m]
            [clojure.core.reducers :as r]
            [lifestreams-db.medium :as medium]
            [cheshire.core :as json]
            [new-model.eval :as eval]
            [experiment.analysis :as analysis]
            [incanter.stats :as stats]
            [new-model.eval-meetup :as eval-meetup])
  (:use [clojure.walk]
        [incanter.core]
        [incanter.stats]
        [incanter.datasets]
        [simple-progress.bar])
  (:import (java.io PushbackReader)))

(m/set-current-implementation :vectorz)

(defn gen-fn [story->v theta testing-items lambda pref?]
  (let [b 0.01 a 1
        story->v (merge  theta story->v)
        story->index (into {} (map vector (map first story->v) (range)))
        V (m/matrix (map second story->v))
        XX (->> (r/map #(m/outer-product % %) V)
                (r/fold m/add)
                (m/mul b)
                (m/matrix))
        testing-V (m/matrix (map story->v testing-items))

        ]
    (fn [pref training testing]
      (let [testing? (into #{} testing)
            pref (if pref? (m/to-vector pref) (m/zero-vector 200))
            pref (cond-> pref
                         (seq training)
                         (#(medium/fold-in-iteration V XX story->index lambda %  training [])))

            story->rank (->>(m/inner-product testing-V pref)
                            (map #(vector %1 %2) testing-items)
                            (sort-by second)
                            (reverse)
                            (map first)
                            (map #(vector %2 %1) (range))
                            (filter (comp testing? first))
                            (into {})

                            )
            ]
        (map story->rank testing)
        )
      )
    )
  )

(defn gen-popular-fn [training-users user-testing testing]

  (let [
        story->freq (->> (r/mapcat user-testing training-users)
                         (into [])
                         (frequencies))
        story->rank (->>
                        testing
                        (map (fn [story] (vector story (or (story->freq story) 0))))
                        (sort-by second)
                        (reverse)
                        (map first)
                        (map #(vector %2 %1 ) (range))
                        (into {})

                        )
        ]
    (fn [_ _ testing]
      (map story->rank testing)
      )
    )
  )



(def analyis-fns
  {:recall-50 #(analysis/k-recall 50 %)
   :recall-20 #(analysis/k-recall 20 %)
   :recall-10 #(analysis/k-recall 10 %)
   :recall-100 #(analysis/k-recall 100 %)
   :nDCG-10 #(analysis/nDCG 10 %)
   :nDCG-50 #(analysis/nDCG 50 %)
   :rr #(analysis/rr %)

   })

(defn export-data [file res]
  (with-data
    (->> res
         (map (fn [%] (reduce-kv (fn [% fn-name f]
                                   (assoc % fn-name (f (:results %)))
                                   )
                                 %
                                 analyis-fns
                                 )))
         (to-dataset))
    (let [se  (->> (aggregate (map first analyis-fns) [:method :n] :rollup-fun #(/ (stats/sd %) (pow (count %) 0.5)))
                   ($order [:n :method] :desc))
          avg (->> (aggregate (map first analyis-fns) [:method :n] :rollup-fun stats/mean)
                   ($order [:n :method] :desc))
          dat ($join
                [[:n :method] [:n :method]]
                avg
                (col-names
                  se (concat [:n :method] (drop 2 (map #(keyword (str (name %) "-se")) (col-names se)))))     )]
      (save dat file)
      res
      )

    ))


(defonce eval-medium (with-open [r (PushbackReader. (clojure.java.io/reader "medium-rec-eval.edn"))]
                       (binding [*read-eval* false]
                         (read r))))

(def analysis-medium
  (let [{:keys [ours ctr pmf theta testing-users training-users user->training user->testing testing-docs]}
        eval-medium
        testing testing-docs
        fns {:ours (gen-fn ours theta testing 10 true)
             :pmf (gen-fn pmf (into {} (map #(vector (first %) (m/zero-vector 200)) theta)) testing 0.01 false )
             :ctr (gen-fn ctr theta testing 0.01 false)
             :most-popular (gen-popular-fn (map :id training-users)  user->testing testing)
             }
        testing-users testing-users
        b (mk-progress-bar (count testing-users))
        ]
    (->>
      (pmap (fn [{:keys [id entity_name]}]
              (let [
                    testing (user->testing id)
                    pref (eval/entity-name->profile :multi-200 entity_name)
                    training (user->training id)
                    ]
                (b)
                (concat
                  (doall (for [i (range 1 11) [alg-name alg] fns ]
                           (let [results (alg pref (take i training) testing)]
                             {:method    alg-name
                              :results   results
                              :n i
                              :recall-50 (analysis/k-recall 50 results)
                              :recall-100 (analysis/k-recall 100 results)})

                           ))
                  (doall (for [method [:most-popular :ours]]
                           (let [results ((fns method) pref [] testing)]
                             {:method  method
                              :results results
                              :n 0
                              :recall-50 (analysis/k-recall 50 results)
                              :recall-100 (analysis/k-recall 100 results)})
                           ))
                  )
                ))
            testing-users)
      (flatten)
      (doall)
      (export-data "medium.csv")
      )

    ))



(def eval-meetup (with-open [r (PushbackReader. (clojure.java.io/reader "meetup-rec-eval.edn"))]
                       (binding [*read-eval* false]
                         (read r))))


(def analysis-meetup
  (let [{:keys [ours ctr pmf theta testing-users training-users user->training user->testing traning-groups testing-groups]}
        eval-meetup
        testing testing-groups
        fns {:ours (gen-fn ours theta testing 10 true)
             :pmf (gen-fn pmf (into {} (map #(vector (first %) (m/zero-vector 200)) theta)) testing 0.01 false )
             :ctr (gen-fn ctr theta testing 0.01 false)
             :most-popular (gen-popular-fn training-users user->testing testing)
             }
        testing-users testing-users
        b (mk-progress-bar (count testing-users))
        ]
    (->>
      (pmap (fn [user]
              (let [
                    testing (user->testing user)
                    pref (eval/entity-name->profile :multi-200 (:twitter user))
                    training (eval/deterministic-shuffle (user->training user))
                    ]
                (b)
                (concat
                  (doall (for [i [1 2 3 4 5] [alg-name alg] fns ]
                           (let [results (alg pref (take i training) testing)]
                             {:method    alg-name
                              :results   results
                              :n i
                              :recall-50 (analysis/k-recall 50 results)
                              :recall-100 (analysis/k-recall 100 results)})

                           ))
                  (doall (for [method [:most-popular :ours]]
                           (let [results ((fns method) pref [] testing)]
                             {:method  method
                              :results results
                              :n 0
                              :recall-50 (analysis/k-recall 50 results)
                              :recall-100 (analysis/k-recall 100 results)})
                           ))
                  )
                ))
            testing-users)
      (flatten)
      (doall)
      (export-data "meetup.csv")
      )

    )
  )

