(ns new-model.eval-power-law
  (:require [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :as postgres]))


(defn to-csv [file c]
  (spit file (clojure.string/join "\n" c))
  )

(def medium-count (sql/query @postgres/pooled-db ["SELECT upvote_count  as count FROM medium_story WHERE upvote_count >= 10 "]))
(def meetup-count (map (comp count second) (lifestreams-db.meetup-crawler/group->users)))

