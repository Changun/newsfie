(ns lifestreams-db.user-state
  "ring wrapper that stores the user's state in a repl accessible atom"
  (:import (java.util UUID))
  (:use [ring.middleware.params])
  )

(defonce states (atom {}))
(defn debug-uuid [request]
  (let [request (params-request request)]
    (if-let [uuid (get (:params request) "uuid")]
      (UUID/fromString uuid)))
  )
(defn state-request
  [request]
  (let [session (:session request)
        debug-uuid (debug-uuid request)
        id (or debug-uuid (:_state-id session))
        ]
    (if debug-uuid (println "Debug " debug-uuid))
    (cond-> request
            id
            (assoc :state (get @states id)))
    ))

(defn state-response
  "If response has a :state key, update the state atom and add state-id to the session
  (if it is not in the session already)."
  {:arglists '([response request])
   :added    "1.2"}
  [response {:keys [session] :as request}]
  (let [session (if (contains? response :session)
                  (response :session)
                  session)
        ]
    (if (and (response :state) (not (debug-uuid request)))
      (let [state-id (or (:_state-id session) (UUID/randomUUID))]
        (swap! states assoc state-id (response :state))
        (assoc response :session (assoc session :_state-id state-id))
        )
      response
      )
    ))

(defn wrap-user-state
  "If a :state key is set in the response. It will be available in the subsequent request from the same user,
  and kepted in a REPL access atom: lifestreams-db.user-state/states"
  [handler]
  (fn [request]
    (if-let [resp (handler (state-request request))]
      (state-response resp (state-request request)))))
