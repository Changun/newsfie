(ns lifestreams-db.twitter-clawer
  (:require [monger.collection :as mc]
            [monger.core :as mg]
            [lifestreams-db.twitter :as tw]

            [lifestreams-db.twitter :as twitter]


            [clojure.core.matrix :as m]
            [clojure.core.async :as async]
            [lifestreams-db.data :as data]
            [lifestreams-db.postgres :refer [pooled-db]]
            [clojure.java.jdbc :as sql])
  (:use [slingshot.slingshot]
        [simple-progress.bar])
  (:import (java.util AbstractCollection)
           (org.joda.time DateTime)
           (com.mongodb MongoInternalException)))


(def conn (mg/connect))
(def db (mg/get-db conn "twitter"))
(def medium-db (mg/get-db conn "medium"))



(defn get-users []
  (let [users (->> (mc/find-maps medium-db
                                 "user-recommendations"
                                 {"$where" "this.recommendations && this.recommendations.length > 20"}
                                 ["_id" "user.username"]
                                 )

                   (map #(or (get-in % [:user :username]) (:_id %)))
                   (filter seq)
                   )
        prog (mk-progress-bar (count users))]
    (loop [[user & us :as users] users]
      (let [ret (try+
                  (let [{:keys [friends tweets] :as ret} (mc/find-map-by-id db "tweets" (str "@" user))]
                    (-> ret
                        (assoc :_id (str "@" user) :screen-name (str "@" user))
                        (cond->
                          (not friends)
                          (assoc :friends (map #(select-keys % [:screen_name]) (tw/friend-sequence nil user))
                                 :friendsAvailable true)
                          (not tweets)
                          (assoc :tweets (map #(dissoc % :user :extended_entities) (tw/get-user-timeline nil user)))
                          (or (not friends) (not tweets))
                          (->> (mc/save db "tweets"))
                          )
                        )

                    )
                  (catch [:status 429] _ (println "limit reach")
                                         (Thread/sleep 1000)
                                         nil)
                  (catch [:status 401] _ :skip)
                  (catch [:status 404] _ :skip)
                  (catch MongoInternalException _ :skip)

                  )]
        (if ret
          (do
            (prog)
            (recur us))
          (recur users)
          )
        )
      )))

(defn- extract-hashtag [tweet]
  (map :text (get-in tweet [:entities :hashtags]))
  )

(defn reduce-size []
  (doseq [m (mc/find-maps db "tweets" {"_id" {"$regex" "^@.+"}})]
    (mc/save db "tweets" (assoc m :tweets (map #(dissoc % :user) (:tweets m))))
    )
  )
(defn get-hashtags []
  (let [tags (->> (mc/find-maps db "tweets" {"friendsAvailable" true})
                  (mapcat :tweets)
                  (mapcat tw/extract-hashtag)
                  (distinct)
                  (map (partial str "#"))
                  )
        prog (mk-progress-bar (count tags))
        ]
    (loop [[tag & ts :as tags] tags]
      (let [ret (try+ AbstractCollection
                      (or (mc/find-by-id db "tweets" tag ["_id"])
                          (let [tweets (take 200 (twitter/get-hashtag-tweets nil tag))]
                            ;(println tag)
                            (data/save-entity-tweets @pooled-db tag tweets)
                            :success
                            )
                          )
                      (catch [:status 429] _ (println "limit reach")
                                             (Thread/sleep 180000)
                                             nil)
                      (catch #(#{410 404 403 500} (:status %)) _ :skip)
                      )]
        (if ret
          (do
            (prog)
            (recur ts))
          (recur tags)
          )
        )
      )
    )
  )

(defn get-friend-tweets []
  (let [
        sqluser (->> (sql/query @pooled-db "select name from twitter_entity where name LIKE '@%'")
                     (map :name)
                     (map #(subs % 1))
                     (into #{})
                     )

        all-users (->> (mc/find-maps db "tweets" {"_id" {"$regex" "^@"}} ["_id"])
                       (map :_id)
                       (map #(subs % 1))
                       (into #{})
                       )

        users (->> (sql/query @pool)

                   )
        users (clojure.set/difference users all-users sqluser)

        prog (mk-progress-bar (count users))
        ]
    (println "Users to fetch:" (count users))
    (loop [[user & us :as users] (seq users)]
      (let [ret (try+
                  (or (mc/find-by-id db "tweets" user ["_id"])
                      (let [tweets (take 200 (twitter/get-user-timeline nil user))]

                        ; TODO MAKE SURE WE DON"T REPLACE THE EXISITNG USER!
                        (data/save-entity-tweets @pooled-db (str "@" user) tweets)
                        :success
                        )
                      )
                  (catch #(#{410 404 403 500 401} (:status %)) _ :skip)
                  )]
        (if ret
          (do
            (prog)
            (recur us))
          (recur users)
          )
        )
      )
    )
  )


(async/thread (get-users))
(async/thread (get-hashtags))
(async/thread (get-friend-tweets))