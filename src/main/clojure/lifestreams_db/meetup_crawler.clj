(ns lifestreams-db.meetup-crawler
  (:require [lifestreams-db.data :as data]
            [lifestreams-db.twitter :as tw]
            [lifestreams-db.postgres :as postgres]
            [cheshire.core :as json]
            [lifestreams-db.ctr :as ctr]
            [taoensso.timbre
             :refer (error info)])
  (:use [lifestreams-db.meetup])
  (:import (java.io PrintWriter)))


(defn group->users []
  (->>
    "/data/group/group2member"
    (clojure.java.io/file)
    (file-seq)
    (filter #(re-matches #"group2member\d+\.json" (.getName %)))
    (map clojure.java.io/reader)
    (pmap #(json/parse-stream % true))
    (apply concat)
    )
  )



(defn ctr []
  (let [user-ids (->>
                   (group->users)
                   (mapcat (fn [[group users]] (map :id users)))
                   (frequencies)
                   (filter #(> (second %) 1 ))
                   (map first)
                   (random-sample 0.3)
                   (into #{})

                   )

        user-id->content
        (->> (apply concat (map second (group->users)))
             (filter #(user-ids (:id %)))
             (map (fn [user] {(:id user) (str (user->content user) " ")}))
             (apply merge-with str)
             )
        user-group (->> (group->users)
                        (mapcat (fn [[group users]]
                                  (map (fn [user] [(:id user) group]) users)
                                  ))
                        (filter #(user-ids (first %)))
                        )
        group-ids (into #{} (map second user-group))
        groups (->> (json/parse-stream (clojure.java.io/reader "/data/group/group2group/group_group_final.json") true)
                        (filter #(group-ids (first %))))
        group->theta (into {} (pmap (fn [[id group]]  [id (content->topic (group->content group)) ]) groups))
        user->gamma  (into {} (pmap (fn [[id content]]
                                      [id
                                       (if (> (count content) 100)
                                         (content->topic content)
                                         )])
                                    user-id->content))
        [_ group->final-theta] (ctr/estimate group->theta
                                         user->gamma
                                         user-group
                                         1
                                         0.01
                                         10
                                         10
                                         10
                                         nil
                                         )
        ]
    (into {}
          (for [[gid group] groups]
            [gid (assoc group :topics (seq (group->final-theta gid)))]
            ))
    )
  )





;; Twitter crawler

(def tnames (->> (apply concat (map second (group->users)))
                 (mapcat :other_services)
                 (filter (comp #{:twitter} first))
                 (map second)
                 (map :identifier)
                 (distinct)
                 (filter #(= \@ (first %)))
                 (map #(clojure.string/replace % " " ""))
                 (map #(clojure.string/replace % #"@+" "@"))
                 (map tw/normalize-screen-name)
                 ))


(comment
  (doseq [tname (sort tnames)]
    (try
      (if-not (seq (data/get-twitter-friends-with-entity-name @postgres/pooled-db tname))
        (let [friends
              (->> (tw/friend-sequence nil (subs tname 1))
                   (map :screen_name)
                   (map tw/normalize-screen-name)
                   )
              ]
          (println "Fetched Friends for" tname (count friends))
          (data/save-twitter-follows @postgres/pooled-db tname friends)
          ))
      (catch Exception e (error e)))
    )

  (doseq [tname (sort tnames)]
    (try
      (if-not (seq (data/get-tweets-with-entity-name @postgres/pooled-db tname))
        (let [tweets (tw/get-user-timeline nil (subs tname 1))]
          (println "Fetched Tweets for" tname (count tweets))
          (data/save-entity-tweets @postgres/pooled-db tname tweets)
          ))
      (catch Exception e (error e)))
    )

  (doseq [hashtag (map first (reverse (sort-by second (frequencies (data/get-hashtags-with-multiple-entity-name @postgres/pooled-db tnames false)) )))]
    (if-not (seq (data/get-tweets-with-entity-name @postgres/pooled-db hashtag))
      (let [tweets (take 200 (tw/get-hashtag-tweets nil hashtag))]

        (data/save-entity-tweets @postgres/pooled-db hashtag tweets)
        (println "Fetched Tweets for" hashtag (count tweets))
        )
      )

    )
)
