(ns lifestreams-db.twitter
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]
            [base64-clj.core :as base64]
            [oauth.client :as oauth]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.memoize :as memoize]
            [lifestreams-db.data :as data]

            [lifestreams-db.postgres :refer [pooled-db]])
  (:use [slingshot.slingshot]
        [lifestreams-db.config])
  (:import (org.httpkit.client TimeoutException)
           (java.io IOException)))

;; Create a Consumer, in this case one to access Twitter.
;; Register an application at Twitter (https://dev.twitter.com/apps/new)
;; to obtain a Consumer token and token secret.

(def ^:private consumer-key (get-in @config [:twitter :consumer-key]))
(def ^:private consumer-secret (get-in @config [:twitter :consumer-secret]))
(def ^:private consumer (oauth/make-consumer consumer-key
                                             consumer-secret
                                             "https://api.twitter.com/oauth/request_token"
                                             "https://api.twitter.com/oauth/access_token"
                                             "https://api.twitter.com/oauth/authorize"
                                             :hmac-sha1))




(defn create-application-token-memoize [consumer-key consumer-secret]
  (memoize/ttl (fn []
                 (let [credential (base64/encode (str consumer-key ":" consumer-secret))
                       {:keys [body error status] :as res}
                       @(http/post "https://api.twitter.com/oauth2/token"
                                   {:headers     {"Authorization" (str "Basic " credential)
                                                  "Content-Type"  "application/x-www-form-urlencoded"}
                                    :form-params {"grant_type" "client_credentials"}
                                    })
                       ]

                   (cond
                     (or (isa? (class error) TimeoutException)
                         (isa? (class error) IOException)
                         (#{503} status))
                     (do (Thread/sleep 10)
                         (recur))
                     (or error (not= 200 status))
                     (throw+ res)
                     :default
                     (:access_token (parse-string body true))
                     )
                   ))
               :ttl/threshold (* 10 60 1000))

  )

; function that returns a twitter "app-only" token with a randomly chosen client
(def get-application-token
  (let [token-memoizes (apply vector
                         (create-application-token-memoize consumer-key consumer-secret)
                         (map (fn [[key secret]]
                                (create-application-token-memoize key secret)
                                ) (get-in @config [:twitter :clients]))
                         )]
    (fn []
      ((rand-nth token-memoizes))
      )
    )
  )


(defn twitter-application-endpoint [method path options]
  {:prev [({:GET :POST} method)]}
  (let [token (get-application-token)
        request-fn (condp = method :GET http/get :POST http/post)
        {:keys [error body status] :as res}
        @(request-fn
           (str "https://api.twitter.com/1.1" path)
           (-> options
               (assoc-in [:headers "Authorization"] (str "Bearer " token))
               (assoc :timeout 2000)
               )
           )

        ]
    (cond
      (or (isa? (class error) TimeoutException)
          (isa? (class error) IOException)
          (#{503 429} status)
          )
      (do (Thread/sleep 1000)
          (recur method path options))
      (or error (not= status 200))
      (throw+ res)
      :default
      res
      )
    )
  )

(defn twitter-endpoint [token method path options & {:keys [fallback] :or {fallback true}}]
  {:prev [({:GET :POST} method)]}
  (if token
    (let [credentials (oauth/credentials consumer
                                         (:oauth_token token)
                                         (:oauth_token_secret token)
                                         method
                                         (str "https://api.twitter.com/1.1" path)
                                         (:query-params options)
                                         )
          request-fn (condp = method :GET http/get :POST http/post)
          {:keys [error body status] :as res}
          @(request-fn
             (str "https://api.twitter.com/1.1" path)
             (assoc options
               :timeout 2000
               :query-params
               (merge
                 (:query-params options)
                 credentials)))

          ]
      (cond
        (or (isa? (class error) TimeoutException)
            (isa? (class error) IOException))
        (do (Thread/sleep 1000)
            (recur token method path options {:fallback fallback}))
        (and (= status 429) fallback)
        (binding [*out* *err*]
          (println "Quota depleted. Use application only endpoint")
          (try+
            (twitter-application-endpoint method path options)
            (catch [:status 429] e
              (println e "All quota depleted. return nil to fail gracefully")
              nil)

            )
          )
        (or error (not= status 200))
        (throw (or error (Exception. (str status body))))
        :default
        res
        )
      )
    (twitter-application-endpoint method path options)
    )
  )
(defn normalize-fn [first-char]
  (fn [entiry-name]
    (cond->>
      entiry-name
      (not= (first entiry-name) first-char)
      (str first-char)
      true
      clojure.string/lower-case))
  )
(def normalize-screen-name (normalize-fn \@))
(def normalize-hashtag (normalize-fn \#))


(defn friend-sequence [token & [screen-name cursor]]
  (let [{:keys [body]} (twitter-endpoint token :GET "/friends/list.json"
                                         {:query-params (cond->
                                                          {:count  200
                                                           :cursor (or cursor -1)}
                                                          screen-name
                                                          (assoc :screen_name screen-name)
                                                          )}
                                         :fallback true)
        {:keys [users next_cursor]} (parse-string body true)
        ]
    (if (seq users)
      (concat users (lazy-seq (friend-sequence token screen-name next_cursor)))
      )
    )
  )



(defn extract-hashtag [tweet]
  (or (:hashtags tweet)
      (map :text (get-in tweet [:entities :hashtags])))
  )
(defn get-user-timeline
  ([token screen-name & [since-id max-id]]
   (let [
         {:keys [body error status]}
         (twitter-endpoint token :GET "/statuses/user_timeline.json"
                           {:query-params (cond-> {:screen_name screen-name
                                                   :count       "200"}
                                                  max-id
                                                  (assoc :max_id max-id)
                                                  since-id
                                                  (assoc :since_id since-id)
                                                  )})]

     (let [timeline (parse-string body true)]
       (when-let [timeline (seq timeline)]
         (concat timeline (lazy-seq (get-user-timeline token screen-name
                                                       since-id
                                                       (- (:id (last timeline)) 1))))
         )
       )
     )
    )
  )



(defn tweets->content [tweets]
  (clojure.string/join "\n" (map :text tweets))
  )

(defn get-hashtag-tweets
  ([token hashtag & [type max-id]]
   (let [type (or type "recent")
         {:keys [body]}
         (twitter-endpoint token :GET "/search/tweets.json"
                           {:query-params (cond-> {:q           (cond->> hashtag
                                                                         (not= (first hashtag) \#)
                                                                         (str "#"))
                                                   :result_type type
                                                   :count       "100"}
                                                  max-id
                                                  (assoc :max_id max-id))})]
     (let [tweets (:statuses (parse-string body true))]
       (if-let [tweets (seq tweets)]
         (concat tweets (lazy-seq (get-hashtag-tweets token hashtag type (- (:id (last tweets)) 1))))
         )
       )
     )
    )
  )

(def db-agent (agent @pooled-db))

(defn get-user-timeline-and-save [token entity-name & [count since-id]]
  (let [tweets (cond->> (get-user-timeline token entity-name since-id)
                        count
                        (take count))]
    (send db-agent (fn [db]
                     (data/save-entity-tweets db entity-name tweets)
                     db))

    tweets
    )
  )
(defn get-hashtag-tweets-and-save [token hashtag & [count]]
  (let [tweets (cond->> (get-hashtag-tweets token hashtag)
                        count
                        (take count))]
    (send db-agent (fn [db]
                     (data/save-entity-tweets db hashtag tweets)
                     db))
    tweets
    ))
(defn get-friends-and-save [token screen-name & [count]]
  (let [friends (->> (friend-sequence token screen-name)
                     (map :screen_name)
                     (map normalize-screen-name)
                     )
        friends (cond->> friends count (take count))
        ]
    (send db-agent (fn [db]
                     (data/save-twitter-follows db screen-name friends)
                     db))
    friends

    ))
(defn my-pmap
  "Like map, except f is applied in parallel. Semi-lazy in that the
  parallel computation stays ahead of the consumption, but doesn't
  realize the entire result unless required. Only useful for
  computationally intensive functions where the time of f dominates
  the coordination overhead."
  ([f coll]
   (let [n 200
         rets (map #(future (f %)) coll)
         step (fn step [[x & xs :as vs] fs]
                (lazy-seq
                  (if-let [s (seq fs)]
                    (cons (deref x) (step xs (rest s)))
                    (map deref vs))))]
     (step rets (drop n rets)))))
(defn get-instances [token & [screen-name]]
  (let [screen-name (normalize-screen-name (or screen-name (:screen_name token)))
        existing-tweets (sort-by :id (data/get-tweets-with-entity-name @pooled-db screen-name))
        tweets (concat (p :get-latest-tweets
                          (get-user-timeline-and-save token screen-name nil (:id (last existing-tweets))))
                       existing-tweets)
        friends (future (->>
                          (concat
                            (data/get-twitter-friends-with-entity-name @pooled-db screen-name)
                            (get-friends-and-save token screen-name 200))
                          (take 250)
                          (map normalize-screen-name)
                          (distinct)
                          (my-pmap (fn [screen_name]
                                 (try
                                   {:content (tweets->content
                                               (p :fetch (get-user-timeline-and-save token screen_name 200)))
                                    :sources [{:type :Twitter
                                               :name screen_name}]
                                    }                                     (catch Exception _ nil))))
                          (filter identity)
                          (p :get-friend-tweets)
                          ))
        hashtags (future (->>
                           tweets
                           (map extract-hashtag)
                           (flatten)
                           (map normalize-hashtag)
                           (distinct)
                           (take 250)
                           (my-pmap (fn [hashtag]
                                  {:content (tweets->content
                                              (p :fetch (get-hashtag-tweets-and-save token hashtag 100))
                                              )
                                   :sources [{:type :Twitter
                                              :name hashtag}]}))
                           (p :get-hashtag-tweets)
                           ))
        ]
    (filter #(> (.length (:content %)) 500)
            (concat
              [{:content (tweets->content tweets)
                :sources [{:type :Twitter
                           :name :mine}]}
               ]
              @hashtags
              @friends

              )))
  )


;;;;;;;;;;;;;;;;; Public Functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def instances get-instances)


(defn get-profile [token & [screen-name]]
  (let [friends (doall (friend-sequence token screen-name))
        my-tweets (doall (get-user-timeline token screen-name))
        my-hashtags (apply concat (map extract-hashtag my-tweets))
        ]
    {:screen-name (str "@" screen-name) :friends friends :tweets my-tweets}
    )
  )

(defn get-request-token
  "Fetch a request token that a OAuth User may authorize"
  [callback-uri]
  (try
    (oauth/request-token consumer callback-uri)
    (catch IOException _ (Thread/sleep 1000)
                         (get-request-token callback-uri)))
  )

(defn get-auth-uri [request-token]
  (oauth/user-approval-uri
    consumer
    (:oauth_token request-token))
  )
(defn get-access-token [request-token verifier]
  (oauth/access-token consumer request-token verifier))
