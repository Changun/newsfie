(ns lifestreams-db.enron
  (:require [clojure.core.reducers :as r]))

(->>
  "maildir"
  (clojure.java.io/file)
  (file-seq)
  (filter #(.isFile %))
  (take 100)
  (r/map (fn [f]
           (->> (line-seq (clojure.java.io/reader f))
                (filter (complement #(or (re-matches #"\s*[\w-]+:.*" %) (re-matches #"\s*---+.*" %))) )
                (clojure.string/join "\n")
                )



           ))
  (into [])


  )