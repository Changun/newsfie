(ns lifestreams-db.topic-model
  (:require [clojure.xml :as xml]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.matrix :as m]
            )
  (:import (cc.mallet.types InstanceList Instance TokenSequence Token Alphabet)
           (cc.mallet.pipe CharSequenceLowercase CharSequence2TokenSequence TokenSequenceRemoveStopwords TokenSequence2FeatureSequence SerialPipes Pipe)
           (cc.mallet.topics TopicInferencer ParallelTopicModel MultiBackgroundTopicModel)
           (cc.mallet.topics ParallelTopicModel TopicAssignment TopicInferencer)
           (org.tartarus.snowball.ext EnglishStemmer)
           (java.io FileOutputStream ObjectOutputStream ObjectInputStream PrintWriter BufferedOutputStream ByteArrayInputStream ByteArrayOutputStream StringReader)
           (org.joda.time DateTime)
           (java.util List ArrayList)
           (cc.mallet.extract StringSpan)
           (clojure.lang PersistentVector)))



(defn stop-word-filter [additionals]
  (.addStopWords (TokenSequenceRemoveStopwords.)
                 (into-array String (into #{} (concat
                                                additionals
                                                ["mailto" "mail" "gmail" "email" "wrote" "write"
                                                 "html" "http" "https" "www" "link" "links" "url" "attached" "attachment" "attach"
                                                 "jan" "feb" "mar" "apr" "april" "may" "june" "jun" "july" "aug" "sept" "oct" "nov" "dec"
                                                 "mon" "monday" "tue" "tues" "tuesday" "wed" "wednesday" "thurs" "thu" "thur" "thursday" "fri" "friday" "sat" "saturday" "sun" "sunday" "week" "meet"

                                                 "time" "address" "googlegroups" "subject" "today" "reply"
                                                 "yeah" "hey" "guys" "cool" "people" "thing" "things" "don't" "didn't" "did" "work" "works" "make" "makes" "made" "lot" "life"
                                                 "youtube" "day" "year" "month" "wasn't" "was" "wasnt" "days" "want" "peopl" "wikipedia" "org" "com" "watch" "youtub" "user" "wiki"
                                                 "person" "point" "problem" "system" "long" "end" "turn" "moment" "don" "good" "talk" "start" "question" "doesn"
                                                 "didn" "back" "feel" "wasn" "felt" "thought" "friend" "knew" "know"]
                                                (->>
                                                  (or (clojure.java.io/resource "stopwords.txt")
                                                      (clojure.java.io/file "resouces/stopwords.txt"))
                                                  (slurp)
                                                  (clojure.string/split-lines)
                                                  (map clojure.string/trim)
                                                  ))))))

(defn create-pipe [f]
  (proxy [Pipe] []
    (pipe [^Instance i]
      (.setData i (f (.getData i)))
      i
      ))
  )

(def token-stemming-pipe
  (create-pipe (fn [data]
                 (let [stemmer (EnglishStemmer.)]
                   (doseq [^Token t data]
                     ;(print t)
                     (.setCurrent stemmer (.getText t))
                     (.stem stemmer)
                     (.setText t (.getCurrent stemmer))
                     ;(print (str ":" t "\n"))
                     ))
                 data)))

(defn after-tokenize-pipe
  ([stop-words & [alphabet]]
   (SerialPipes. [
                  ;(CharSequenceLowercase.)
                  ;(CharSequence2TokenSequence. #"\p{L}\p{L}\p{L}+")
                  ;tokenize-noun-filter-pipe
                  ;token-lowercase-pipe
                  ;(create-token-filter-pipe #(re-matches #"\p{L}[\p{L}\.-][\p{L}.-]+" %))

                  ;(apply stop-word-filter stop-words)
                  ;token-stemming-pipe
                  (stop-word-filter stop-words)
                  (TokenSequence2FeatureSequence. (or alphabet (Alphabet.)))
                  ]))

  )

(defn tokenize-pipe []
  (SerialPipes. [
                 (CharSequenceLowercase.)
                 (CharSequence2TokenSequence. #"\p{L}\p{L}\p{L}+")
                 token-stemming-pipe
                 ]))

(defn full-pipe [stop-words & [alphabet]]
  (SerialPipes. [(tokenize-pipe)
                 (after-tokenize-pipe stop-words alphabet)])
  )
(defn extract-stopword [insts freq-threshold]
  (let [word-freq
        (->> insts
             (pmap (fn [^Instance instance]
                     (->> instance
                          (.getData)
                          (map #(.getText ^StringSpan %))
                          (distinct)
                          )))
             (mapcat identity)
             (frequencies)
             )
        thres (* (count insts) freq-threshold)
        ]
    (println thres)
    (->> word-freq
         (filter (fn [[_ freq]]
                   (> freq thres)
                   ))
         (map first))
    )
  )
(defn run-model [docs n-topics n-iteration stop-word-percent]
  (let [tokens-list (InstanceList. (tokenize-pipe))
        _ (doseq [{:keys [data label name source]} docs]
            (.addThruPipe tokens-list (Instance. data label name source)))
        stop-words (extract-stopword tokens-list stop-word-percent)
        _ (println "Stop words" stop-words)
        pipe (after-tokenize-pipe stop-words)
        _ (doseq [% tokens-list]
            (.unLock ^Instance %)
            )
        instances-list (doto
                         (InstanceList. pipe)
                         (.addThruPipe (.iterator tokens-list))
                         )
        model (ParallelTopicModel. n-topics 1.0 0.01)]
    (.setNumThreads model 8)
    (.setNumIterations model n-iteration)
    (.addInstances model instances-list)
    (.estimate model)
    ; output model
    (let [out (FileOutputStream. (str "model-" (DateTime.) ".bin") false)]
      (with-open [dos (ObjectOutputStream. out)]
        (.writeObject dos [model (into [] stop-words)]))
      )
    model
    )
  )

(defn deserialize-model
  "Accepts a file name or url"
  [file]
  (let [model (with-open [dis (ObjectInputStream.
                                (clojure.java.io/input-stream file))]
                (.readObject dis))]
    (cond
      (isa? (class model) ParallelTopicModel)
      model
      (isa? (class model) PersistentVector)
      (first model)
      :default
      (throw (Exception. (str file " is not a valid model file. Its class is " (class model))))
      )
    )
  )

(defn norm [ma]
  (m/div ma (m/esum ma))
  )
(defn infer-topic-dist [model text & [source]]

  (let [^TopicInferencer inferer (if (= MultiBackgroundTopicModel (class model))
                                   (.getInferencer  ^MultiBackgroundTopicModel model source)
                                   (.getInferencer  model)
                                   )
        freezed-alphabet (doto
                           (.alphabet model)
                           (.stopGrowth))
        instance-list (InstanceList. (full-pipe nil freezed-alphabet))]
    (do (.addThruPipe instance-list (Instance. text nil "testing instance", nil))
        (-> (.getSampledDistribution inferer (nth instance-list 0) 100 1 5)
            (cond-> (= MultiBackgroundTopicModel (class model))
                    (butlast))
            (norm))
        )
    ))


(defn ^ParallelTopicModel load-model [file]
  (let [model ^ParallelTopicModel (deserialize-model file)]
    (.stopGrowth (.getAlphabet model))
    (set! (.data model) (ArrayList. (.subList (.data model) 0 1)))
    model
    ))

(defn topic-titles [model]
  (let [buf (ByteArrayOutputStream.)]
    (.topicPhraseXMLReport
      model
      (PrintWriter. buf true)
      20)

    (->> buf
         (.toByteArray)
         (ByteArrayInputStream.)
         (xml/parse)
         (:content)
         (map #(get-in % [:attrs :titles]))
         )
    ))


