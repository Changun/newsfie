

(ns lifestreams-db.slack
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [ring.util.codec :refer [url-encode form-encode]]
            [cheshire.core :as json]
            [net.cgrand.enlive-html :as html])
  (:use [aprint.core]
        )
  (:import (org.httpkit.client TimeoutException)
           (java.io IOException)
           (org.apache.commons.io IOUtils)))
(def client-id "4794600304.10316703589")
(def client-secret "6e13867a82311add09bf9188cd837182")
(def ^:private slack-api-uri "https://slack.com/api/")
(def ^:private scope "read")

(defn- slack-options
  "merge base options with the given options"
  [access-token options]
  (-> options
      (assoc :timeout 2000)
      (assoc-in [:headers "Authorization"] (str "Bearer " access-token))
      (assoc-in [:headers "Accept-Encoding"] " gzip")
      (assoc-in [:headers "User-Agent"] " my program (gzip)")
      )
  )

(defn- slack-endpoint [path options access-token]
  (let [{:keys [error status] :as res}
        @(http/get (str slack-api-uri path)
                   (slack-options access-token options)
                   )

        ]
    (if (or (= status 429)
            (isa? (class error) TimeoutException)
            (isa? (class error) IOException)
            )
      (recur path options access-token)
      res))
  )



(defn- channel-sequence [token ]
  (-> (slack-endpoint "channels.list"
                      {:query-params
                       {"exclude_archived"  "1"
                        "token" token
                        }
                       } token)
      :body
      (json/parse-string true)
      :channels
      )
  )

(defn- history-sequence
  [token channel & [latest]]
  (let [{:keys [messages has_more]}
        (->
          (slack-endpoint "channels.history"
                          {:query-params
                           (cond-> {"channel"  channel
                                "token" token
                                }
                               latest
                               (assoc "latest" latest))
                           }
                          token)
                 :body
                 (json/parse-string true)

                 )]
    (if has_more
      (lazy-seq (concat messages (history-sequence token channel (:ts (last messages)))))
      messages
      )
    )
  )
(defn- channel-histories->instance [channel histories]
  {:content (->
              (->> histories
                   (filter :text)
                   (filter (complement :subtype))
                   (map :text)
                   (clojure.string/join " ")
                   (html/html-snippet)
                   (html/texts)
                   (clojure.string/join " ")
                   )
              (clojure.string/replace #"<[^>]+>" ""))
   :sources [{:type :slack
              :name channel}]}
  )
;;;;;;;;;;;;;;;;;;;;;;; Public functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn get-auth-uri [callback-uri]
  (str "https://slack.com/oauth/authorize?"
       (form-encode
         {:redirect_uri  callback-uri
          :client_id     client-id
          :scope         scope}))
  )
(defn get-access-token [code callback-uri]
  (let [{:keys [error status body]}
        @(http/post "https://slack.com/api/oauth.access"
                    {:form-params {"code"          code
                                   "client_id"     client-id
                                   "client_secret" client-secret
                                   "redirect_uri"  callback-uri
                                   }}
                    )

        ]
    (if (or error (not= status 200))
      (throw (or error (Exception. status body)))
      (-> body (parse-string true) :access_token)))
  )

(defn get-instances [token]

  (->> (for [{:keys [id name]} (channel-sequence token)]
         (channel-histories->instance name (history-sequence token id))
         )
       (filter #(> (.length (:content %)) 300))
       )
  )