(ns lifestreams-db.nyt
  (:require
    [cheshire.core :as json]
    [org.httpkit.client :as http]
    [net.cgrand.enlive-html :as html]
    [taoensso.timbre
     :refer (error info)]
    )
  (:import
    (java.io IOException)
    (java.net URLEncoder)
    (org.httpkit.client TimeoutException)

    ))


(defn get-url [url]
  (->
    @(http/get (str "http://archive.org/wayback/available?url=" url))
    (:body)
    (slurp)
    (json/parse-string true)
    (:archived_snapshots)
    (:closest)
    (:url))

  )

(def url "http://web.archive.org/cdx/search/cdx?url=nytimes.com/%s/*&output=json&collapse=urlkey&offset=%s")
(defn wayback-nyt
  ([year] (wayback-nyt year 0))
  ([year offset] (let [{:keys [body]} @(http/get (format url year offset))
                       ret (drop 1 (json/parse-string body true))
                       ]
                   (if (< (count ret) 1)
                     nil
                     (lazy-seq (concat ret (wayback-nyt year (+ offset (count ret)))))
                     )
                   ))
  )

(defn download []
  (->> (concat  (wayback-nyt 2015) (wayback-nyt 2014) (wayback-nyt 2013) (wayback-nyt 2012) )
       (filter #(re-matches #".+/\d+/\d+/\d+/\w+/[\w+-]+\w+\.html.+" (nth % 2)))
       (map vector  (range))

       (pmap (fn [[ i [_ _ url _ _ id _]]]
               (while  (not (.exists (clojure.java.io/as-file (str "nyt/" id))))
                 (let [text (->
                              @(http/get (str "http://web.archive.org/web/" url))
                              (:body)
                              )]
                   (spit (str "nyt/" id)  text)
                   )
                 (println i url ))


               ) )
       (doall)
       )
  )

(->
  @(http/get (str "http://web.archive.org/web/20150530202146/" url))
  (:body)
  (html/html-snippet )
  (html/select   [:.story-content])
  (butlast)
  html/texts
  )


(with-open [out (clojure.java.io/writer "nyt.json")]
  (->>
    "nyt/"
    (clojure.java.io/file)
    (file-seq)
    (filter #(.isFile %))
    (pmap slurp)
    (pmap html/html-snippet)
    (pmap (fn [page]
            (try
              (let [metas (->
                            page
                            (html/select  [:meta])
                            )
                    get-meta-by-name (fn [name]
                                       (->
                                         metas
                                         (html/select  [[:meta (html/attr= :name name)]])
                                         (first)
                                         :attrs
                                         :content
                                         ))

                    title  (get-meta-by-name "hdl")
                    news-keywords (get-meta-by-name "news_keywords")
                    keywords (get-meta-by-name "keywords")

                    text (->>
                           (concat
                             (-> page
                                 (html/select  [[:p (html/attr= :itemprop "articleBody")]])
                                 (html/texts)
                                 )
                             (-> page
                                 (html/select  [:div.articleBody])
                                 (html/texts)
                                 )
                             (-> page
                                 (html/select  [:div#articleBody])
                                 (html/texts)
                                 )
                             (-> page
                                 (html/select  [[:p (html/attr= :itemprop "reviewBody")]])
                                 (html/texts)
                                 )
                             (-> page
                                 (html/select  [:div.reviewBody])
                                 (html/texts)
                                 )
                             (-> page
                                 (html/select  [:div#reviewBody])
                                 (html/texts)
                                 )

                             )
                           (clojure.string/join " ")
                           )

                    ]

                (println (count text) title)
                (if title
                  {:title title
                   :news_keywords (if news-keywords
                                    (clojure.string/split news-keywords #",")
                                    )

                   :keywords (if keywords
                                    (clojure.string/split keywords #",")
                                    )
                   :text    text
                   :pdate   (get-meta-by-name "pdate")

                   }
                  )
                )

              (catch Exception e (error e)
                                 (println (->
                                            page
                                            (html/select  [[:meta (html/attr= :name "news_keywords")]])
                                            (first)
                                            :attrs
                                            :content
                                            ))
                                 ) )
            ))
    (filter identity)
    (#(json/generate-stream % out))



    )
  )


