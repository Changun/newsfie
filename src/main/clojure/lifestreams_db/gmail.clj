(ns lifestreams-db.gmail
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]
            [base64-clj.core :as base64]
            [net.cgrand.enlive-html :as html]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.async :as async]
            [lifestreams-db.google-api :as gapi]
            [ring.util.codec :refer [url-encode form-encode]]
            )
  (:use [aprint.core]
        )
  (:import (java.util UUID)
           (clojure.lang ISeq IPersistentMap)
           (org.httpkit.client TimeoutException)
           (java.io IOException)))

(def ^:private gmail-message-api-uri "https://www.googleapis.com/gmail/v1/users/me")
(def ^:private scope "https://www.googleapis.com/auth/gmail.readonly")

(defn- gmail-options
  "merge base options with the given options"
  [access-token options]
  (-> options
      (assoc :timeout 2000)
      (assoc-in [:headers "Authorization"] (str "Bearer " access-token))
      (assoc-in [:headers "Accept-Encoding"] " gzip")
      (assoc-in [:headers "User-Agent"] " my program (gzip)")
      (assoc-in [:query-params :quotaUser] (UUID/randomUUID))
      )
  )

(defn- gmail-endpoint [path options access-token]
  (let [{:keys [error status] :as res}
        @(http/get (str gmail-message-api-uri path)
                   (gmail-options access-token options)
                   )

        ]
    (if (or (= status 429)
            (isa? (class error) TimeoutException)
            (isa? (class error) IOException)
            )
      (recur path options access-token)
      res))
  )

(defn- gmail-endpoint-async [path options access-token ch]
  (http/get (str gmail-message-api-uri path)
            (gmail-options access-token options)
            (fn [{:keys [status headers body error] :as response}] ;; asynchronous response handling
              (if (or
                    (isa? (class error) IOException)
                    (isa? (class error) TimeoutException)
                    (= status 429))
                (gmail-endpoint-async path options access-token ch)
                (async/put! ch response)
                ))
            )
  )

(defn- thread-sequence
  ([filter access-token & [labels page-token]]
   (let [params (cond-> {}
                        filter
                        (assoc :q filter)
                        page-token
                        (assoc :pageToken page-token)
                        labels
                        (assoc :labelIds labels)
                        )
         {:keys [status body error]} (gmail-endpoint "/threads" {:query-params params} access-token)]
     (if (or error (not= status 200))
       (throw (Throwable. (str "Failed, exception: " error " " status body)))
       (let [{:keys [nextPageToken threads]} (parse-string body true)]
         (if nextPageToken
           (concat threads (lazy-seq (thread-sequence filter access-token labels nextPageToken)))
           threads
           )
         )
       )))
  )


(defn- get-thread-async
  ([id access-token ch & {:keys [format] :or {format "full"}}]
   {:pre ([(= 16 (count id))])}
   (let [options {:query-params {:format format}}]
     (gmail-endpoint-async (str "/threads/" id) options access-token ch)
     )
    )
  )


(defmulti extract-content-in-html class)
(defmethod extract-content-in-html IPersistentMap [{:keys [content]}]
  (mapcat extract-content-in-html content)
  )
(defmethod extract-content-in-html ISeq [coll]
  (mapcat extract-content-in-html coll)
  )
(defmethod extract-content-in-html String [s]
  [(clojure.string/replace s #"&\w\w\w+;" " ") ]
  )
(defmethod extract-content-in-html :default [_]
  nil
  )


(defmulti decode-payload :mimeType)


(defn- decode-multipart [payload]
  (let [fisrt-of-type (fn [type] (first (filter #(= (:mimeType %) type) (:parts payload))))
        ]
    (merge
      {:subject (:value (first (filter #(= (:name %) "Subject") (:headers payload))))}
      ;(decode-payload (fisrt-of-type "text/html"))
      (decode-payload (fisrt-of-type "text/plain")))
    ))
(defmethod decode-payload "multipart/alternative" [payload]
  (decode-multipart payload)
  )
(defmethod decode-payload "multipart/mixed" [payload]
  (decode-multipart payload)
  )
(defn- decode-text-payload
  [payload]
  (let [body (get-in payload [:body :data])]
    (if (empty? body)
      "" (base64/decode body)
      )
    )
  )
(defmethod decode-payload "text/html" [payload]
  (let [html-body (decode-text-payload payload)
        body (html/select (html/html-snippet html-body) [:body])]
    {:links (for [link (html/select body [:a])]
              {:href (get-in link [:attrs :href])
               :text (clojure.string/join " " (extract-content-in-html link))}
              )
     :text  (clojure.string/join " " (extract-content-in-html body))
     }
    )
  )

(defmethod decode-payload "text/plain" [payload]
  {:text (decode-text-payload payload)}
  )
(defmethod decode-payload :default [payload]
  nil
  )

(defn- thread->text [{:keys [messages]}]
  (letfn [(remove-quote-and-sig [text]
                                (let [*m* (re-matcher #"(^|\n)([->]|([Oo]n [MmTtWwFfSs]))" text)]
                                  (if (.find *m*)
                                    (subs text 0 (.start *m*))
                                    text
                                    )
                                  ))]

    (->> messages
         (map #(p :decode (decode-payload (:payload %))))
         (map #(str (:subject %) "\n" (:text %)))
         (map #(p :remove-sig-quote (remove-quote-and-sig %)))
         (clojure.string/join "\n")
         )
    )

  )


(defn- header [{:keys [payload]} key]
  (->> payload
       (:headers)
       (filter #(= (keyword (clojure.string/lower-case (:name %))) key))
       (first)
       (:value)
       )
  )

(defn- parse-contacts [value]
  (if (seq value)
    (for [person (clojure.string/split value #"\s*,\s*")]
      (let [matcher (re-matcher #"\s*([^<>]+)\s*<(.+@.+)>" person)]
        (if (.find matcher)
          (let [[full name email] (re-groups matcher)]
            {:email (clojure.string/trim email)
             :name  (clojure.string/trim name)
             :full  full}
            )
          )

        )
      ))
  )
(defn- message->recipients [msg]
  (keep identity (concat (parse-contacts (header msg :to))
                         (parse-contacts (header msg :cc))))
  )
(defn- message->sender [msg]
  (first (parse-contacts (header msg :from)))
  )

;; these functions will be defined in the public function sections
(declare hangout? sent?)

(defn- thread->contacts [thread]
  (distinct (apply concat
                   (for [msg (:messages thread)]
                     (cond
                       (hangout? msg) [{:name :Hangout}]
                       (sent? msg) (message->recipients msg)
                       :default [(message->sender msg)]

                       )
                     )))
  )



(defn- parse-thread-response [{:keys [body status error] :as res}]
  (cond (= 200 status)
        (let [thread (parse-string body true)]
          (assoc thread :content (thread->text thread)
                        :sources (->> (thread->contacts thread)
                                      (map (fn [{:keys [name]}]
                                             {:name name
                                              :type :email}
                                             ))
                                      (distinct)
                                      )
                        )
          )
        (= 404 status)
        :not-found
        :default
        (do
          (throw (or error (Exception. (str status body)))))
        )
  )

;;;;;;;;;;;;;;;;; Public Functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn get-auth-uri [callback-uri]
  (gapi/get-auth-uri scope callback-uri)
  )
(defn get-access-token [code callback-uri]
  (gapi/get-access-token scope code callback-uri)
  )

(defn user-profile
  "Return the user's gmail profile"
  [token]
  (let [{:keys [status body error]} (gmail-endpoint "/profile" {} token)]
    (if (or error (not= status 200))
      (throw (Throwable. (str "Failed, exception: " error " " status body)))
      (parse-string body true)
      )
    )

  )
(defn hangout?
  "If the message is a hangout conversation"
  [msg]
  (and (not (header msg :to))
       (header msg :from))
  )
(defn sent?
  "If the message was sent by the user"
  [{:keys [labelIds] :as msg}]
  (if (not (hangout? msg))
    (some #(= % "SENT") labelIds))
  )
(defn important?
  "If the message has been labeled as IMPORTANT"
  [{:keys [labelIds]}]
  (some #(= % "IMPORTANT") labelIds)
  )
(defn latest-threads-async
  "Return a lazy sequence that syncronously return email threads that matches the specified query in a
  non-strict reversed chronological order.  Under the hood, this function sends requests asyncronously
  to GMail API to improve the throughput so that the order of email thread cannot be quaranteed."

  ([token & {:keys [q] :or {q "is:read from:(-{noreply notifications bank}) subject:(-Statement) "}}]
   (let [id-seq (->> (thread-sequence q token)
                     (map :id)
                     )
         ; chan that hold response from http requests
         response-chan (async/chan 1000)
         ; chan that hold parsed http response (the response could be nil) if thread is not found
         output-chan (async/chan 1000 (map parse-thread-response))
         ; chan used to throttle the number of look ahead requests
         throttler-chan (async/chan 1000)
         ]

     (async/pipe response-chan output-chan)
     (async/go-loop [[id & ids] id-seq]
       ; async put id to the throttler-chan or park if throttler-chan is full
       (if (and id (async/>! throttler-chan id))
         (do
           ; issue request
           (get-thread-async id token response-chan :format "full")
           (recur ids))
         (do
           ; no more thread to fetch. close all channels
           (async/close! throttler-chan)
           (async/close! response-chan)
           (async/close! output-chan)
           )
         ))
     ; create a lazy sequence that returns each found threads
     ((fn output-seq []
        (async/<!! throttler-chan)
        (let [output (async/<!! output-chan)]
          (condp = output
            ; output chan is closed. end of the sequence
            nil nil
            ; the thread is not found
            :not-found (lazy-seq (output-seq))
            ; cons the found thread
            (cons output (lazy-seq (output-seq)))
            )
          )

        ))

     )
    ))
















