(ns lifestreams-db.google-api
  (require [org.httpkit.client :as http]
           [ring.util.codec :refer [url-encode form-encode]]
           [cheshire.core :refer :all])
  (use [lifestreams-db.config]))

(def ^:private client-id (get-in @config [:google :consumer-key]))
(def ^:private client-secret (get-in @config [:google :consumer-secret]))


(defn get-auth-uri [scope callback-uri]
  (str "https://accounts.google.com/o/oauth2/auth?"
       (form-encode
         {:redirect_uri  callback-uri
          :response_type "code"
          :client_id     client-id
          :scope         scope}))
  )
(defn get-access-token [scope code callback-uri]
  (let [{:keys [error status body]}
        @(http/post "https://www.googleapis.com/oauth2/v3/token"
                    {:form-params {"code"          code
                                   "client_id"     client-id
                                   "scope"         scope
                                   "client_secret" client-secret
                                   "redirect_uri"  callback-uri
                                   "grant_type"    "authorization_code"
                                   }}
                    )

        ]
    (if (or error (not= status 200))
      (throw (or error (Exception. status body)))
      (-> body (parse-string true) :access_token)))
  )
(defn revoke-token [token]
  @(http/get (str "https://accounts.google.com/o/oauth2/revoke?token=" token)))
