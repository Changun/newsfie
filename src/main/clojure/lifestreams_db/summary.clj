(ns lifestreams-db.summary
  (:require [monger.collection :as mc]

            [monger.core :as mg]
            [incanter.stats :as stats]
            [clojure.core.matrix :as m]
            [lifestreams-db.medium :as medium]
            [lifestreams-db.meetup :as meetup]
            [experiment.analysis :as analysis])
  (:use [incanter.core])
  )


(def conn (mg/connect))
(def db (mg/get-db conn "twitter"))


(defn rating-for [session]
  (->> (flatten
         (for [[log user] (map vector

                               (remove (comp #{36 47 41} :user-id)
                                       (mc/find-maps   db "logs" {:info.pilot false
                                                                  :info.startTime {:$gt "2015-10-05"}
                                                                  ;69 50
                                                                  }))
                               (range))]

           (for [[session data] (:data log)]
             (for [{:keys [recommended-by rating batchId]} data]
               (for [method recommended-by ]
                 {:user user
                  :session session
                  :method method
                  :rating (if rating (identity rating))
                  :stage batchId
                  }
                 )

               )
             )
           ))
       (group-by :user)
       (filter (fn [[_ r]] (and (= 4 (count (into #{} (map #(select-keys % [:session :stage]) r)))) (every? :rating r) (seq r))))
       (mapcat second)
       (filter #(= (:session %) session))
       (map :rating)
       (frequencies)
       ))
(def medium-scale
  (let [freq (rating-for :medium)]
    (let [sum (apply + (map second freq))
          p (map #(/ % sum 1.0) (map second  (sort-by first freq)) )]
      (into {} (map #(vector %3 (- %1 (/ %2 2))) (cumulative-sum p) p (range 1 6)))
      )

    )
  )
(def meetup-scale
  (let [freq (rating-for :meetup)]
    (let [sum (apply + (map second freq))
          p (map #(/ % sum 1.0) (map second  (sort-by first freq)) )]
      (into {} (map #(vector %3 (- %1 (/ %2 2))) (cumulative-sum p) p (range 1 6)))
      )

    )
  )

(def likert->continuos #((if (= (:session %) :medium) medium-scale meetup-scale) (:rating %)))


(defn get-dat [rate-fn & [sum-fn]]
  (->>(for [{:keys [data user-id profiles]}
            (->>
              (mc/find-maps   db "logs" {:info.pilot false :info.startTime {:$gt "2015-10-05"}})
              (filter (comp seq :profiles))
              (remove (comp #{36 47 41} :user-id))
              (filter (fn [{:keys [data]}]
                        (and (every? :rating (mapcat second data))
                             (= 2 (count (into #{} (map :batchId (second (first data))))) )
                             (= 2 (count (into #{} (map :batchId (second (second data))))) ))

                        )))
            ]

        (for [[session data] data]
          (for [{:keys [recommended-by rating batchId] :as item} data]
            (for [method recommended-by ]
              {:user            user-id
               :session         session
               :method          method
               :rating          (if rating ((or rate-fn likert->continuos) (assoc item :session session :method method :stage batchId)))
               :precision-4      (if (> rating 3) 1 0)
               :precision-5      (if (> rating 4) 1 0)
               :rating-raw rating
               :stage           batchId
               :profiles        (count profiles)
               :gmail           (if (:gmail profiles) 1 0)
               :twitter         (if (:twitter profiles) 1 0)
               :facebook        (if (:facebook profiles) 1 0)
               :profile-entropy (analysis/entropy (->> profiles
                                                       (map second)
                                                       (filter m/vec?)
                                                       (apply m/add)
                                                       ))

               }
              )

            )
          )
        )
       (flatten)
       (group-by #(dissoc % :rating :precision-4 :precision-5 :rating-raw) )
       (map #(assoc (first %)
              :rating ((or sum-fn stats/mean) (map :rating (second %) ))
              :precision-4 ((or sum-fn stats/mean) (map :precision-4 (second %) ))
              :precision-5 ((or sum-fn stats/mean) (map :precision-5 (second %) ))

              :rating-raw ((or sum-fn stats/mean) (map :rating-raw (second %) ))
              ))
       )
  )
(defn get-flat-dat [rate-fn & [sum-fn]]
  (->>
    (for [[u r] (get-dat rate-fn sum-fn)]
      (for [[k v] r]
        (assoc (select-keys u [:session :stage :user]) :method k :val v)
        )
      )
    (flatten))
  )
(defn avg-rating [rate-fn]
  (->> (get-flat-dat rate-fn)
       (group-by #(select-keys % [:method :stage :session]))
       (map (fn [[k r]] (assoc k :avg (stats/mean (map :val r)) :sem (/ (stats/sd (map :val r)) (Math/pow (count r) 0.5)))))
       (sort-by #(str (:session %) (:stage %)  (:method %)))
       )

  )


(defn MRR [r]
  (flatten
    (for [[ user {:keys [data profiles user-id]}] (->> (mc/find-maps   db "logs" {:info.pilot false :info.startTime {:$gt "2015-10-05"}})
                                               (filter :profiles)
                                               (filter (fn [{:keys [data]}]
                                                         (and (every? :rating (mapcat second data))
                                                              (= 2 (count (into #{} (map :batchId (second (first data))))) )
                                                              (= 2 (count (into #{} (map :batchId (second (second data))))) ))

                                                         ))
                                               (map vector (range)))]

      (for [[session data] data]

        (let [user user-id
              {:keys [stories ours ctr pmf orig-topics]} (if (= session :medium)
                                                           medium/cf-models
                                                           meetup/cf-models)

              pref (->> profiles
                        (map second)
                        (filter m/vec?)
                        (apply m/add)
                        )

              truth (->> (filter #(>= (:rating %) r) data)
                         (map :id)
                         (into #{}))
              ratings (->> data
                                      (filter #(= 0 (:batchId %)))
                                      (remove #(= (:recommended-by %) ["ours"]))
                                      (map (fn [%] [(:id %) (/ (- (:rating %) 1) 4)]))
                                      (into {})

                                      )
              second-batch-truth (->> (filter #(>= (:rating %) r) data)
                                      (filter #(= 1 (:batchId %)))
                                      (map :id)
                                      (into #{}))
              first-batch-truth (->> (filter #(>= (:rating %) r) data)
                                      (filter #(= 0 (:batchId %)))
                                      (map :id)
                                      (into #{}))

              first-batch (->>
                               (filter #(= 0 (:batchId %)) data)
                               (map :id)
                               (into #{}))
              pref->stories (fn [model ratings]
                              (let [{:keys [topics fold-in]} model
                                    pref (cond-> pref
                                                 (seq ratings)
                                                 (fold-in ratings)
                                                 )]
                                (->>(m/inner-product topics pref)
                                    (map #(vector %1 %2) stories)
                                    (sort-by second)
                                    (reverse)
                                    (map first)

                                    )
                                )
                              )
              stories->first-truth (fn [ truth stories ignore?]
                                     (+ 1 (or
                                            (->>
                                                 (filter (complement ignore?) stories)
                                                 (map vector  (range) )
                                                 (drop-while (comp (complement truth) second))
                                                 (first)
                                                 (first)
                                                 )
                                            (count stories)
                                            ))
                                     )

              ours-stories (pref->stories ours  {})

              ours-stories-2 (pref->stories ours  ratings)
              most-popular stories
              pmf (pref->stories pmf ratings)
              ctr (pref->stories ctr ratings)
              ]

          [{:session session
            :method "most-popular"
            :stage 0
            :user user
            :mrr (float (/ 1 (stories->first-truth first-batch-truth most-popular #{}) ))
            }
           {:session session
            :method "most-popular"
            :stage 1

            :user user
            :mrr (float (/ 1 (stories->first-truth second-batch-truth most-popular first-batch) ))
            }
           {:session session
            :method "ours"
            :mrr (float (/ 1 (stories->first-truth first-batch-truth ours-stories #{})))
            :stage 0

            :user user
            }
           {:session session
            :method "ctr"
            :mrr (float (/ 1 (stories->first-truth second-batch-truth ctr first-batch)))
            :stage 1

            :user user
            }

           {:session session
            :method "pmf"
            :mrr (float (/ 1 (stories->first-truth second-batch-truth pmf first-batch)))
            :stage 1

            :user user
            }
           {:session session
            :method "ours"
            :mrr (float (/ 1 (stories->first-truth second-batch-truth ours-stories-2 first-batch)))
            :stage 1
            :user user
            }
           ]
          )
        )
      )))
(def dat ($join [[:user :stage :session :method] [:user :stage :session :method]] (to-dataset (MRR 5)) (to-dataset (get-flat-dat nil)))  )
(defn paired-t-test [rate-fn a b & [n]]
  (->>
    (for [[u r] (get-dat rate-fn)]
      [(select-keys u [:stage :session])
       (if (and (r a) (r b))
         (- (r a) (r b)))

       ]
      )
    (filter second)
    (group-by first)
    (map (fn [[g recs]] (let [r (map second recs)
                              percent (map last recs)]
                          [g (stats/mean r) (stats/mean percent)
                           (* 2 (- 1 (stats/cdf-t (* (/ (stats/mean r)
                                                        (stats/sd r))
                                                     (Math/pow (or n (count r) ) 0.5 )) :df (- (or n (count r) ) 1)) ))]
                          )))
    )
  )