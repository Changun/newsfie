(ns lifestreams-db.crawler
  (:require [clojure.java.jdbc :as sql]
            [lifestreams-db.twitter :as tw]
            [lifestreams-db.data :as data]
            [lifestreams-db.postgres :refer [pooled-db]]
            [lifestreams-db.medium :as medium]
            [taoensso.timbre :as timbre
             :refer (error info)]

            [clojure.core.matrix :as m]
            [lifestreams-db.ctr :as ctr])
  (:import (org.joda.time LocalDate DateTime)))


(defn followees-tweets
  "Fetch tweets of users whom the Medium.com user follow"
  []
  (doseq [{:keys [name]}
          (sql/query @pooled-db "SELECT lower(followee) AS name
                                 FROM twitter_follow LEFT OUTER JOIN twitter_entity ON lower(followee)=lower(name)
                                 WHERE name is NULL GROUP BY lower(followee) ORDER BY count(*) DESC")]
    (try
      (let [tweets (take 200 (tw/get-user-timeline nil (subs name 1)))]

        (data/save-entity-tweets @pooled-db (clojure.string/lower-case name) tweets)
        (if (>= (count tweets) 100)
          (data/update-twitter-entity-topics @pooled-db name
                                             (medium/infer-topics (tw/tweets->content tweets)))
          )
        (println name (count tweets)))
      (catch Exception e (error e)))
    )
  )
(defn update-tweet-topics
  []
  (pmap (fn [{:keys [name]}]
          (data/update-twitter-entity-topics @pooled-db name (medium/infer-topics (data/get-agg-tweets-with-entity-name @pooled-db name)))
          (println "Update topics for " name)
          )
        (sql/query @pooled-db "SELECT name FROM
                                        (SELECT name, count(*) AS count FROM twitter_entity
                                         INNER JOIN twitter_entity_tweet ON entity_id=twitter_entity.id
                                         WHERE twitter_entity.topics IS NULL GROUP BY name) a
                                        WHERE count >= 100;
                                         "))
  )

(defonce twitter-name->twitter-profile
         (memoize
           (fn [tname]
             (or
               (data/get-twitter-profile-cache @pooled-db tname)
               (let [tuser (if (= (first tname) \@) tname (str "@" tname))
                     friends (future (data/get-twitter-friends-topics-with-entity-name @pooled-db tuser))
                     hashtags (future (data/get-hashtags-topics-with-entity-name @pooled-db tuser))
                     tweets-topics (future (or (data/get-twitter-entity-topics @pooled-db tuser)
                                               (if-let [text (data/get-agg-tweets-with-entity-name @pooled-db tuser)]
                                                 (medium/infer-topics text))
                                               ))
                     tweets-count (future (data/get-tweets-count-with-entity-name @pooled-db tuser))
                     instances (concat
                                 [{:topics (or @tweets-topics
                                               (m/zero-vector (count medium/topic-titles)))
                                   :weight @tweets-count}]
                                 (->> @friends
                                      (filter identity)
                                      (map (fn [%] {:topics % :weight 1}))
                                      )
                                 (->> @hashtags
                                      (filter identity)
                                      (map (fn [%] {:topics % :weight 0.5}))
                                      )

                                 )

                     ]
                 (when (> (apply + (map :weight instances)) 100)
                   (let [sum-of-product (->>
                                          (map :topics instances)
                                          (m/matrix)
                                          (m/mmul (map :weight instances))
                                          )
                         profile (m/div sum-of-product (m/esum sum-of-product))]
                     (data/cache-twitter-profile @pooled-db tuser profile)
                     profile

                     )
                   )

                 ))
             ))
         )
(defonce story->topic (memoize #(medium/infer-topics (:text (data/get-story-with-id @pooled-db %)))))


(defn new-stories-recommended-by-users
  "Fetch new storeis that the existing users upvoted.
  We only visit a user one out of fives time to shorten the update time."
  []
  (doall
    (pmap
      (fn [{:keys [medium_user_name] :as person}]
        (println "Recommended by" medium_user_name)
        (try (let [library (data/get-user-medium-library @pooled-db medium_user_name)]
               (doseq [story (apply medium/user-recommended-stories medium_user_name library)]
                 (future
                   (when (not (data/get-story-with-id @pooled-db (:id story)))
                     (try (data/save-medium-stories @pooled-db (medium/get-story story))
                          (catch Exception e
                            ; the other thread may already save the story
                            (if-not (data/get-story-with-id @pooled-db (:id story))
                              (throw e))
                            ))
                     (println (:id story))
                     )
                   (data/save-person-medium-like @pooled-db person [story]))
                 )
               )
             (catch Exception e (error e)))      )
      (random-sample 0.5 (data/get-people @pooled-db))))
  )

(defn new-top-stories
  "Fetch all top stories (the ones recommended by MediumStaff and the most popular stories)"
  []
  (let [save-story (fn [story]
                     (try
                       (when (not (data/get-story-with-id @pooled-db (:id story)))
                         (data/save-medium-stories @pooled-db (medium/get-story story))
                         (println (:id story))
                         )
                       (catch Exception e (error e))))]
    (println "Check MediumStaff Recommended Story")
    (try
      (doseq [top-story (apply medium/user-recommended-stories "mediumstaff" (data/get-user-medium-library @pooled-db "mediumstaff"))]
        (save-story top-story)
        )
      (catch Exception e (error e)))
    (println "Check Top Stories")
    (try
      (doseq [top-story (medium/top-story-sequence (LocalDate.))]
        (save-story top-story)
        )
      (catch Exception e (error e)))
    )
  )

(defn new-medium-users
  "Discover new users from stories' upvotes records "
  []
  (let [existing? (into #{} (map :medium_user_name (data/get-people @pooled-db)))]
    (doseq [{:keys [twitterScreenName facebookAccountId username]}
            (->>
              (sql/query @pooled-db ["SELECT json_extract_path(json,'upvotes','users') AS users FROM medium_story"])
              (mapcat :users)
              (distinct)
              )]
      (if (not (existing? (clojure.string/lower-case username)))
        (let [person {:twitter_screen_name (if (seq twitterScreenName) (str "@" twitterScreenName) nil)
                      :facebook_id         (if (seq facebookAccountId) (str facebookAccountId) nil)
                      :medium_user_name    username}]
          (try
            (data/save-person @pooled-db person)
            (catch Exception e (error e)))
          ))
      ))
  )


(defn update-latest-stories
  "Train a Collaborative Filtering model with the stories in last six months"
  []
  (let [rets (sql/query @pooled-db [
                                    "SELECT twitter_screen_name AS tname, person.id as uid, medium_story.id as sid
                                    FROM medium_story
                                    INNER JOIN medium_like ON story_id=medium_story.id
                                    INNER JOIN person ON person.id=person_id
                                    WHERE published_at > ? AND upvote_count > 5"
                                    (.minusMonths (DateTime.) 9)
                                    ])
        docs (distinct (map :sid rets))
        users (distinct (map :uid rets))
        _ (println "Doc count " (count docs) "User count" (count users))
        user->tname (into {} (distinct (map vector (map :uid rets) (map :tname rets))))

        Theta (into {} (pmap #(vector % (story->topic %)) docs))
        Gamma (into {} (pmap #(vector % (when-let [tname (user->tname %)]
                                         (twitter-name->twitter-profile tname)
                                         )) users)
                    )
        [U V] (ctr/estimate Theta
                            Gamma
                            (for [{:keys [uid sid]} rets]
                              [uid sid]
                              )
                            1
                            0.01
                            10
                            40
                            10
                            nil
                            )

        _ (info "Done offset estimation")
        ]

    (sql/with-db-transaction [db @pooled-db]
                             (sql/execute! db ["TRUNCATE latest_medium_story"])
                             (apply sql/insert! db "latest_medium_story"
                                    (for [[did topics] V]
                                      (merge (first (sql/query db ["SELECT * FROM medium_story WHERE id = ?" did]))
                                             {:id     did
                                              :topics (seq topics)
                                              :orig_topics (Theta did)})


                                      ))
                             )
    (info "Done updating articles")

    )
  )

(defn update-medium []
  (loop []
    (println "New Top Stories")
    (new-top-stories)
    (println "New Stories Recommended by Users")
    (new-stories-recommended-by-users)
    (println "New Medium.com users")
    (new-medium-users)
    (update-latest-stories)
    (Thread/sleep 1000)
    (recur))
  )


