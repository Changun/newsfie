(ns lifestreams-db.medium
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]
            [clojure.java.jdbc :as sql]
            [lifestreams-db.data :as data]
            [lifestreams-db.postgres :refer [pooled-db]]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [taoensso.timbre
             :refer (error info)]
            [monger.core :as mg]
            [lifestreams-db.topic-model :as topics]
            [clojure.core.matrix :as m]
            [clojure.core.reducers :as r]
            [cheshire.core :as json]
            [clojure.core.matrix.linear :as ml])
  (:use [aprint.core]
        [slingshot.slingshot]
        [simple-progress.bar]
        )
  (:import (org.joda.time DateTime)
           (org.joda.time.format DateTimeFormat)
           (org.httpkit.client TimeoutException)
           (java.io IOException)
           ))

(m/set-current-implementation :vectorz)
(def model (topics/load-model "medium-new.bin"))
(def topic-titles (topics/topic-titles model))
(def twitter-weight (map #(if (= 0.0 %) 1E-30 %)
                         (json/parse-string (slurp "twitter-weight.json"))))
(def removed-topics [8 33 54 62 64 115 118 119 179])


(defn infer-topics [content]
  (apply topics/infer-topic-dist model content 100 removed-topics))

(def medium-url "https://medium.com")
(defn medium-endpoint [op path options]
  (let [request-fn (condp = op :GET http/get :POST http/post)
        {:keys [error status body] :as res}
        @(request-fn (str medium-url path)
                     (assoc options :headers (merge
                                               {"accept"        "application/json"
                                                "content-type"  "application/json"
                                                "x-xsrf-token"  "1"
                                                "x-obvious-cid" "web"}
                                               (:headers options)
                                               )))

        ]
    (cond
      (or (isa? (class error) TimeoutException)
          (isa? (class error) IOException)
          )
      (do (Thread/sleep 1000)
          (recur op path options))
      (or error (not= status 200))
      (throw+ res)
      :default
      res
      )
    )
  )
(def response-prefix-length (count "])}while(1);</x>"))
(defn body->json [body]
  (parse-string (subs body response-prefix-length) true)
  )

(defn story->collection [story]
  (-> story
      (:canonicalUrl)
      (clojure.string/split #"/")
      (butlast)
      (last)
      )
  )
(defn story->content [story]
  (->> (get-in story [:content :bodyModel :paragraphs])
       (map :text)
       (clojure.string/join "\n")
       )

  )
(defn get-story-meta [{:keys [id]} type]
  {:prev [(#{:upvotes :responses :notes :quotes} type)]}
  (-> (medium-endpoint :GET
                       (str "/p/" id "/" (name type))
                       {:query-params {:format "json"}})
      (:body)
      (body->json)
      (get-in [:payload :value]))
  )

(defn get-story-tags [{:keys [id]}]
  (-> (medium-endpoint :GET
                       (str "/_/api/posts/" id "/tags")
                       {:query-params {:format "json"}})
      (:body)
      (body->json)
      (get-in [:payload :value]))
  )

(defn get-story [{:keys [id] :as base}]
  (let [{:keys [body error]} (medium-endpoint :GET
                                              (str "/p/" id)
                                              {:query-params {:format "json"}})
        res (body->json body)
        story (get-in res [:payload :value])
        ]
    (merge base
           (assoc story :collection (story->collection story)
                        ;:responses   (get-story-meta story :responses)
                        ;:tags       (get-story-tags story)
                        :upvotes (get-story-meta story :upvotes)
                        ;:notes    (get-story-meta story :notes)
                        ;:quotes    (get-story-meta story :quotes)
                        :_id id))

    )
  )
(defn user-profile [user]
  (->
    (medium-endpoint :GET
                     (str "/@" user)
                     {:query-params {:format "json"}})
    (:body)
    (body->json)
    (get-in [:payload :value]))
  )
(defn user-recommended-stories [user & ignore]
  (let [{:keys [body error]}
        (medium-endpoint :POST
                         (format "/@%s/has-recommended" user)
                         {:body (generate-string {:count 100 :ignore (apply vector ignore)})})
        posts (get-in (body->json body) [:payload :posts])
        ]
    (if (> (count posts) 0)
      (do
        (concat posts (lazy-seq (apply user-recommended-stories
                                       user
                                       (distinct (concat ignore (map :id posts)))))))
      []
      )
    )
  )



(defn user-stories
  ([user] (user-stories user 0))
  ([user from] (user-stories user from (.getMillis (DateTime.))))
  ([user from to]

   (let [{:keys [body error]}
         (medium-endpoint :POST
                          (str "/" user "/load-more?sortBy=latest")
                          {:body (generate-string {:limit 100 :from from :to to})})
         posts (get-in (body->json body) [:payload :value])
         posts (filter #(>= (:createdAt %) from) posts)
         last-createdAt (:createdAt (last posts))
         ]
     ; (println "Fetch stories from " user " " (DateTime. from) " " (DateTime. to) " Last:" last-createdAt)
     (if (> (count posts) 0)
       (concat posts (lazy-seq (user-stories user from last-createdAt)))
       []
       )
     ))
  )

(defn top-stories [date]
  (let [date-str (.toLowerCase (.print (DateTimeFormat/forPattern "MMMM-dd-y") date))
        {:keys [body error]}
        (medium-endpoint :GET
                         (str "/top-stories/" date-str)
                         {})

        ]
    (get-in (body->json body) [:payload :value :posts])
    )

  )

(defn top-story-sequence [from]
  (let [posts (top-stories from)]
    (println from)
    (if (seq posts)
      (concat posts (lazy-seq (top-story-sequence (.minusDays from 1))))
      []
      )
    )
  )

(defn story-topics [{:keys [id] :as story}]
  (->> story
       (story->content)
       (#(topics/infer-topic-dist model % 200))
       (apply vector)
       (m/matrix)

       )
  )
(defn run-model [topics iterations]
  (let [docs (->> (sql/query @pooled-db "select id, text from medium_story")
                  (map #(identity {:data   (:text %)
                                   :label  "trainning"
                                   :name   (:id %)
                                   :source (:id %)}))

                  )]
    (topics/run-model docs topics iterations 0.02)
    )
  )

(defmacro try++ [& body]
  (let [e (gensym)]
    `(try+
       ~@body
       (catch #(#{410 502 404 504 503} (:status %)) ~e
         (binding [*out* *err*]
           (println "Ignore error " ~e)
           ))
       )
    )
  )
(defn fold-in-iteration [V XX story-id->index lambda_u profile likes dislikes]
  (println likes dislikes (first story-id->index) (map story-id->index (concat likes dislikes)))
  (let [a 1 b 0.01
        gamma-u profile
        num-factors (m/column-count V)
        a-minus-b (- a b)
        A (reduce (fn [A doc-id]
                    (let [v (m/row-matrix (m/get-row V doc-id))]
                      (m/add A (m/mul (m/mmul (m/transpose v) v) a-minus-b))
                      )
                    ) (m/clone XX) (map story-id->index (concat likes dislikes)))
        x (reduce (fn [x doc-id]
                    (let [v (m/get-row V doc-id)]
                      (m/add x (m/mul v a))
                      )
                    ) (m/zero-vector num-factors) (map story-id->index likes))
        x (m/add x (m/mul gamma-u lambda_u))
        A (m/add A (m/mul (m/identity-matrix num-factors) lambda_u))
        ]
    (m/to-vector (ml/solve (m/mul A 1e6) (m/mul x 1e6)))
    )
  )

(defn fold-in-iteration [V XX story-id->index lambda_u profile likes dislikes]
  (let [a 1 b 0.01
        gamma-u profile
        num-factors (m/column-count V)
        a-minus-b (- a b)
        A (reduce (fn [A doc-id]
                    (let [v (m/get-row V doc-id)]
                      (m/add A (m/mul (m/outer-product v v) a-minus-b))
                      )
                    ) XX (map story-id->index (concat likes dislikes)))
        x (reduce (fn [x doc-id]
                    (let [v (m/get-row V doc-id)]
                      (m/add x (m/mul v a))
                      )
                    ) (m/zero-vector num-factors) (map story-id->index likes))
        x (m/add x (m/mul gamma-u lambda_u))
        A (m/add A (m/mul (m/identity-matrix num-factors) lambda_u))
        ]
    (m/to-vector (ml/solve (m/mul A 1e6) (m/mul x 1e6)))
    )
  )

(defn fold-in-with-ratings [V XX story-id->index lambda_u profile ratings]
  (let [a 1 b 0.01
        gamma-u profile
        num-factors (m/column-count V)
        a-minus-b (- a b)
        A (reduce (fn [A doc-id]
                    (let [v (m/row-matrix (m/get-row V doc-id))]
                      (m/add A (m/mul (m/mmul (m/transpose v) v) a-minus-b))
                      )
                    ) (m/clone XX) (map story-id->index (map first ratings)))
        x (reduce (fn [x [doc-id rating]]
                    (let [v (m/get-row V (story-id->index doc-id) )]
                      (m/add x (m/mul v a rating))
                      )
                    ) (m/zero-vector num-factors) ratings)
        x (m/add x (m/mul gamma-u lambda_u))
        A (m/add A (m/mul (m/identity-matrix num-factors) lambda_u))
        ]
    (m/to-vector (ml/solve (m/mul A 1e6) (m/mul x 1e6)))
    )
  )


(def deadline (DateTime. 2015 7 1 0 0))



(def cf-models
  (let [b 0.01
        {:keys [ours pmf ctr theta]}
        (with-open [r (clojure.java.io/reader "medium_ousr_pmf_ctr_theta.json")]
          (json/parse-stream r true)
          )
        ours (sort-by first ours)
        pmf (sort-by first pmf)
        ctr (sort-by first ctr)
        story-id->index (into {} (map vector (map (comp name first) ours) (range)))
        ; this story has been removed
        avail? (disj (into #{} (map first ours)) :7b5e7add6fff)
        most-popular (->> (sql/query @pooled-db
                                     ["SELECT medium_story.id
                                            FROM medium_story
                                            INNER JOIN medium_like ON story_id = id
                                            WHERE text IS NOT NULL
                                                  AND upvote_count >= 5
                                                  AND published_at >= ?
                                            GROUP BY medium_story.id ORDER BY COUNT(medium_story.id)"
                                      deadline])
                          (map :id)
                          (reverse)
                          (filter #(= (:detectedLanguage (data/get-story-details-with-id @pooled-db %))  "en"))
                          )
        story-keys (filter avail? (map keyword most-popular))
        gen-fold-in-fn (fn [m lambda-u]
                         (let [V (m/matrix (map second m))
                               XX (->> (r/map #(m/outer-product % %) V)
                                       (r/fold m/add)
                                       (m/mul b)
                                       (m/matrix))]
                           (fn [profile ratings] (fold-in-with-ratings V XX story-id->index lambda-u profile ratings)))
                         )
        ]
    {:ours {:topics (m/matrix (map (into {} ours) story-keys))
            :fold-in (gen-fold-in-fn ours 10)}
     :ctr {:topics (m/matrix (map (into {} ctr) story-keys))
           :fold-in (gen-fold-in-fn ctr 0.01)}
     :pmf {:topics (m/matrix (map (into {} pmf) story-keys))
           :fold-in (gen-fold-in-fn pmf 0.01)}
     :stories (map name story-keys)
     :orig-topics  (m/matrix (map theta story-keys))
     }

    ))









(def latest-stories
  (let [b 0.01
        stories (data/get-latest-medium-stories @pooled-db)
        V (m/matrix (map :topics stories))
        new-latest-stories (filter #(.isAfter (:published_at %) (.minusMonths (DateTime.) 4)) stories)
        XX (->> (pmap #(m/outer-product % %) V)
                (r/fold m/add)
                (m/mul b)
                (m/matrix))
        story-id->index (into {} (map #(vector (:id %1) %2) stories (range)))
        ]
    {
     ; all sotires
     :stories stories



     ; latest stories
     :latest-stories new-latest-stories
     :orig-topics  (m/matrix (map :orig_topics new-latest-stories))
     :offsets (->> new-latest-stories
                   (map (fn [{:keys [topics orig_topics]}]
                          (m/sub topics orig_topics)
                          ))
                   (m/matrix)
                   )
     :fold-in (fn [profile likes dislikes] (fold-in-iteration V XX story-id->index 10 profile likes dislikes))

     }
    ))








