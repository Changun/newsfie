(ns lifestreams-db.hacker-news
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]
            [base64-clj.core :as base64]
            [net.cgrand.enlive-html :as html])
  (:require [monger.core :as mg]
            [monger.query :as mq]
            [monger.collection :as mc]
            [lifestreams-db.html-utils :refer [extract-snippet-content]]
            [lifestreams-db.topic-model :as topics]
            [clojure.core.matrix :as m])
  (:import (org.joda.time DateTime Interval Minutes)
           (clojure.lang IPersistentMap)
           (java.util.concurrent Executors TimeUnit)
           (org.httpkit.client TimeoutException)
           (java.io IOException))
  )

(def model (topics/load-model "model-hn.bin"))
(def knock-out-topics [26 45 86 59 75 99 51])
(def topic-titles (topics/topic-titles model))


(def conn (mg/connect))
(def db (mg/get-db conn "hacker-news"))

(defn query-stories [& {:keys [start end limit order] :or {order -1}}]
  (mq/with-collection db "story"
                      (mq/find (cond-> {}
                                       start
                                       (assoc-in [:created_at_i "$gte"] (/ (.getMillis start) 1000))
                                       end
                                       (assoc-in [:created_at_i "$lt"] (/ (.getMillis end) 1000))
                                       ))
                      ;; it is VERY IMPORTANT to use array maps with sort
                      (mq/sort (array-map :created_at_i order))
                      (mq/limit limit)))


(defmulti extract-text class)
(defmethod extract-text IPersistentMap
  [{:keys [children title text]}]
  (concat [title]
          (extract-snippet-content (html/html-snippet text))
          (mapcat extract-text children))
  )
(defmethod extract-text String [s]
  [s]
  )




(defn get-story [id]
  {:pre ([(re-matches #"[0-9]+" (str id))])}
  (let [{:keys [body error status]} @(http/get (str "http://hn.algolia.com/api/v1/items/" id))
        ]
    (cond
      (or (isa? (class error) TimeoutException)
          (isa? (class error) IOException))
      (do (Thread/sleep 1000)
          (recur id))
      (or error (not= status 200))
      (throw (or error (Exception. (str status body))))
      :default
      (parse-string body true)
      )
    )
  )

(def top-stories-atom (atom []))

(defn story-topics [story]
  (->> story
       (extract-text)
       (clojure.string/join "\n")
       (#(topics/infer-topic-dist model % 200))
       (m/matrix)

       ))

(defn frontpage-rank [{:keys [points created_at type url]}]
  (let [gravity* 1.8 timebase* 120 front-threshold* 1
        nourl-factor* 0.4 lightweight-factor* 0.17 gag-factor* 0.1
        base (- points 1)
        base (if (> base 0) (Math/pow base 0.8) base)
        age (.getMinutes (Minutes/minutesBetween (DateTime. created_at) (DateTime.)))]
    (* (/ base
          (Math/pow (/ (+ age timebase*) 60) gravity*))
       (cond (not (#{"story" "poll"} type)) 0.8
             (or (= url "") (= url nil)) nourl-factor*
             :default lightweight-factor*
             )))
  )


(defn refresh-top-stories []
  (let [{:keys [body error]}
        @(http/get "https://hacker-news.firebaseio.com/v0/topstories.json")]
    (if error
      (throw error)
      (doseq [[id index] (map vector (parse-string body) (range))]
        (try
          (let [story (-> (get-story id))
                story (assoc story :topics (story-topics story)
                                   :rank-score (frontpage-rank story))]
            (swap! top-stories-atom #(assoc % index story))
            )
          (catch Exception e (println (str "failed to refresh story: " id " " e)))
          )
        ))
    )
  )



(let [s (Executors/newSingleThreadScheduledExecutor)]
  (.scheduleAtFixedRate ^Executors s
                        #(try
                          (refresh-top-stories)
                          (catch Exception e
                            (.printStackTrace e)))
                        (long 0)
                        (long 30)
                        TimeUnit/MINUTES))








(comment
  (def docs (let [conn (mg/connect)
                  db (mg/get-db conn "hacker-news")]
              (for [doc (mc/find-maps db "story" {})]
                {:data   (clojure.string/join "\n" (extract-text doc))
                 :label  "trainning"
                 :name   (:title doc)
                 :source "hacker-news"
                 }
                )
              ))
  (def model (run-model (take 50000 docs) 100 2000))

  (defn update-stories [last-n-days]
    (let [earliest (.minusDays (DateTime.) last-n-days)
          ids (story-id-sequence 10)
          stories (map get-story ids)
          stories (take-while (fn [story] (.isAfter (DateTime. (:created_at story)) earliest)) stories)
          ]
      (printf "Start updating stories til %s\n" earliest)
      (doseq [[story id index] (map vector stories ids (range))]
        (println (:created_at story))
        (mc/save db "story" (assoc story :_id id))
        (Thread/sleep 400)
        )

      ))



  (defn story-id-sequence
    ([comments]
     (story-id-sequence comments (int (/ (.getMillis (DateTime.)) 1000)))
      )
    ([comments create-at]
     (let [{:keys [body error]}
           @(http/get "http://hn.algolia.com/api/v1/search_by_date"
                      {:query-params
                       {:hitsPerPage    1000
                        :numericFilters (clojure.string/join
                                          ","
                                          [(str "num_comments" ">=" comments)
                                           (str "created_at_i" "<=" create-at)])}})
           {:keys [hits]} (parse-string body true)
           ids (map :objectID hits)]
       (if-not (seq ids)
         (throw (Exception. error body))
         )
       (distinct (concat ids (lazy-seq (story-id-sequence comments (:created_at_i (last hits))))))
       )
      )
    )

  )



