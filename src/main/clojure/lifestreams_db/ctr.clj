(ns lifestreams-db.ctr
  (:require [clojure.core.matrix :as m]
            [clojure.core.matrix.linear :as ml]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [taoensso.timbre
             :refer (error)]
            [clojure.core.memoize :as memoize]
            [incanter.stats :as stats]
            [clojure.core.reducers :as r])
  (:use [incanter.core]
        [incanter.charts]
        [incanter.stats]
        )
  )


(defn solve-safe [A b]
  {:post [%]}
  (ml/solve (m/mul A 1e6) (m/mul b 1e6))
  )

;; based on the java implementation
;; https://github.com/terro/ADS-AR/blob/master/src/cn/edu/tsinghua/AR/ctr/CTREstimate.java
(defn- estimate-internal [num-users Theta Gamma doc->users user->docs a b lambda_u lambda_v min-iter & [V iteration old-likelihood]]
  (let [
        num-factors (m/column-count Theta)
        V (or V (m/add Theta (repeatedly
                               (m/row-count Theta)
                                #(stats/sample-normal num-factors :sd (sqrt lambda_v)))))
        iteration (or iteration 1)
        start (System/currentTimeMillis)
        num-docs (m/row-count V)
        a-minus-b (- a b)
        doc-id->v-outter-product
        (memoize (fn [doc-id]
                   (let [v (m/get-row V doc-id)]
                     (m/outer-product v v)
                     )
                   ))
        XX (->> (pmap doc-id->v-outter-product (range num-docs))
                (r/fold m/add)
                (m/mul b))
        _ (println "Finish init XX for U" (m/row-count XX))
        user-iteration
        (fn [uid] (let [gamma-u (m/get-row Gamma uid)
                        docs (user->docs uid)
                        A
                        (->> (map doc-id->v-outter-product docs)
                             (apply m/add)
                             (m/mul a-minus-b)
                             (m/add XX)
                             )
                        x (reduce (fn [x doc-id]
                                    (let [v (m/get-row V doc-id)]
                                      (m/add x (m/mul v a))
                                      )
                                    ) (m/zero-vector num-factors) docs)
                        x (m/add x (m/mul gamma-u lambda_u))
                        ;;B (m/clone A)
                        A (m/add A (m/mul (m/identity-matrix num-factors) lambda_u))
                        u (m/to-vector (solve-safe A x))
                        likelihood (* -0.5 (m/scalar (m/esum (m/pow (m/sub u gamma-u) 2))) lambda_u)]
                    [u likelihood]

                    ))
        user-results (pmap #(p :user-iteration (user-iteration %)) (range num-users))
        U (m/matrix (map first user-results))
        likelihood (reduce + 0 (map second user-results))
        user-id->u-outter-product
        (memoize/lru (fn [user-id]
                       (let [u (m/get-row U user-id)]
                         (m/outer-product  u u))
                       )
                     :lru/threshold 10000)
        XX
        (->> (pmap user-id->u-outter-product (range num-users))
             (r/fold m/add)
             (m/mul b))
        _ (println "Finish init XX for V")
        doc-iteration
        (fn [doc-id] (let [theta-v (m/get-row Theta doc-id)
                           users (doc->users doc-id)]
                       (if (seq users)
                         (let [A
                               (->> (map user-id->u-outter-product users)
                                    (apply m/add)
                                    (m/mul a-minus-b)
                                    (m/add XX)
                                    )
                               x (reduce (fn [x uid]
                                           (let [u (m/get-row U uid)]
                                             (m/add x (m/mul u a))
                                             )
                                           ) (m/zero-vector num-factors) users)

                               x (m/add x (m/mul theta-v lambda_v))
                               ;B (m/clone A)
                               A (m/add A (m/mul (m/identity-matrix num-factors) lambda_v))
                               v (m/to-vector (solve-safe A x))
                               likelihood (reduce (fn [likelihood uid]
                                                    (let [u (m/get-row U uid)]
                                                      (- likelihood (* a (Math/pow (- 1 (m/scalar (m/mmul u v))) 2) 0.5))
                                                      )
                                                    ) 0 users)
                               likelihood (- likelihood (* 0.5 (m/scalar (m/inner-product (m/sub v theta-v) (m/sub v theta-v))) lambda_v))
                               ]
                           [v likelihood]
                           )
                         [theta-v 0]
                         )
                       ))
        doc-results (pmap doc-iteration (range num-docs))
        V (m/matrix (map first doc-results))
        likelihood (reduce + likelihood (map second doc-results))
        converge (if old-likelihood
                   (/ (Math/abs (- old-likelihood likelihood)) (Math/abs old-likelihood))
                   Double/POSITIVE_INFINITY)
        elapsed (- (System/currentTimeMillis) start)
        ]
    (if (and old-likelihood (< (- likelihood old-likelihood) 0))
      (error "Likelihood is decreasing!")
      )
    (if (and (> iteration min-iter)
             (< converge 1E-3))
      [U V]
      (do
        (printf "iter=%04d, time=%06d, likelihood=%.5f, converge=%.10f\n", iteration, elapsed, likelihood, converge)
        (recur num-users Theta Gamma doc->users user->docs a b lambda_u lambda_v min-iter [V (inc iteration) likelihood]))
      )
    ))



(defn estimate [doc->theta user->gamma user-doc a b lambda_u lambda_v min-iter & [implementation ]]
  (let [implementation (or implementation :vectorz)]
    (m/set-current-implementation implementation)
    (println "Use matrix implementation " implementation)
    )

  (let [dims (count (seq (second (first doc->theta))))
        [user->index doc->index doc->users user->docs]
        (loop [[[user doc] & user-doc] user-doc  user->index {} doc->index {} doc->users {} user->docs {}]
          (if user
            (let [uid (or (user->index user) (count user->index))
                  did (or (doc->index doc) (count doc->index))]
              (recur user-doc
                     (assoc user->index user uid)
                     (assoc doc->index doc did)
                     ; doc to all users who like it
                     (assoc doc->users did (conj (doc->users did) uid))
                     ; user to all docs he/she likes
                     (assoc user->docs uid (conj (user->docs uid) did))
                     )
              )
            [user->index doc->index doc->users user->docs]
            )
          )
        _ (println "User->docs doc->users created.")
        _ (println "Doc count" (count doc->users)
                   "User count" (count user->docs)
                   "Avg. upvotes per user"
                   (float (/ (apply + (map (comp count second) user->docs))
                             (count user->docs))) )
        Gamma  (map (fn [[user _]]
                      (or (user->gamma user) (m/add (m/zero-vector dims) (/ 1 dims)))
                      )
                    (sort-by second user->index)
                    )
        Theta  (map (fn [[doc _]]
                      (doc->theta doc)
                      )
                    (sort-by second doc->index)
                    )
        [U V] (estimate-internal (count Gamma) (m/matrix Theta) (m/matrix Gamma) doc->users user->docs a b lambda_u lambda_v min-iter)
        user->u (into {} (map #(vector (first %) (m/get-row U (second %))) user->index))
        doc->v (into {} (map #(vector (first %) (m/get-row V (second %))) doc->index))
        ]
       [user->u doc->v]

    )
  )
