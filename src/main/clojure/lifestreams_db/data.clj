(ns lifestreams-db.data
  (:require [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :refer [pooled-db]]
            [clj-time.coerce :as time-coerce]
            [cheshire.core :as json]
            [clojure.string])
  (:import (org.joda.time.format DateTimeFormat)
           (clojure.lang IPersistentVector Sequential IPersistentSet IPersistentMap)
           (java.sql Array PreparedStatement Timestamp)
           (org.joda.time DateTime)
           (org.postgresql.util PGobject)))

(extend-protocol clojure.java.jdbc/ISQLParameter
  Sequential
  (set-parameter [v ^PreparedStatement stmt ^long i]
    (let [conn (.getConnection stmt)
          meta (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta i)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt i v)))))
(extend-protocol clojure.java.jdbc/ISQLParameter
  IPersistentSet
  (set-parameter [v ^PreparedStatement stmt ^long i]
    (let [conn (.getConnection stmt)
          meta (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta i)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt i v)))))

(extend-protocol clojure.java.jdbc/IResultSetReadColumn
  Array
  (result-set-read-column [val _ _]
    (into [] (.getArray val))))

(extend-protocol clojure.java.jdbc/ISQLValue
  IPersistentMap
  (sql-value [value]
    (doto (PGobject.)
      (.setType "json")
      (.setValue (json/generate-string value)))))

(extend-protocol clojure.java.jdbc/IResultSetReadColumn
  PGobject
  (result-set-read-column [pgobj metadata idx]
    (let [type (.getType pgobj)
          value (.getValue pgobj)]
      (case type
        "json" (json/parse-string value true)

        :else value)))
  Timestamp
  (result-set-read-column [col _ _]
    (time-coerce/from-sql-time col)))



; http://clojure.github.io/java.jdbc/#clojure.java.jdbc/ISQLValue
; This is going back out to the database
(extend-protocol clojure.java.jdbc/ISQLValue
  DateTime
  (sql-value [v]
    (time-coerce/to-sql-time v)))



(def twitter-datetime-pattern (DateTimeFormat/forPattern "EEE MMMM dd HH:mm:ss Z y"))



(defn save-tweet [db & raw-tweets]

  (->>
    (for [{:keys [text created_at id geo place entities in_reply_to_screen_name]} raw-tweets]

      (do

        {:text                    (if (seq text)
                                    (clojure.string/replace text #"\x00" "")
                                    "")
         :created_at              (.parseDateTime twitter-datetime-pattern created_at)
         ;TODO (:coordinates geo)
         :id                      id
         :place                   (:id place)
         :hashtags                (map :text (:hashtags entities))
         :user_mentions           (map :screen_name (:user_mentions entities))
         :urls                    (map :expanded_url (:urls entities))
         :in_reply_to_screen_name in_reply_to_screen_name
         })

      )
    (apply sql/insert! db "tweet")
    )
  )
(defn get-twitter-entity-id [db entity-name]
  (:id (first (sql/query db ["SELECT id from twitter_entity WHERE name=? LIMIT 1" entity-name]))))

(defn save-twitter-entity [db entity-name]
  (sql/with-db-transaction
    [db db]
    (let [entity-name (clojure.string/lower-case entity-name)]
      (or
        (get-twitter-entity-id db entity-name)
        (:id (first (sql/insert! db "twitter_entity" {:name entity-name})))))
    )
  )
(defn save-entity-tweets-slow [db entity-name tweets]
  (let [entity-id (save-twitter-entity db entity-name)]
    ; save-new-tweets
    (sql/with-db-transaction
      [db db]
      (let [existing-tweets? (->> (sql/query db ["SELECT id from tweet WHERE id = ANY(?)" (map :id tweets)])
                                  (map :id)
                                  (into #{})
                                  )
            new-tweets (->> tweets
                            (filter #(not (existing-tweets? (:id %))))
                            )]
        (if-let [new-tweets (seq new-tweets)]
          (apply save-tweet db new-tweets)
          )
        ))


    ; save new entity->tweet mappings
    (sql/with-db-transaction
      [db db]
      (let [existing-entity-tweets
            (->> (sql/query db ["SELECT tweet_id from twitter_entity_tweet WHERE entity_id =? AND tweet_id = ANY(?)" entity-id (map :id tweets)])
                 (map :tweet_id)
                 (into #{})
                 )
            new-entity-tweets
            (filter #(not (contains? existing-entity-tweets (:id %))) tweets)]
        (if-let [new-entity-tweets (seq new-entity-tweets)]
          (apply sql/insert! db "twitter_entity_tweet"
                 (for [{:keys [id]} new-entity-tweets]
                   {:tweet_id  id
                    :entity_id entity-id
                    }
                   )))
        )
      )

    )
  )


(defn save-entity-tweets [db entity-name tweets]
  (try
    (let [
          entity-id (save-twitter-entity db entity-name)
          ]
      ; save-new-tweets
      (if-let [new-tweets (seq tweets)]
        (apply save-tweet db new-tweets)
        )
      ; save new entity->tweet mappings
      (if-let [new-entity-tweets (seq tweets)]
        (apply sql/insert! db "twitter_entity_tweet"
               (for [{:keys [id]} new-entity-tweets]
                 {:tweet_id  id
                  :entity_id entity-id
                  }
                 )))

      ) (catch Exception e (save-entity-tweets-slow db entity-name tweets)))
  )

(defn save-twitter-follows [db follower followees]
  (if (seq followees)
    (let [follower (cond->>
                     follower
                     (not= (first follower) \@)
                           (str "@"))
          entity-id (save-twitter-entity db follower)
          followees (into #{} followees)]
      (sql/with-db-transaction
        [db db]
        (let [new-followees
              (->> (sql/query db ["SELECT followee from twitter_follow WHERE  follower_entity_id = ?  AND followee = ANY(?)" entity-id followees])
                   (map :followee)
                   (into #{})
                   (clojure.set/difference followees)
                   )

              ]
          (if-let [new-followees (seq new-followees)]
            (apply sql/insert! db "twitter_follow"
                   (for [followee new-followees]
                     {:follower_entity_id entity-id
                      :followee           followee}
                     )
                   )
            )
          ))
      ))

  )



(defn save-person [db person]
  (cond
    (and (:twitter_screen_name person) (not (= (first (:twitter_screen_name person)) \@)))
    (throw (Exception. (str (:twitter_screen_name person) "Twitter screen name must start with @")))
    (:id person)
    (do (if-not (sql/update! db "person" person ["id = ?" (:id person)])
          (throw (Exception. (str "Person id:" (:id person) " not found"))))
        person
        )
    :default
    (let [person (into {} (map (fn [[k v]] [k (if v (clojure.string/lower-case v))]) person))
          existing-people (sql/query db ["SELECT * from person
                                          WHERE  (medium_user_name IS NOT NULL AND lower(medium_user_name) = lower(?))"
                                         ;(:twitter_screen_name person)
                                         ;(:facebook_id person)
                                         (:medium_user_name person)
                                         ]
                                     )
          existing (first existing-people)
          person (merge (first existing-people) person)]

      (cond (= 1 (count existing-people))
            (if (not= existing person)
              (do (sql/update! db "person" person ["id = ?" (:id person)])
                  person)
              person)
            (= 0 (count existing-people))
            (first (sql/insert! db "person" person))
            :default
            (throw (Exception. (str "More than one users have these info:" person)))
            )
      )

    ))

(defn save-person-medium-like [db person stories]
  (let [
        person-id (or (:id person) (:id (save-person db person)))
        story-ids (into #{} (map :id stories))
        new-likes (->> (sql/query db ["SELECT story_id from medium_like WHERE  person_id = ?  AND story_id = ANY(?)" person-id story-ids])
                       (map :story_id)
                       (into #{})
                       (clojure.set/difference story-ids)
                       )
        ]
    (if-let [new-likes (seq new-likes)]
      (apply sql/insert! db "medium_like"
             (for [story-id new-likes]
               {:person_id person-id
                :story_id  story-id}
               )
             ))
    )
  )

(defn- story->content [story]
  (->> (get-in story [:content :bodyModel :paragraphs])
       (map :text)
       (clojure.string/join "\n")
       )
  )
(defn save-medium-stories [db & stories]
  (doseq [{:keys [id firstPublishedAt upvotes] :as story} stories]
    (let [story {:id           id
                 :published_at (DateTime. firstPublishedAt)
                 :upvote_count (:count upvotes)
                 :json         (doto (PGobject.)
                                 (.setType "json")
                                 (.setValue (json/generate-string story)))
                 :text         (story->content story)
                 }]
      (if (seq (sql/query db ["SELECT id FROM medium_story WHERE id=?" id]))
        (sql/update! db "medium_story" story ["id = ?" id])
        (sql/insert! db "medium_story" story)
        )
      )

    )

  )

(defn update-twitter-entity-topics [db name topics]
  (sql/update! db "twitter_entity" {:topics topics} ["lower(name) = lower(?)" name])
  )

(defn cache-twitter-profile [db entity-name profile]
  (if (zero? (first (sql/update! db
                                 "twitter_profile_cache"
                                 {:topics profile}
                                 ["lower(entity_name) = lower(?)" entity-name])))
    (sql/insert! db
                 "twitter_profile_cache"
                 {:entity_name entity-name
                  :topics      profile})
    )
  )

(defn medium-user->twitter-screen-name [db user]
  (->> (sql/query db ["select twitter_screen_name as tname FROM person WHERE lower(medium_user_name) = lower(?)" user])
       (first)
       (:tname))
  )
(defn get-twitter-entity-topics [db name]
  (->>
    (sql/query db ["select topics from twitter_entity WHERE lower(name) = lower(?)" name])
    (first)
    (:topics))
  )

(defn get-stories [db & {:keys [start end upvotes]}]

  (->> (apply vector (cond-> "SELECT id FROM medium_story WHERE true "
                             start
                             (str "AND published_at >= ?")
                             end
                             (str "AND published_at < ?")
                             upvotes
                             (str "AND upvote_count >= ?")
                             true
                             (str "ORDER BY published_at")
                             )
              (filter identity [start end upvotes])
              )
       (sql/query db)
       (map :id))

  )



(defn get-story-with-id [db id]

  (->>
    (sql/query db ["SELECT id, text, published_at, upvote_count FROM medium_story WHERE id = ?" id])
    (first))

  )
(defn get-story-detials-with-ids [db ids]

  (->>
    (sql/query db ["SELECT json FROM medium_story WHERE id = ANY(?)" (into #{} ids)])
    (map #(merge (:json %) %))
    )

  )
(defn get-story-details-with-id [db id]

  (->>
    (sql/query db ["SELECT json FROM medium_story WHERE id = ?" id])
    (first)
    (:json))

  )
(defn get-latest-story-details-with-id [db id]

  (->>
    (sql/query db ["SELECT json FROM latest_medium_story WHERE id = ?" id])
    (first)
    (:json))

  )


(defn get-agg-tweets-with-entity-name [db name]

  (->>
    (sql/query db ["SELECT string_agg(text, '\n') AS text
                    FROM twitter_entity INNER JOIN twitter_entity_tweet ON entity_id = twitter_entity.id
                                        INNER JOIN tweet ON tweet.id  = tweet_id
                    WHERE lower(name)=lower(?)" name])
    (first)
    (:text))

  )




(defn get-tweets-with-entity-name [db name]

  (->>
    (sql/query db ["SELECT tweet.*
                    FROM twitter_entity INNER JOIN twitter_entity_tweet ON entity_id = twitter_entity.id
                                        INNER JOIN tweet ON tweet.id  = tweet_id
                    WHERE lower(name)=lower(?)" name])

    )
  )

(defn get-tweets-existence-with-entity-name [db name]

  (->>
    (sql/query db ["SELECT 1
                    FROM twitter_entity INNER JOIN twitter_entity_tweet ON entity_id = twitter_entity.id
                    WHERE lower(name)=lower(?) LIMIT 1" name])
    (first)

    )
  )



(defn get-tweets-count-with-entity-name [db name]

  (->>
    (sql/query db ["SELECT count(*) as count
                    FROM twitter_entity INNER JOIN twitter_entity_tweet ON entity_id = twitter_entity.id
                    WHERE lower(name)=lower(?)" name])
    (first)
    (:count)

    )

  )
(defn get-twitter-friends-with-entity-name [db name]
  (->>
    (sql/query db ["SELECT followee FROM twitter_follow
                              INNER JOIN twitter_entity ON follower_entity_id = id WHERE lower(name)=lower(?)" name])
    (map :followee)
    )
  )

(defn get-twitter-friends-topics-with-entity-name [db name]
  (->>
    (sql/query db ["SELECT topics FROM twitter_follow
                              INNER JOIN twitter_entity ON follower_entity_id = id
                              WHERE lower(name)=lower(?) AND topics IS NOT NULL" name])
    (map :topics)
    )
  )


(defn get-hashtags-with-entity-name [db name & [distinct?]]
  (->>
    (sql/query db [(format
                     "SELECT %s '#'
                                   || lower(Unnest(hashtags)) AS hashtag
                   FROM   twitter_entity
                          INNER JOIN twitter_entity_tweet
                                  ON twitter_entity.id = entity_id
                          INNER JOIN tweet
                                  ON tweet_id = tweet.id
                   WHERE  lower(NAME) = lower(?)" (if distinct? "DISTINCT" ""))
                   name])
    (map :hashtag)
    )
  )


(defn get-hashtags-with-multiple-entity-name [db names & [distinct?]]
  (->>
    (sql/query db [(format
                     "SELECT %s '#'
                                   || lower(Unnest(hashtags)) AS hashtag
                   FROM   twitter_entity
                          INNER JOIN twitter_entity_tweet
                                  ON twitter_entity.id = entity_id
                          INNER JOIN tweet
                                  ON tweet_id = tweet.id
                   WHERE  lower(NAME) = ANY(?)" (if distinct? "DISTINCT" ""))
                   names])
    (map :hashtag)
    )
  )
(defn get-hashtags-topics-with-entity-name [db name & [distinct?]]
  (->>
    (sql/query db [(format
                     "SELECT topics FROM twitter_entity INNER JOIN
                      (SELECT %s '#'
                                       || Unnest(hashtags) AS hashtag
                       FROM   twitter_entity
                              INNER JOIN twitter_entity_tweet
                                      ON twitter_entity.id = entity_id
                              INNER JOIN tweet
                                      ON tweet_id = tweet.id
                       WHERE  lower(NAME) = lower(?)) a
                     ON lower(name)= lower(hashtag) WHERE topics IS NOT NULL
                     " (if distinct? "DISTINCT" "")) name])
    (map :topics)
    )
  )

(defn get-people [db]
  (->>
    (sql/query db ["SELECT * FROM person"])

    )
  )

(defn get-user-medium-library [db medium-user-name & {:keys [start end upvotes]}]

  (->>
    (apply vector (cond-> "SELECT story_id FROM medium_like
                                         INNER JOIN person ON person_id=id
                                         INNER JOIN medium_story ON medium_story.id=story_id
                                         WHERE  lower(medium_user_name) = lower(?) "
                          start
                          (str "AND published_at >= ?")
                          end
                          (str "AND published_at < ?")
                          upvotes
                          (str "AND upvote_count >= ?")
                          true
                          (str "ORDER BY published_at")
                          )
           (filter identity [medium-user-name start end upvotes])
           )
    (sql/query db)
    (map :story_id)
    )
  )

(defn get-latest-medium-stories [db]
  (sql/query db ["SELECT id, topics, published_at, upvote_count, orig_topics FROM latest_medium_story
                  "]
             ))

(defn get-latest-medium-stories-by-popularity [db]
  (sql/query db ["SELECT id FROM latest_medium_story
                  WHERE published_at >= ? ORDER BY upvote_count DESC" (.minusMonths (DateTime.) 4)]
             ))



(defn get-twitter-profile-cache [db entity-name]
  (->
    (sql/query db ["SELECT topics FROM twitter_profile_cache
                   WHERE entity_name = ?" entity-name]
               )
    (first)
    :topics
    ))



