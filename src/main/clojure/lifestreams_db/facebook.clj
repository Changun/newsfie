(ns lifestreams-db.facebook
  (:use [lifestreams-db.config])
  (:require [org.httpkit.client :as http])
  (:import (com.restfb DefaultFacebookClient Parameter)
           (com.restfb.types Post Page User)
           (com.restfb.scope ScopeBuilder UserDataPermissions FacebookPermissions)))



(def ^:private client-id (get-in @config [:facebook :consumer-key]))
(def ^:private client-secret (get-in @config [:facebook :consumer-secret]))
(def limit 100)
(def permissions
  (-> (ScopeBuilder. true)
      (.addPermission   UserDataPermissions/USER_LIKES)
      (.addPermission   UserDataPermissions/USER_POSTS)
      ))

(defn get-auth-uri [ callback-uri]
  (.getLoginDialogUrl (DefaultFacebookClient.)
                      client-id callback-uri permissions)
  )
(defn get-auth-uri-for-id [ callback-uri]
  (.getLoginDialogUrl (DefaultFacebookClient.)
                      "604520866242946" callback-uri (ScopeBuilder.))
  )


(defn get-access-token [code callback-uri]
  (.getAccessToken (.obtainUserAccessToken (DefaultFacebookClient.) client-id client-secret callback-uri code))
  )


(defn get-access-token-for-id [code callback-uri]
  (.getAccessToken (.obtainUserAccessToken (DefaultFacebookClient.) "604520866242946" "76a97f2ff7a5563f2e6b3325c2822aef" callback-uri code))
  )

(defn page->content [^Page page]
  (str (.getAbout page) " " (.getDescription page))
  )

(defn post->content [^Post post]
  (str (.getDescription ^Post post) " " (.getMessage ^Post post)))

(defn revoke-token [token]
  (.deleteObject (DefaultFacebookClient. token) "me/permissions" (make-array Parameter 0)))

(defn get-user-id [token]
  (.getF ^User (.fetchObject ^DefaultFacebookClient (DefaultFacebookClient. token) "me" User (make-array Parameter 0)))
  )
(defn get-posts [access-token & [who]]
  (let [who (or who "me")]
    (-> (DefaultFacebookClient. access-token)
        (.fetchConnection  (str who "/posts") Post (doto (make-array Parameter 2)
                                                     (aset 0 (Parameter/with "fields" "description,message"))
                                                     (aset 1 (Parameter/with "limit" (str limit)))
                                                     ))
        (.iterator )
        (iterator-seq)
        (->>
          (mapcat identity)
          )
        ))
  )

(defn get-liked-pages [access-token]
  (-> (DefaultFacebookClient. access-token)
      (.fetchConnection  "me/likes" Page (doto (make-array Parameter 2)
                                           (aset 0 (Parameter/with "fields" "id,description,about"))
                                           (aset 1 (Parameter/with "limit" (str limit)))))
      (.iterator )
      (iterator-seq)
      (->>
        (mapcat identity)
        )
      )
  )

