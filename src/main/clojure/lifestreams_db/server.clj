(ns lifestreams-db.server
  (:require [clojure.core.matrix :as m]
            [lifestreams-db.topic-model :as topics]
            [lifestreams-db.medium :as medium]
            [lifestreams-db.meetup :as meetup]
            [lifestreams-db.gmail :as gmail]
            [lifestreams-db.youtube :as youtube]
            [lifestreams-db.slack :as slack]
            [lifestreams-db.google-api :as gapi]
            [lifestreams-db.twitter :as twitter]
            [lifestreams-db.facebook :as fb]
            [ring.util.codec :refer [form-decode]]
            [lifestreams-db.postgres :refer [pooled-db]]
            [taoensso.timbre :as timbre
             :refer (error info)]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]

            [lifestreams-db.data :as data]
            [cheshire.core :as json]
            [clojure.core.async :as async]
            [clj-time.core :as t]
            [monger.core :as mg]
            [monger.collection :as mc])
  (:use [org.httpkit.server]
        [compojure.route :only [files not-found]]
        [compojure.handler :only [site api]]                ; form, query params decode; cookie; session, etc
        [compojure.core :only [defroutes GET POST DELETE ANY context]]
        [cheshire.core]
        [cheshire.generate :only [add-encoder encode-seq]]
        [lifestreams-db.user-state]
        [lifestreams-db.config]
        )
  (:use [ring.middleware
         session
         flash
         ])
  (:import (mikera.vectorz Vector)
           (com.restfb.types Post)))


(add-encoder Vector encode-seq)
(m/set-current-implementation :vectorz)

(defonce model (topics/load-model "model-2015-09-17T03:24:53.366Z-200.bin"))
(def pref->titles (let [titles (topics/topic-titles model)]
                    (fn [pref]
                      (sort-by second (map vector titles pref)))
                    ))
(defn email-instances [token]
  (letfn [(email->weight [t]
                         (* (if (some gmail/sent? (:messages t)) 10 1)
                            ; (if (some gmail/important? (:messages t)) 2 1)
                            ))]
    (let [emails
          (->>
            (gmail/latest-threads-async token :q "in:sent from:(-{noreply notifications bank}) subject:(-Statement) -password")
            (filter #(> (count (:content %)) 500))
            (take 500))
          ]
      (->>
        emails
        (pmap (fn [%]
                {:weight  (email->weight %)
                 :sources "Mail"
                 :content (-> (:content %)
                              (clojure.string/replace  #"<[^>]+>" "")
                              (clojure.string/replace  #"(password)|(login)|(account)" ""))
                          }))
        )
      )

    )

  )

(defn twitter-instances [token & [screen-name content]]
  (letfn [(twitter->weight [{:keys [sources]}]
                           (if (= :mine (-> sources first :name))
                             (/ (count content) 20)
                             1)
                           )]
    (->> (twitter/instances token screen-name)
         (pmap (fn [%] {:weight  (twitter->weight %)
                        :sources "Twitter"
                        :content (:content %)}))
         (filter #(> (.length (:content %)) 400))
         ))
  )

(defn facebook-instances [token ]

  (let [id->posts (fn [id]
                    (->>(fb/get-posts token id)
                        (take 200)
                        (pmap fb/post->content)
                        ))

        page-posts (->>
                     (fb/get-liked-pages token)
                     (map #(future (conj  (id->posts (.getId %)) (fb/page->content %))))
                     (take 1000)
                     (doall)

                     )
        user-post (future (->>(fb/get-posts token)
                              (take 1000)
                              (pmap fb/post->content)
                              ))
        ]

    (conj
      (pmap (fn [post]
              {:weight 5
               :sources "Twitter"
               :content (clojure.string/join " " @post)}
              ) page-posts)
      {:weight (count @user-post)
       :sources "Twitter"
       :content (str (clojure.string/join " " @user-post) )})

    )
  )

(defn slack-instances [token ]
  (letfn [(instance->weight [{:keys [content]}]
                            100
                            )]
    (->> (slack/get-instances token)
         (pmap (fn [%] {:weight  (instance->weight %)
                        :sources "Twitter"
                        :content (:content %)}))
         (filter #(> (.length (:content %)) 400))
         ))
  )

(defn youtube-instances [token]
  (->>
    (youtube/get-instances token)
    (filter #(> (count (:content %)) 200))
    (pmap (fn [%] {:weight  2
                   :sources "Twitter"
                   :content (:content %)}))
    )
  )





(defn my-profile [token]
  (gmail/user-profile token))




(def instances->pref
  (memoize
    (fn [instances]
      (let [l1-normalize #(m/div % (m/esum %))
            topic-matrix (pmap (fn [{:keys [content sources]}]
                                 (topics/infer-topic-dist model content sources)) instances)]
        (-> (m/matrix (map :weight instances))
            (m/mmul (m/matrix topic-matrix))
            (l1-normalize)

            )
        )))
  )
(defonce twitter-name->pref
  (memoize (fn [screen-name]
             (->> (twitter-instances nil (twitter/normalize-screen-name screen-name))
                  (instances->pref))
             ))
  )

(future
  (doseq [tname ["BarackObama"
                 "cornell_tech"
                 "BilldeBlasio"
                 "BillGates"
                 "elonmusk"
                 "KingJames"
                 "katyperry"
                 "oprah"
                 "ActuallyNPH"
                 "timarmstrongaol"
                 "maymann"
                 "nytdavidbrooks"
                 "zephoria"
                 "hmason"
                 "amcafee"

                 ]

          ]
    (twitter-name->pref tname)
    ))

(def dickens-topics
  (->> (json/parse-string (slurp "dickens.json") true)
       (map :content)
       (filter identity)
       (map #(apply topics/infer-topic-dist medium/model % 100 medium/removed-topics))
       (apply m/add)
       ))


(defn topic-title-by-dist [topics titles]
  (reverse (sort-by first (map vector topics titles)))
  )

(defn handler-base [{:keys [state body] :as req} f]
  (with-channel req channel
                (profile :info :request
                         (let [{:keys [twitter dickens] :as params}
                               (if body (parse-string (slurp body) true)
                                 )
                               pref (cond
                                      twitter
                                      (twitter-name->pref twitter)
                                      dickens
                                      dickens-topics
                                      :else
                                      (if-let [profiles (:profiles state)]
                                        (->> profiles
                                             (map second)
                                             (map #(identity @%))
                                             (filter #(and (m/vec? %)))
                                             (apply m/add)
                                             )
                                        (send! channel {:status 401})
                                        )

                                      )
                               response (apply f req pref (apply concat params))
                               ]
                           (if twitter
                             (info "Use preference vector for twitter user " twitter)
                             (info "Compute preference vector for the user")

                             )

                           (send! channel response)
                           ))
                ))


(def ignores-stories #{"3e28efbc3bb7", "895e2cb0cad4", "cc0233e15c8d"})
(defn handler-medium [req pref & {:keys [likes dislikes ignores count]}]
  (let [story-response (fn [{:keys [topics id score]}]
                         (let [story (data/get-latest-story-details-with-id @pooled-db id)]
                           (assoc (select-keys story [:title :virtuals :canonicalUrl])
                             ; :sources (take 10 (top-sources topics source-prefs source-freqs))
                             :score score
                             ; :topics (take 5 (topic-title-by-dist topics medium/topic-titles))
                             :upvotes (get-in story [:upvotes :count])
                             :id id
                             )
                           ))
        ignores (into ignores-stories ignores)
        count (or count 20)
        orig-pref (m/div pref (m/esum pref))
        pref (p :fold-in
                (cond-> orig-pref
                        (or (seq likes) (seq dislikes))
                        ((:fold-in medium/latest-stories) likes dislikes)
                        ))
        ]
    (let [{:keys [orig-topics offsets topics latest-stories]} (->> medium/latest-stories)
          score (p :compute-score (m/inner-product (m/add orig-topics (m/mul offsets 0.6)) pref))
          ;gravity (m/pow (map #(/ (t/in-days (t/interval (:published_at %) (t/now))) 7) stories) 0.5)
          ;score (m/div score gravity)
          stories (map #(assoc %1 :score %2) latest-stories score)]
      (->>
        {:stories
                       (p :generate-stories
                          (->> stories
                               (sort-by :score)
                               (reverse)
                               (filter (complement (comp ignores :id)))
                               (take count)
                               (pmap story-response)
                               (doall)
                               )
                          )
         :orig-pref    orig-pref
         :pref         pref
         :topic-titles medium/topic-titles
         ;:stories-by-popularity (->> (data/get-latest-medium-stories-by-popularity @pooled-db)(take 700))

         ;
         }
        generate-string
        (assoc {:status 200 :headers {"content-type" "application/json"}} :body)
        )
      )
    ))


(defn handler-meetup [req pref & {:keys [likes dislikes ignores count] :or {count 20}}]
  (let [ignores (into #{} ignores)
        orig-pref (m/div pref (m/esum pref))
        pref orig-pref
        ]
    (let [meetups meetup/meetups
          score (m/inner-product (map :topics meetups) pref)
          meetups (map #(assoc %1 :score %2 :topics (take 10 (reverse (sort-by first (map vector (:topics %1) meetup/topic-titles)
                                                                               )))) meetups score)]
      (->>
        {:meetups
                       (->> meetups
                            (sort-by :score)
                            (reverse)
                            (filter (complement (comp ignores :id)))
                            (take count)
                            )
         :orig-pref orig-pref
         :pref pref
         :topic-titles meetup/topic-titles
         :sorted (sort-by first (map vector pref meetup/topic-titles))}
        generate-string
        (assoc {:status 200 :headers {"content-type" "application/json"}} :body)
        )
      )
    ))


(defn handler-user-study [models pref  id->content & {:keys [ratings ignores count methods] :or {count 10}}]

  (let [{:keys [stories ours ctr pmf orig-topics]} models
        ignores (into #{} ignores)
        methods? (into #{} methods)
        feedback? (seq ratings)
        pref->stories (fn [model model-name orig-pref]
                        (let [{:keys [topics fold-in]} model
                              pref (cond-> orig-pref
                                           feedback?
                                           (fold-in ratings)
                                           )]
                          (->>(m/inner-product topics pref)
                              (map #(vector %1 %2) stories)
                              (sort-by second)
                              (reverse)
                              (map first)
                              (map #(vector % model-name) )
                              )
                          )
                        )
        random-stories (map #(vector % "random") (shuffle stories))
        ours-stories (pref->stories ours "ours" (m/div pref (m/esum pref)))
        ctr-stories (if feedback? (pref->stories ctr "ctr" (m/zero-vector 200)))
        pmf-stories (if feedback? (pref->stories pmf "pmf" (m/zero-vector 200)))
        most-popular (map #(vector % "most-popular") stories)
        story->rec-by (->> [random-stories
                            ours-stories
                            ctr-stories
                            pmf-stories
                            most-popular]
                           (mapcat (fn [stories]
                                     (take count (filter (comp (complement ignores) first) stories))))
                           (filter (comp methods? second))
                           (map (fn [[story method]]
                                  {story [method]}
                                  ))
                           (apply merge-with concat)

                           )
        ]
    (shuffle
      (for [[story bys] story->rec-by]
        (assoc (id->content story) :recommended-by bys :id story)
        ))
    ))
(defn study-handler-base [models id->content {:keys [state body] :as req} ]
  (with-channel req channel
                (let [params
                      (if body (parse-string (slurp body) true))

                      pref (if-let [profiles (:profiles state)]
                             (->> profiles
                                  (map second)
                                  (map #(identity @%))
                                  (filter m/vec?)
                                  (apply m/add)
                                  )
                             (send! channel {:status 401})
                             )
                      response (apply handler-user-study models  pref id->content (apply concat params))
                      ]

                  (send! channel {:status 200
                                  :headers {"content-type" "application/json"}
                                  :body (generate-string response)})
                  )
                ))

(defn handler-status
  "Return the data providers the user has connected to"
  [{:keys [state] :as req}]
  (with-channel req channel
                (->>
                  {:connected-providers (keys (:profiles state))}
                  (generate-string)
                  (assoc {:status 200 :headers {"content-type" "application/json"}} :body)
                  (send! channel)
                  )
                )

  )
(defn medium-story->response [id]
  (let [{:keys [virtuals canonicalUrl content title]}
        (data/get-story-details-with-id @pooled-db id)]
    {:id id
     :title title
     :url canonicalUrl
     :img (if (get-in virtuals [:previewImage :imageId])
            (str "https://cdn-images-1.medium.com/max/400/" (get-in virtuals [:previewImage :imageId])))
     :content
     (reduce (fn [content {:keys [text]}]
               (if (< (count content) 2500)
                 (str content "<p>" text "</p>")
                 (reduced content)
                 )
               )
             ""
             (take 14 (rest (get-in content [:bodyModel :paragraphs]))) )}
    ))
(defn meetup-group->response [id]
  (let [{:keys [description group_photo link name]}
        (meetup/groups (name id))]
    {:id id
     :title name
     :url link
     :img (let [{:keys [thumb_link highres_link photo_link]} group_photo]
            (or highres_link photo_link thumb_link))
     :content
     (cond-> description
             (> (count description) 2500)
             (.substring 0 2500))}
    ))

(def conn (mg/connect))
(def db (mg/get-db conn "twitter"))

(defn print-log [log]
  ; print results so fat
  (println "##################################")
  (println "User ID:" (:user-id log) (:info log))
  (doseq [[session data] (:data log)]
    (doseq [[batch data] (group-by :batchId data)]
      (let [rated (filter :rating data)
            method->rating (reduce (fn [ret {:keys [rating recommended-by]}]
                                     (reduce (fn [ret by]
                                               (assoc ret by (conj (ret by) rating))
                                               )
                                             ret
                                             recommended-by
                                             ))
                                   {}
                                   rated)]

        (printf
          "%s Batch:%s (%s/%s) %s\n"
          session  batch  (count rated) (count data)
          (sort-by second (map #(vector (first %) (float (/ (apply + (second %)) (count (second %))))) method->rating)))
        ))

    )
  (println "##################################")
  )




(def _id->user-id (atom {}))


(defn handler-logging [{:keys [body state session] :as req}]
  (with-channel req channel
                (let [state-id (str (:_state-id  session))
                      profiles (->> (:profiles state)
                                    (map (fn [[key fu]] [key (if (m/vec? @fu) (seq @fu))]))
                                    (into {} ))
                      body (parse-string (slurp body) true)
                      log (-> body
                              (assoc :profiles profiles)
                              (assoc :user-info (:info state))
                              (assoc :time (str (t/now)))
                              (assoc :state-id state-id)

                              )
                      _id (select-keys log [:state-id :info])
                      log (assoc log :_id _id)
                      user-id (or (@_id->user-id _id)
                                (do (swap! _id->user-id (fn [m] (assoc m _id (inc (apply max -1 (map second m))))))
                                    (@_id->user-id _id))

                                )
                      log (assoc log :user-id user-id)
                      ]


                  (if (and (:state-id log) (:info log))
                    (try (mc/save db "logs" log)
                         (send! channel {:status 200
                                         :headers {"content-type" "application/json"}
                                         :body (json/generate-string {:status "ok"
                                                                      :user-id user-id})})
                         (catch Exception e (error e)
                                            (send! channel {:status 500})))
                    (send! channel {:status 401})

                    )
                   (try (print-log log)
                        (catch Exception e (error e)))
                  )
                )

  )
(defn api-handler [req]

  (condp = (-> req :params :api-name)
    ;  "hn"
    ;(handler-base req news/model news/knock-out-topics handler-hn)
    "medium"
    (handler-base req handler-medium)
    "meetup"
    (handler-base req handler-meetup)
    "medium-study"
    (study-handler-base medium/cf-models
                        medium-story->response
                        req)
    "meetup-study"
    (study-handler-base meetup/cf-models
                        meetup-group->response
                        req)
    "status"
    (handler-status req)
    "logs"
    (handler-logging req)
    ; "debug"
    ; (handler-debug req)
    ))

(defn callback-uri [provider-name]
  (str (get-in @config [:server :root-url]) "auth/" provider-name "/callback")
  )

(defn auth-handler [{:keys [state params headers] :as req}]
  (println (dissoc req :state))
  (let [referer (get headers "referer")
        provider-name (keyword (:provider-name params))
        callback (callback-uri (name provider-name))]
    (println callback)
    (merge
      {:status 302
       :flash  {:referer referer}}
      (condp = provider-name
        :twitter
        (let [req-token (twitter/get-request-token callback)]
          {:headers {"Location" (twitter/get-auth-uri req-token)}
           :state   (assoc state :twitter-request-token req-token)}
          )
        :gmail
        {:headers {"Location" (gmail/get-auth-uri callback)}}
        :youtube
        {:headers {"Location" (youtube/get-auth-uri callback)}}
        :slack
        {:headers {"Location" (slack/get-auth-uri callback)}}
        :facebook
        {:headers {"Location" (fb/get-auth-uri callback)}}




        ))

    )
  )

(defn deauth-handler [{:keys [state params headers] :as req}]

  (let [referer (get headers "referer")
        provider-name (keyword (:provider-name params))]
    {:status  302
     :state   (assoc state :profiles (dissoc (:profiles state) provider-name))
     :headers {"Location" referer}
     }
    )
  )

(defn callback-handler [{:keys [state params flash] :as req}]
  (let [provider-name (keyword (:provider-name params))
        redirect (or (:referer flash) (get-in @config [:server :default-redirect-url]))]

    (merge
      {:headers {"Location" redirect}
       :status  302
       }
      (condp = provider-name
        :twitter
        (let [access-token (twitter/get-access-token
                             (:twitter-request-token state)
                             (:oauth_verifier params)
                             )]
          {:state (-> state
                      (assoc-in [:info :twitter] access-token)
                      (assoc-in [:profiles provider-name]
                                (future
                                  (try
                                    (let [profile (->> (twitter-instances access-token)
                                                       (instances->pref)
                                                       )]
                                      (println "Finished fetch twitter instances")
                                      profile
                                      )
                                    (catch Exception _ nil))

                                  )
                                )
                      (dissoc :twitter-request-token))}
          )
        :youtube
        (let [token (youtube/get-access-token
                      (:code params)
                      (callback-uri (name provider-name)))]
          {:state (-> state
                      (assoc-in [:profiles provider-name]
                                (future
                                  (try
                                    (let [profile (->> (youtube-instances token)
                                                       (instances->pref)

                                                       )]
                                      (println "Finished fetch youtube instances")
                                      (gapi/revoke-token token)
                                      profile
                                      )
                                    (catch Exception _ nil))

                                  )
                                )

                      )}
          )
        :gmail
        (let [token (gmail/get-access-token
                      (:code params)
                      (callback-uri (name provider-name)))]
          {:state (-> state
                      (assoc-in [:info :gmail] (my-profile token))
                      (assoc-in [:profiles provider-name]
                                (future
                                  (try
                                    (let [profile (->> (email-instances token)
                                                       (instances->pref)
                                                       )]
                                      (println "Finished fetch email instances")
                                      (gapi/revoke-token token)
                                      profile
                                      )
                                    (catch Exception _ nil))

                                  ))

                      )}
          )

        :slack
        (let [token (slack/get-access-token
                      (:code params)
                      (callback-uri (name provider-name)))]
          {:state (-> state
                      (assoc-in [:profiles provider-name]
                                (future
                                  (try
                                    (let [profile (->> (slack-instances token)
                                                       (instances->pref)
                                                       )]
                                      (println "Finished fetch slack instances")
                                      (println "Unfortunately, Slack does not allow access token revoke at the moment")
                                      profile
                                      )
                                    (catch Exception _ nil))

                                  )
                                )

                      )}
          )
        :facebook
        (let [token (fb/get-access-token
                      (:code params)
                      (callback-uri (name provider-name)))]
          {:state (-> state
                      (assoc-in [:profiles provider-name]
                                (future
                                  (try
                                    (let [profile (->> (facebook-instances token)
                                                       (instances->pref)
                                                       )]

                                      (println "Finished fetch facebook instances")
                                      (fb/revoke-token token)
                                      profile
                                      )
                                    (catch Exception e (error e)))

                                  )
                                )

                      )}
          )


        ))

    )
  )

(defroutes all-routes
           ;(GET "/" [] show-landing-page)
           (GET "/auth/:provider-name" [] auth-handler)     ;; websocket
           (GET "/deauth/:provider-name" [] deauth-handler) ;; websocket
           (GET "/auth/:provider-name/callback" [] callback-handler) ;; asynchronous(long polling)
           (ANY "/api/:api-name" [] api-handler)            ;; asynchronous(long polling)

           (not-found "<p>Page not found.</p>"))            ;; all other, return 404


(def stop-server (run-server (-> (api #'all-routes)
                                 (wrap-user-state)
                                 (wrap-flash)
                                 (wrap-session)) {:port 8100}))




