(ns lifestreams-db.youtube
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]
            [base64-clj.core :as base64]
            [net.cgrand.enlive-html :as html]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.async :as async]
            [lifestreams-db.google-api :as gapi]
            [ring.util.codec :refer [url-encode form-encode]]
            [clojure.xml :as xml])
  (:use [aprint.core]
        )
  (:import (org.httpkit.client TimeoutException)
           (java.io IOException)
           (org.apache.commons.io IOUtils)))

(def ^:private youtube-api-uri "https://gdata.youtube.com/feeds/api/users/default")
(def ^:private scope "https://gdata.youtube.com")

(defn- youtube-options
  "merge base options with the given options"
  [access-token options]
  (-> options
      (assoc :timeout 2000)
      (assoc-in [:headers "Authorization"] (str "Bearer " access-token))
      (assoc-in [:headers "Accept-Encoding"] " gzip")
      (assoc-in [:headers "User-Agent"] " my program (gzip)")
      )
  )

(defn- youtube-endpoint [path options access-token]
  (let [{:keys [error status] :as res}
        @(http/get (str youtube-api-uri path)
                   (youtube-options access-token options)
                   )

        ]
    (if (or (= status 429)
            (isa? (class error) TimeoutException)
            (isa? (class error) IOException)
            )
      (recur path options access-token)
      res))
  )


(defn- video->instance [{:keys [content]}]
  (letfn [(attr [content key] (:content (first (filter #(= (:tag %) key) content))))]
    (let [media-group (attr content :media:group)]
      {:content (clojure.string/join "\n" (concat (attr content :title)
                                                  (attr media-group :media:description)
                                                  (attr media-group :media:category)
                                                  (attr media-group :media:keywords)))
       :sources [{:type :youtube
                  :name :watch-history}]})

    )
  )
(defn- history-sequence [token & [start-index]]
  (let [start-index (or start-index 1)
        history (->> (youtube-endpoint "/watch_history"
                                       {:query-params
                                        {"v"           "2"
                                         "start-index" (str start-index)
                                         "max-results" "50"
                                         }
                                        } token)
                     :body
                     (IOUtils/toInputStream)
                     (xml/parse)
                     :content
                     )
        entires (filter #(= (:tag %) :entry) history)
        ]

    (if-let [entries (seq entires)]
      (concat entries (lazy-seq (history-sequence token (+ start-index (count entires)))))
      )
    )
  )
;;;;;;;;;;;;;;;;;;;;;;; Public functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn get-auth-uri [callback-uri]
  (gapi/get-auth-uri scope callback-uri)
  )
(defn get-access-token [code callback-uri]
  (gapi/get-access-token scope code callback-uri)
  )

(defn get-instances [token]
  (->> (history-sequence token)
       (pmap video->instance)
       (filter #(> (.length (:content %)) 500))
       )
  )