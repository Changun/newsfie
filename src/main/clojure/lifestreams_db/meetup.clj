(ns lifestreams-db.meetup
  (:require [cheshire.core :as json]
            [org.httpkit.client :as http]
            [lifestreams-db.topic-model :as topics]
            [taoensso.timbre
             :refer (error info)]
            [clojure.core.matrix :as m]
            [clojure.core.reducers :as r]
            [clojure.core.matrix.linear :as ml]
            [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :as postgres]
            [net.cgrand.enlive-html :as html]
            [lifestreams-db.medium :as medium])
  (:use [slingshot.slingshot])
  (:import (java.util.concurrent TimeoutException)
           (java.io IOException)))

(def model medium/model)
(def topic-titles medium/topic-titles)

(def meetup-url "http://www.meetup.com/")
(defn meetup-endpoint [path options]
  (let [{:keys [error status body] :as res}
        @(http/get (str meetup-url path)
                   (assoc options :headers (merge
                                             {"accept"        "application/json"
                                              "content-type"  "application/json"
                                              "x-xsrf-token"  "1"
                                              "x-obvious-cid" "web"}
                                             (:headers options)
                                             )))

        ]
    (cond
      (or (isa? (class error) TimeoutException)
          (isa? (class error) IOException)
          )
      (do (Thread/sleep 1000)
          (recur path options))
      (or error (not= status 200))
      (throw+ res)
      :default
      res
      )
    )
  )

(def meetups (map second (json/parse-stream
                           (clojure.java.io/reader "meetup-groups-topics.json")
                           true)))



(defn remove-html-url [text]
  (-> (str "<body>" text "</body>")
      (html/html-snippet )
      (html/texts)
      (->> (clojure.string/join " "))
      (clojure.string/replace
        #"(https?|ftp)://(-\.)?([^\s/?\.#-]+\.?)+(/[^\s]*)?"
        ""                      )
      (clojure.string/replace  #"<[^>]+>" "")
      (clojure.string/replace  #"\d\d+" "")
      )

  )

(defn group->content [{:keys [topics description link]}]
  (let [topics (map :name topics)
        description (if description
                      (remove-html-url description)
                       "")]
    (str (clojure.string/join "\n" topics) " " description)
    )
  )
(defn user->content [{:keys [topics bio]}]
  (let [topics (map :name topics)
        description (if bio (remove-html-url bio) "")]
    (str (clojure.string/join "\n" topics)  " " description)
    )
  )



(comment (defn content->topic [content]
           (topics/infer-topic-dist model content 100
                                    135 136)))

(defn content->topic [content]
  (apply topics/infer-topic-dist medium/model content 100 [8 33 54 62 64 115 118 119 179]))

(comment
  (apply concat

         (comment
           (->>
             "/data/group/new-group2group"
             (clojure.java.io/file)
             (file-seq)
             (filter #(re-matches #"group2group\d+\.json" (.getName %)))
             (map clojure.java.io/reader)
             (pmap #(json/parse-stream % true))
             ))
         ))
(def groups
  (->>
    (-> (clojure.java.io/reader "/data/group/group2group/group_group_final.json")
        (json/parse-stream  true)
        )
    (map (fn [[k v]] (vector (name k) v)))
      (distinct)

      (into {}))

  )

(defn run-model []
  (let [docs (->>
               groups
               (map second)
               (filter :description)
               (pmap (fn [group]
                       {:data   (group->content group)
                        :label  "trainning"
                        :name   (:link group)
                        :source (:link group)}
                       ))


               )]
    (topics/run-model docs 200 2000 0.02)
    ))

(def cf-models
  (let [b 0.01
        {:keys [ours pmf ctr theta user-group]}
        (with-open [r (clojure.java.io/reader "meetup_ousr_pmf_ctr_theta.json")]
          (json/parse-stream r true)
          )
        most-popular (->> user-group
                          (map second)
                          (frequencies)
                          (sort-by second)
                          (reverse)
                          (map first)
                          (filter #(#{"New York", "Brooklyn", "Jersey City", "Astoria", "Bronx", "Flushing", "Forest Hills", "New Brunswick", "Staten Island"}
                                    (:city (groups %))))
                          )
        ours (sort-by first ours)
        pmf (sort-by first pmf)
        ctr (sort-by first ctr)
        story-keys (map keyword most-popular)
        story-id->index (into {} (map vector (map (comp name first) ours) (range)))
        gen-fold-in-fn (fn [m lambda-u]
                         (let [V (m/matrix (map second m))
                               XX (->> (pmap #(m/outer-product % %) V)
                                       (r/fold m/add)
                                       (m/mul b)
                                       (m/matrix))]
                           (fn [profile ratings] (medium/fold-in-with-ratings V XX story-id->index lambda-u profile ratings)))
                         )
        ]
    {:ours {:topics (m/matrix (map (into {} ours) story-keys))
            :fold-in (gen-fold-in-fn ours 10)}
     :ctr {:topics (m/matrix (map (into {} ctr) story-keys))
           :fold-in (gen-fold-in-fn ctr 0.01)}
     :pmf {:topics (m/matrix (map (into {} pmf) story-keys))
           :fold-in (gen-fold-in-fn pmf 0.01)}
     :stories (map name story-keys)
     :orig-topics  (m/matrix (map theta story-keys))
     }

    ))








