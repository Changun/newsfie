(ns new-model.model)


(defprotocol TopicModel
  (update-local-params [model old-word-params  new-word-params local-params])
  (update-global-params [model old-word-params  new-word-params global-params])
  (init-global-params [model])
  (init-local-params [model words])
  (init-word-params [model word])
  (sample [model word word-params local-params global-params])
  )


