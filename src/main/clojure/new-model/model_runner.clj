(ns new-model.lda.model-runner
  (:require [lifestreams-db.data :as data])
  (:import (org.tartarus.snowball.ext EnglishStemmer)
           (new_model.lda LDA)))


(let [stem (fn [word]
             (let [stemmer (EnglishStemmer.)]
               (.setCurrent stemmer word)
               (.stem stemmer)
               (.getCurrent stemmer))
             )
      docs (->> (data/get-stories @lifestreams-db.postgres/pooled-db)
                (map (partial data/get-story-with-id @lifestreams-db.postgres/pooled-db))
                (map :text)
                (filter #(> (count %) 1000))
                (map #(->> (re-seq #"\p{L}\p{L}\p{L}+" %)
                           (map clojure.string/lower-case)
                           (map stem)
                           (map keyword)
                           ))
                (take 2000)
                )
      doc-freq (->> docs
                    (map distinct)
                    (flatten)
                    (frequencies)
                    )
      stop-words (->> doc-freq
                      (filter (fn [[_ freq]]
                                (> freq (* 0.1 (count docs)))
                                ))
                      (map first)
                      (into #{})
                      )
      _ (println stop-words)
      docs (for [words docs]
             (filter (complement stop-words) words))
      all-words (flatten docs)
      words (into #{} all-words)
      W (count words)
      T 200
      lda (LDA. )
      word->index (into {} (map #(vector %1 %2) words (range)))
      ; randomly initialize word tuples
      doc-word-tuples
      (mapv (fn [doc] (mapv #(vector (word->index %)) doc)) docs)
      global-params
      (reduce (fn [global-params tuple]
                (update-global-params! [(first tuple) nil nil] tuple global-params)
                )
              (init-global-params W T)
              (mapcat identity doc-word-tuples)
              )
      num-iteration 20

      ]
  (reduce
    (fn [[global-params doc-word-tuples] iteration]
      (println iteration)
      (one-iteration [T W] global-params doc-word-tuples)
      )
    [global-params doc-word-tuples]
    (range num-iteration)
    )
  nil

  )